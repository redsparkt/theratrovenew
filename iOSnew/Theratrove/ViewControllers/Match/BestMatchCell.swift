//
//  BestMatchCell.swift
//  Theratrove
//
//  Created by Dhruv Patel on 17/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class BestMatchCell: UITableViewCell {

    @IBOutlet var btncall_width: NSLayoutConstraint!
    @IBOutlet var btnMsg: UIButton!
    @IBOutlet var btnCall: UIButton!
    @IBOutlet var lblCost: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var imgUserImage: UIImageView!
    @IBOutlet var lblAbout: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var uvwRating: FloatRatingView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
