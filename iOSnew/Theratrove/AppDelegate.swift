//
//  AppDelegate.swift
//  Theratrove
//
//  Created by Dhruv Patel on 17/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit
import LGSideMenuController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var beforLogin : Int = 0
     var skip : Int = 0

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
      
        let dictUser = Constants.USERDEFAULTS.value(forKey: "username") as? String
        if(dictUser != nil && !(dictUser?.isEmpty)!){
            self.beforLogin = 1
            
//            let skipIntro = Constants.USERDEFAULTS.value(forKey: "SkipIntro") as? Int
//            if skipIntro == 1
//            {
//                let storyboard = UIStoryboard(name: "Home", bundle: nil)
//                let rootViewController = storyboard.instantiateViewController(withIdentifier: "HomePage") as? HomePage
//                let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuController") as? LeftMenuController
//                let navigationController = UINavigationController(rootViewController: rootViewController!)
//                navigationController.isNavigationBarHidden = true
//
//                let sideMenuController = LGSideMenuController(rootViewController: navigationController,
//                                                              leftViewController: leftViewController,
//                                                              rightViewController: nil)
//
//                sideMenuController.leftViewWidth = window!.frame.size.width - 100
//                sideMenuController.leftViewPresentationStyle = .slideAbove
//
//                window!.rootViewController = sideMenuController
//                window!.makeKeyAndVisible()
//            }
//            else
//            {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let rootViewController = storyboard.instantiateViewController(withIdentifier: "introduction") as? introduction
                let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuController") as? LeftMenuController
                let navigationController = UINavigationController(rootViewController: rootViewController!)
                navigationController.isNavigationBarHidden = true
                
                let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                              leftViewController: leftViewController,
                                                              rightViewController: nil)
                
                sideMenuController.leftViewWidth = window!.frame.size.width - 100
                sideMenuController.leftViewPresentationStyle = .slideAbove
                
                window!.rootViewController = sideMenuController
                window!.makeKeyAndVisible()
           // }
            
            
            //self.SetupIntroMenu()
        }
        else
        {
//            let skipIntro = Constants.USERDEFAULTS.value(forKey: "SkipIntro") as? Int
//            if skipIntro == 1
//            {
//                let storyboard = UIStoryboard(name: "Home", bundle: nil)
//                let rootViewController = storyboard.instantiateViewController(withIdentifier: "HomePage") as? HomePage
//                let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuController") as? LeftMenuController
//                let navigationController = UINavigationController(rootViewController: rootViewController!)
//                navigationController.isNavigationBarHidden = true
//
//                let sideMenuController = LGSideMenuController(rootViewController: navigationController,
//                                                              leftViewController: leftViewController,
//                                                              rightViewController: nil)
//
//                sideMenuController.leftViewWidth = window!.frame.size.width - 100
//                sideMenuController.leftViewPresentationStyle = .slideAbove
//
//                window!.rootViewController = sideMenuController
//                window!.makeKeyAndVisible()
//            }
//            else
//            {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let rootViewController = storyboard.instantiateViewController(withIdentifier: "introduction") as? introduction
                let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuController") as? LeftMenuController
                let navigationController = UINavigationController(rootViewController: rootViewController!)
                navigationController.isNavigationBarHidden = true
                
                let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                              leftViewController: leftViewController,
                                                              rightViewController: nil)
                
                sideMenuController.leftViewWidth = window!.frame.size.width - 100
                sideMenuController.leftViewPresentationStyle = .slideAbove
                
                window!.rootViewController = sideMenuController
                window!.makeKeyAndVisible()
           // }
        }
        return true
    }

    func SetupIntroMenu()
    {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let rootViewController = storyboard.instantiateViewController(withIdentifier: "introduction") as? introduction
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuController") as? LeftMenuController
        let navigationController = UINavigationController(rootViewController: rootViewController!)
        navigationController.isNavigationBarHidden = true
        
        
        let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                      leftViewController: leftViewController,
                                                      rightViewController: nil)
        
        sideMenuController.leftViewWidth = window!.frame.size.width - 100
        sideMenuController.leftViewPresentationStyle = .slideAbove
        
        window!.rootViewController = sideMenuController
        window!.makeKeyAndVisible()
    }
    
    func SetupMenu()
    {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let rootViewController = storyboard.instantiateViewController(withIdentifier: "HomePage") as? HomePage
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftMenuController") as? LeftMenuController        
        let navigationController = UINavigationController(rootViewController: rootViewController!)
        navigationController.isNavigationBarHidden = true
        
        
        let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                      leftViewController: leftViewController,
                                                      rightViewController: nil)        

        sideMenuController.leftViewWidth = window!.frame.size.width - 100
        sideMenuController.leftViewPresentationStyle = .slideAbove
        
        window!.rootViewController = sideMenuController
        window!.makeKeyAndVisible()
    }
    
    func setRootview()  {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootViewController = storyboard.instantiateViewController(withIdentifier: "LoginPage") as? LoginPage
        let navigationController = UINavigationController(rootViewController: rootViewController!)
        navigationController.isNavigationBarHidden = true
        window!.rootViewController = navigationController
        window!.makeKeyAndVisible()
    }
    
    func setRootview(GuestUser : String)  {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootViewController = storyboard.instantiateViewController(withIdentifier: "LoginPage") as? LoginPage
        rootViewController?.ForSignUp = GuestUser
        let navigationController = UINavigationController(rootViewController: rootViewController!)
        navigationController.isNavigationBarHidden = true
        Constants.appDelegate.window!.rootViewController = navigationController
        Constants.appDelegate.window!.makeKeyAndVisible()
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
}

