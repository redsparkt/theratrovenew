package com.theratrove.activityList;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.theratrove.R;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theratrove.activityList.appUtils.Constant.DEVICE_TYPE;

public class SignUpActivity extends AppCompatActivity implements Callback<JsonObject> {


    ImageView imgeback;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.edtUserName)
    EditText edtUserName;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.edtConfirmPwd)
    EditText edtConfirmPwd;
    @BindView(R.id.checkbox)
    CheckBox checkbox;
    @BindView(R.id.txtSignUp)
    TextView txtSignUp;
    @BindView(R.id.txtSignIn)
    TextView txtSignIn;
    @BindView(R.id.cvFirstname)
    CardView cvFirstname;


    ApiInterface apiService;
    HttpUrl httpUrlSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        apiService = ApiClient.getClient().create(ApiInterface.class);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            imgeback = toolbar.findViewById(R.id.imgBack);
        }

        Spannable wordtoSpan = new SpannableString(getString(R.string.i_agree_to_the_the_company_term_of_use));
        final ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(final View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://theratrove-staging.azurewebsites.net/Home/TermsAndConditions"));
//                startActivity(browserIntent);

                Intent intent = new Intent(SignUpActivity.this, TermAndConditionActivity.class);
                startActivity(intent);
            }

            public void updateDrawState(TextPaint ds) {// override updateDrawState
                ds.setColor(Color.rgb(0, 0, 255));
                ds.setUnderlineText(false); // set to false to remove underline
            }
        };

//        wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLUE), 23, wordtoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(clickableSpan, 23, wordtoSpan.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        checkbox.setMovementMethod(LinkMovementMethod.getInstance());
        checkbox.setText(wordtoSpan);


        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    if (CommonValidation.checkInternetConnection(SignUpActivity.this)) {
                        callSignUp(edtUserName.getText().toString().trim(), edtPassword.getText().toString().trim(),
                                edtEmail.getText().toString().trim(), edtConfirmPwd.getText().toString().trim());
                    } else {
                        CommonValidation.showToast(SignUpActivity.this, getString(R.string.no_internet));
                    }

                }

            }
        });


        txtSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(intent);
            }
        });

        imgeback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void callSignUp(String userName, String password, String email, String confrmpassword) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(Constant.LOGIN__USER_KEY, userName);
        jsonObject.addProperty(Constant.LOGIN__PASSWORD_KEY, password);
        jsonObject.addProperty(Constant.SIGN_UP_EMAIL, email);
        jsonObject.addProperty(Constant.SIGN_UP__CONFIRM_PASSWORD, confrmpassword);


        Call<JsonObject> callLogin = apiService.signUp(jsonObject);
        httpUrlSignUp = callLogin.request().url();
        callLogin.clone().enqueue(this);
        Utility.showProgress(SignUpActivity.this);
    }

    public boolean isValidate() {
        if (edtUserName.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(this, getString(R.string.username_blank_msg));
            return false;
        } else if (edtEmail.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(this, getString(R.string.email_blank));
            return false;
        } else if (!CommonValidation.isValidEmail(edtEmail.getText().toString().trim())) {
            CommonValidation.showToast(this, getString(R.string.email__valid_msg));
            return false;
        } else if (edtPassword.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(this, getString(R.string.passworde_blank_msg));
            return false;
        } else if (edtConfirmPwd.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(this, getString(R.string.confirm_password_blank_msg));
            return false;
        } else if (!CommonValidation.isValidPassword(edtPassword.getText().toString().trim())) {
            CommonValidation.showToast(this, getString(R.string.pass_match));
            return false;
        } else if (!edtPassword.getText().toString().trim().equals(edtConfirmPwd.getText().toString().trim())) {
            CommonValidation.showToast(this, getString(R.string.not_match));
            return false;
        } else if (!checkbox.isChecked()) {
            CommonValidation.showToast(this, getString(R.string.term));
            return false;
        }
        return true;
    }

    @Override
    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
        Utility.dismissProgress();
        if (response.isSuccessful() && response.code() == 200) {
            Log.e("Sucess", "response");
//            CommonValidation.showToast(SignUpActivity.this, response);
            CommonValidation.showToast(SignUpActivity.this, getString(R.string.success));
            finish();
        } else {
            try {
                CommonValidation.showToast(SignUpActivity.this, response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<JsonObject> call, Throwable t) {
        Utility.dismissProgress();
    }
}
