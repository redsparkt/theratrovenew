package com.theratrove.activityList.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.theratrove.R;
import com.theratrove.activityList.model.SearchFilterData;

import java.util.ArrayList;

public class ForAdapter_Profile extends RecyclerView.Adapter<ForAdapter_Profile.ViewHolder> {

    private ArrayList<SearchFilterData.ForOption> myItems = new ArrayList<>();
    Activity activity;
    private LayoutInflater inflater;

    public ForAdapter_Profile(Activity activity, ArrayList<SearchFilterData.ForOption> getItems) {
        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        myItems = getItems;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public AppCompatCheckBox cbSelect;

        public ViewHolder(View v) {
            super(v);
            cbSelect = (AppCompatCheckBox) v.findViewById(R.id.faith_checkbox);
        }
    }
    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.item_faith, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final SearchFilterData.ForOption objIncome = myItems.get(position);
        holder.cbSelect.setText(objIncome.getKey());

        //in some cases, it will prevent unwanted situations
        holder.cbSelect.setOnCheckedChangeListener(null);

        //if true, your checkbox will be selected, else unselected
        holder.cbSelect.setChecked(objIncome.getSelected());
        holder.cbSelect.setTag(holder.getAdapterPosition());
        holder.cbSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    myItems.get(holder.getAdapterPosition()).setSelected(isChecked);


            }
        });

    }

    @Override
    public int getItemCount() {
        return myItems.size();
    }

    public ArrayList<SearchFilterData.ForOption> getSelectForOption() {
        ArrayList<SearchFilterData.ForOption> selectedIssue = new ArrayList<>();
        for (SearchFilterData.ForOption c : myItems) {
            if (c.getSelected())
                selectedIssue.add(c);
        }

        return selectedIssue;
    }
}