//
//  Video.swift
//  Theratrove
//
//  Created by Dhruv Patel on 18/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer
import WebKit

class Video: UIViewController {
     @IBOutlet var wbview: WKWebView!
    var VideoUrl = String()
    var player = AVPlayer()
    var playerLayer = AVPlayerLayer()
    var playerController = AVPlayerViewController()
    
    @IBOutlet var uvwVideoView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if !VideoUrl.isEmpty{
//            self.playVideo(UrlString: self.VideoUrl)
//        }
        
        wbview.uiDelegate = self as? WKUIDelegate
        let url = URL (string: self.VideoUrl)
        let requestObj = URLRequest(url: url!)
        self.wbview.load(requestObj)
        // Do any additional setup after loading tThe view.
    }
    
//    private func playVideo(UrlString : String) {
//        let VideoURL : NSURL = NSURL(string: UrlString)!
//        self.player = AVPlayer(url: VideoURL as URL)
//        self.playerLayer = AVPlayerLayer(player: player)
//        playerLayer.frame = self.uvwVideoView.bounds
//        self.uvwVideoView.layer.addSublayer(playerLayer)
//        player.play()
//    }
    
    override func viewDidDisappear(_ animated: Bool) {
  
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
