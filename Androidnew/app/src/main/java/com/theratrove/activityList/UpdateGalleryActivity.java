package com.theratrove.activityList;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.ArrayMap;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.theratrove.R;
import com.theratrove.activityList.Adapter.GalleryAdapter;
import com.theratrove.activityList.Adapter.UpdateGalleryAdapter;
import com.theratrove.activityList.appUtils.AppSharedPreferences;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.appUtils.PermissionUtility;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.model.Profile;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.HttpUrl;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theratrove.activityList.EditProfileActivity.therapyGalleryLists;

public class UpdateGalleryActivity extends AppCompatActivity implements Callback<ResponseBody>, UpdateGalleryAdapter.DeleteAction,
        CropImageView.OnSetImageUriCompleteListener,
        CropImageView.OnCropImageCompleteListener {


    ApiInterface apiService;
    HttpUrl httpUrlCallLogin;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txttitle)
    TextView txttitle;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    @BindView(R.id.toolbar)
    Toolbar toolbarTop;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;


//    ArrayList<Profile.TherapyGalleryList> GalleryList;
    int selectedDeletePosition = -1;
    UpdateGalleryAdapter adapter;
    private Uri mCropImageUri;
    private File imageFile;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_gallery);
        ButterKnife.bind(this);

        setSupportActionBar(toolbarTop);
        txttitle.setText(getResources().getString(R.string.update_gallery));
        imgSearch.setImageResource(R.drawable.plus);
        imgSearch.setColorFilter(Color.argb(255, 255, 255, 255));

        apiService = ApiClient.getClient().create(ApiInterface.class);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getIntent().hasExtra("list")) {
//            GalleryList = (ArrayList<Profile.TherapyGalleryList>) getIntent().getSerializableExtra("list");
        }


        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);

        if (therapyGalleryLists != null && therapyGalleryLists.size() > 0) {
            adapter = new UpdateGalleryAdapter(this, therapyGalleryLists, this);
            recyclerView.setAdapter(adapter);
        }

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (CommonValidation.checkWriteExternalPermission(UpdateGalleryActivity.this)) {
                    if (CropImage.isExplicitCameraPermissionRequired(getApplicationContext())) {
                        requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                                CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                    } else {
                        CropImage.startPickImageActivity(UpdateGalleryActivity.this);
                    }
                } else {
                    PermissionUtility.allPermission(UpdateGalleryActivity.this);
                }
            }
        });


    }

    public void callDeleteGalley(int pos) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("TherapistId", AppSharedPreferences.getSharePreference(this).getUserID());
        jsonObject.addProperty("PictureId", therapyGalleryLists.get(pos).getId());
        Call<ResponseBody> callLogin = apiService.deleteHalleryPic(jsonObject);
        httpUrlCallLogin = callLogin.request().url();
        callLogin.clone().enqueue(this);
        Utility.showProgress(UpdateGalleryActivity.this);
    }

    @Override
    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
        Utility.dismissProgress();
        if (response.isSuccessful() && response.code() == 200) {
            try {

                therapyGalleryLists.remove(selectedDeletePosition);
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                CommonValidation.showToast(UpdateGalleryActivity.this, response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
        Utility.dismissProgress();
        t.printStackTrace();
    }

    @Override
    public void selectedItem(int position) {

        if (CommonValidation.checkInternetConnection(this)) {
            selectedDeletePosition = position;
            callDeleteGalley(position);
        } else {
            CommonValidation.showToast(this, getString(R.string.no_internet));
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE
                    && resultCode == AppCompatActivity.RESULT_OK) {
                Uri imageUri = CropImage.getPickImageResultUri(this, data);

                // For API >= 23 we need to check specifically that we have permissions to read external
                // storage,
                // but we don't know if we need to for the URI so the simplest is to try open the stream and
                // see if we get error.
                boolean requirePermissions = false;
                if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {

                    // request permissions and handle the result in onRequestPermissionsResult()
                    requirePermissions = true;
                    mCropImageUri = imageUri;
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
                } else {

                    setImageUri(imageUri);
                }
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                if (data != null) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    handleCropResult(result);
                }
            }
        }
    }

    public void setImageUri(Uri imageUri) {
        //        mCropImageView.setImageUriAsync(imageUri);
        CropImage.activity(imageUri)   // .setAspectRatio(1, 1)    .setFixAspectRatio(true)
                .setRequestedSize(100, 100)
                .setFixAspectRatio(true)
                .setAspectRatio(1, 1)
                .start(UpdateGalleryActivity.this);
    }

    private void handleCropResult(CropImageView.CropResult result) {
        if (result.getError() == null) {
            if (result.getUri() != null) {
                //                ivMusicBanner.setImageURI(result.getUri());
                onSetImageResult(result.getUri());
            }
        } else {
            Log.e("AIC", "Failed to crop image", result.getError());
            Toast.makeText(getApplicationContext(),
                    "Image crop failed: " + result.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void onSetImageResult(Uri imageUri) {
        try {

            //            Uri imageUri = data.getData();
            Bitmap thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
           /* imageFile =
                    new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");*/
            imageFile = new File(getExternalFilesDir(null), "temp_image.jpg");
            FileOutputStream fo;
            try {
                imageFile.createNewFile();
                fo = new FileOutputStream(imageFile);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

//            imgUserHome.setImageURI(imageUri);

            callUploadgalleryPic(bytes.toByteArray());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error == null) {
            Toast.makeText(getApplicationContext(), "Image load successful", Toast.LENGTH_SHORT).show();
        } else {
            Log.e("AIC", "Failed to load image by URI", error);
            Toast.makeText(getApplicationContext(), "Image load failed: " + error.getMessage(),
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        handleCropResult(result);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG)
                        .show();
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null
                    && grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setImageUri(mCropImageUri);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    @SuppressLint("NewApi")
    public void callUploadgalleryPic(byte[] aByte) {


        try {
            Map<String, Object> jsonParams = new ArrayMap<>();
            jsonParams.put("TherapistId", AppSharedPreferences.getSharePreference(this).getUserID());
            jsonParams.put("PictureFileName", imageFile.getName());
            jsonParams.put("PictureFile", Base64.encodeToString(aByte, Base64.DEFAULT));
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(jsonParams)).toString());
            Utility.showProgress(UpdateGalleryActivity.this);
            Call<ResponseBody> callLogin = apiService.galleryPicUpload(body);
            HttpUrl httpUrl = callLogin.request().url();
            Log.e("Url==", httpUrl + "");
            callLogin.clone().enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                    Utility.dismissProgress();
                    try {
                        if (response.code() == 200) {

                            JSONObject jsonObject = new JSONObject(response.body().string());
                            ArrayList<Profile.TherapyGalleryList> list = new ArrayList<>();
                            Profile.TherapyGalleryList therapyGalleryList = new Profile.TherapyGalleryList();
                            therapyGalleryList.setId(jsonObject.optInt("therapyGalleryId"));
                            therapyGalleryList.setGalleryImage(jsonObject.optString("urlImage"));
                            list.add(therapyGalleryList);

                            if (adapter == null) {
                                adapter = new UpdateGalleryAdapter(UpdateGalleryActivity.this, list, UpdateGalleryActivity.this);
                                recyclerView.setAdapter(adapter);
                            } else {
                                adapter.updateList(list);
                            }


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Utility.dismissProgress();
                    t.printStackTrace();
                }
            });
//

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}
