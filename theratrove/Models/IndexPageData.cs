﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using theratrove.data;

namespace theratrove.Models
{
    public class IndexPageData
    {
        public IndexPageData()
        {
            //this.TherapistsByCondition = new List<TherapyType>();
            this.TherapistsByCondition = new List<string>();
            this.TherapistsByLocation = new List<string>();
            this.SearchResult = new SearchResults();
        }

        public int NoOfTherapists { get; set; }

        public int Members { get; set; }

        public int DailySearches { get; set; }

        //public List<TherapyType> TherapistsByCondition { get; set; }
        public List<string> TherapistsByCondition { get; set; }
        public List<theratrove.data.Testimonial> TestimonialList { get; set; }

        public List<string> TherapistsByLocation { get; set; }

        public SearchResults SearchResult { get; set; }
    }
}