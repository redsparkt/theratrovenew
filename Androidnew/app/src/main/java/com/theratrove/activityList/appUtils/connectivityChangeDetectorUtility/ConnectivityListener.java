package com.theratrove.activityList.appUtils.connectivityChangeDetectorUtility;

/**
 * Created by Redspark on 12/18/2017.
 */

public interface ConnectivityListener
{
  void connectivityChanged(boolean connectivityAvailable);
}
