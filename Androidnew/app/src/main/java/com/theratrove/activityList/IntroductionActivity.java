package com.theratrove.activityList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.theratrove.R;
import com.theratrove.activityList.Adapter.IntroPagerAdapte;
import com.theratrove.activityList.appUtils.AppSharedPreferences;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class IntroductionActivity extends AppCompatActivity {


    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.txttitle)
    TextView txttitle;
    @BindView(R.id.indicator)
    CircleIndicator indicator;

    @BindView(R.id.txtSkip)
    TextView txtSkip;

    ArrayList<Integer> imagelist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);
        ButterKnife.bind(this);

        initView();

        if (AppSharedPreferences.getSharePreference(IntroductionActivity.this).getOnlyTime()) {
            Intent intent = new Intent(IntroductionActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        }

    }

    public void initView() {
        imagelist.add(R.drawable.intro1);
        imagelist.add(R.drawable.intro2);
        imagelist.add(R.drawable.intro3);
        IntroPagerAdapte adapter = new IntroPagerAdapte(this, imagelist);
        viewpager.setAdapter(adapter);
        indicator.setViewPager(viewpager);

        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IntroductionActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
                AppSharedPreferences.getSharePreference(IntroductionActivity.this).setOnlyOnetime(true);
            }
        });

    }
}
