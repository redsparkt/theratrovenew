package com.theratrove.activityList;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.ArrayMap;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.theratrove.R;
import com.theratrove.activityList.Adapter.FaithAdapter;
import com.theratrove.activityList.Adapter.ForAdapter_Profile;
import com.theratrove.activityList.Adapter.GenderAdapter;
import com.theratrove.activityList.Adapter.GenderAdapter_Profile;
import com.theratrove.activityList.Adapter.GenderAdapter_Spinner;
import com.theratrove.activityList.Adapter.InsuranceAdapter;
import com.theratrove.activityList.Adapter.IssueAdapter;
import com.theratrove.activityList.Adapter.LanguageAdapter;
import com.theratrove.activityList.Adapter.LocationAdapter_Profile;
import com.theratrove.activityList.Adapter.TypeOfTherapyAdapter;
import com.theratrove.activityList.appUtils.AppSharedPreferences;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.PermissionUtility;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.model.GenderModel;
import com.theratrove.activityList.model.Profile;
import com.theratrove.activityList.model.SearchFilterData;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.HttpUrl;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity implements Callback<SearchFilterData>, CropImageView.OnSetImageUriCompleteListener,
        CropImageView.OnCropImageCompleteListener {


    ApiInterface apiService;

    HttpUrl httpGetFilterData;


    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txttitle)
    TextView txttitle;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    @BindView(R.id.toolbar)
    Toolbar toolbarTop;
    @BindView(R.id.imgUserHome)
    ImageView imgUserHome;
    @BindView(R.id.imgUpload)
    ImageView imgUpload;
    @BindView(R.id.edtFirstName)
    TextInputEditText edtFirstName;
    @BindView(R.id.edtLastName)
    TextInputEditText edtLastName;
    @BindView(R.id.edtTitle)
    TextInputEditText edtTitle;
    @BindView(R.id.spinner_gender)
    Spinner spinnerGender;
    @BindView(R.id.edtYear)
    TextInputEditText edtYear;
    @BindView(R.id.edtEducation)
    TextInputEditText edtEducation;
    @BindView(R.id.edtLicense)
    TextInputEditText edtLicense;
    @BindView(R.id.edtAbout)
    AppCompatEditText edtAbout;
    @BindView(R.id.edtVideo)
    TextInputEditText edtVideo;
    @BindView(R.id.edtPhone)
    TextInputEditText edtPhone;
    @BindView(R.id.edtLocation)
    TextInputEditText edtLocation;
    @BindView(R.id.edtAvg_Cost)
    TextInputEditText edtAvgCost;
    @BindView(R.id.edtAddress)
    TextInputEditText edtAddress;
    @BindView(R.id.edtAddress_second)
    TextInputEditText edtAddressSecond;
    @BindView(R.id.edtCity)
    TextInputEditText edtCity;
    @BindView(R.id.edtState)
    TextInputEditText edtState;
    @BindView(R.id.edtZipCode)
    TextInputEditText edtZipCode;
    @BindView(R.id.recyclerview_faith)
    RecyclerView recyclerviewFaith;
    @BindView(R.id.recyclerview_For)
    RecyclerView recyclerviewFor;
    @BindView(R.id.recyclerview_gender)
    RecyclerView recyclerviewGender;
    @BindView(R.id.recyclerview_Location)
    RecyclerView recyclerviewLocation;
    @BindView(R.id.recyclerview_Language)
    RecyclerView recyclerviewLanguage;
    @BindView(R.id.recyclerview_insurance)
    RecyclerView recyclerviewInsurance;
    @BindView(R.id.recyclerview_issues)
    RecyclerView recyclerviewIssues;
    @BindView(R.id.recyclerview_therapy)
    RecyclerView recyclerviewTherapy;
    @BindView(R.id.txtSubmit)
    TextView txtSubmit;

    @BindView(R.id.txtUpdate)
    TextView txtUpdate;


    @BindView(R.id.checkbox)
    CheckBox checkbox;

    FaithAdapter faithAdapter;
    IssueAdapter issueAdapter;
    InsuranceAdapter insuranceAdapter;
    LanguageAdapter languageAdapter;
    ForAdapter_Profile forAdapter;
    LocationAdapter_Profile locationAdapter;
    GenderAdapter_Profile genderAdapter;
    TypeOfTherapyAdapter typeOfTherapyAdapter;


    String faith, forFilter, GenderPreference, LocationFilter, LanguageFilter, InsuranceFilter, IssuesFilter;

    public static ArrayList<Profile.TherapyGalleryList> therapyGalleryLists;
    ArrayList<Profile.TherapistTypesOfTherapy> typeOfTheraphy;

    int gender_selected;

    GenderAdapter_Spinner gendata;
    int genderId;
    /*Profile Photo*/
    private Uri mCropImageUri;
    private File imageFile;
    ByteArrayOutputStream bytes;
    RequestOptions options;

    ArrayList<GenderModel> genderList = new ArrayList<>();

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);
        ButterKnife.bind(this);

        GenderModel genderModel1 = new GenderModel();
        genderModel1.name = "Male";
        genderModel1.genderId = "1";
        genderList.add(genderModel1);

        GenderModel genderModel2 = new GenderModel();
        genderModel2.name = "Female";
        genderModel2.genderId = "2";
        genderList.add(genderModel2);

        GenderModel genderModel3 = new GenderModel();
        genderModel3.name = "Select Gender";
        genderModel3.genderId = "0";
        genderList.add(genderModel3);

        setSupportActionBar(toolbarTop);
        txttitle.setText("Edit Profile");
        imgSearch.setVisibility(View.GONE);
        apiService = ApiClient.getClient().create(ApiInterface.class);

        txtUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EditProfileActivity.this, UpdateGalleryActivity.class);
//                if (therapyGalleryLists != null && therapyGalleryLists.size() > 0) {
//                    intent.putExtra("list", therapyGalleryLists);
//                }
                startActivity(intent);

            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imgUserHome.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (CommonValidation.checkWriteExternalPermission(EditProfileActivity.this)) {
                    if (CropImage.isExplicitCameraPermissionRequired(getApplicationContext())) {
                        requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                                CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                    } else {
                        CropImage.startPickImageActivity(EditProfileActivity.this);
                    }
                } else {
                    PermissionUtility.allPermission(EditProfileActivity.this);
                }
            }
        });

        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonValidation.checkInternetConnection(EditProfileActivity.this)) {
                    if (bytes != null) {
                        callUploadProfilePic(bytes.toByteArray());
                    } else {
                        callUpdateProfile(AppSharedPreferences.getSharePreference(EditProfileActivity.this).getUserID(), genderId,
                                edtFirstName.getText().toString().trim(), edtLastName.getText().toString().trim(),
                                edtTitle.getText().toString().trim(), edtYear.getText().toString().trim(), edtEducation.getText().toString().trim(),
                                edtLicense.getText().toString().trim(), edtAbout.getText().toString().trim(),
                                edtPhone.getText().toString().trim(), edtVideo.getText().toString().trim(),
                                edtAvgCost.getText().toString().trim(), edtAddress.getText().toString().trim(), edtAddressSecond.getText().toString().trim()
                                , edtCity.getText().toString().trim(),
                                edtState.getText().toString().trim(),
                                edtZipCode.getText().toString().trim(), checkbox.isChecked() + "",
                                getFaithId(faithAdapter.getSelectFaith()),
                                getForId(forAdapter.getSelectForOption()),
                                getGenderPreferenceId(genderAdapter.getSelectGenderOption()),
                                getLocationOptionId(locationAdapter.getSelectLocationOption()),
                                getLanguageId(languageAdapter.getSelectLanguageOption()),
                                getIssuranceId(insuranceAdapter.getSelectInsuranceOption())
                                , getIssueId(issueAdapter.getSelectIssue()),
                                getTherapyId(typeOfTherapyAdapter.getSelectedTypeOfTheraphy()));
                    }
                } else {
                    CommonValidation.showToast(EditProfileActivity.this, getString(R.string.no_internet));
                }

            }
        });


        if (CommonValidation.checkInternetConnection(this)) {
            callGetSearchFilterList();
        } else {
            CommonValidation.showToast(this, getString(R.string.no_internet));
        }

        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {


                try {
                    GenderModel locationOption = gendata.getItem(position);
                    if (locationOption.getGenderId() != null) {
                        genderId = Integer.parseInt(locationOption.getGenderId());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        if (AppSharedPreferences.getSharePreference(this).getProfileic() != null
                && AppSharedPreferences.getSharePreference(this).getProfileic().length() > 0) {
            options = new RequestOptions()
//                .centerCrop()
                    .placeholder(R.drawable.dummyuserimage)
                    .error(R.drawable.dummyuserimage);

            Glide.with(this).load(AppSharedPreferences.getSharePreference(this).getProfileic()).apply(options).into(imgUserHome);
        }

        if (getIntent().hasExtra("FirstName")) {

            edtFirstName.setText(getIntent().getStringExtra("FirstName"));
            edtLastName.setText(getIntent().getStringExtra("lastName"));
            edtTitle.setText(getIntent().getStringExtra("title"));
            edtYear.setText(getIntent().getStringExtra("YearsInService"));
            edtEducation.setText(getIntent().getStringExtra("Education"));
            checkbox.setChecked(getIntent().getBooleanExtra("AcceptInsurance", false));
            edtLicense.setText(getIntent().getStringExtra("License"));
            if (getIntent().getStringExtra("AboutMe") != null && getIntent().getStringExtra("AboutMe").length() > 0) {
                edtAbout.setText(Html.fromHtml(getIntent().getStringExtra("AboutMe")));
            }
            edtVideo.setText(getIntent().getStringExtra("videoUrl"));
            edtPhone.setText(getIntent().getStringExtra("phoneNumber"));

            edtLocation.setText(getIntent().getStringExtra("Location"));
            edtAvgCost.setText(getIntent().getIntExtra("AvgCost", 0) + "");
            edtAddress.setText(getIntent().getStringExtra("Address1"));
            edtAddressSecond.setText(getIntent().getStringExtra("Address2"));

            edtCity.setText(getIntent().getStringExtra("city"));
            edtZipCode.setText(getIntent().getStringExtra("zipcode"));

            faith = getIntent().getStringExtra("faithFilter");
            forFilter = getIntent().getStringExtra("forFilter");

            GenderPreference = getIntent().getStringExtra("GenderPreference");
            LocationFilter = getIntent().getStringExtra("LocationFilter");
            LanguageFilter = getIntent().getStringExtra("LanguageFilter");
            InsuranceFilter = getIntent().getStringExtra("InsuranceFilter");
            IssuesFilter = getIntent().getStringExtra("IssuesFilter");
            gender_selected = getIntent().getIntExtra("gender", 1);

            therapyGalleryLists = (ArrayList<Profile.TherapyGalleryList>) getIntent().getSerializableExtra("galleryImage");

            typeOfTheraphy = (ArrayList<Profile.TherapistTypesOfTherapy>) getIntent().getSerializableExtra("typeofFilter");
        }


    }

    @SuppressLint("NewApi")
    public void callUploadProfilePic(byte[] aByte) {


        try {
            Map<String, Object> jsonParams = new ArrayMap<>();
            jsonParams.put("TherapistId", AppSharedPreferences.getSharePreference(this).getUserID());
            jsonParams.put("PictureFileName", imageFile.getName());
            jsonParams.put("PictureFile", Base64.encodeToString(aByte, Base64.DEFAULT));
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(jsonParams)).toString());
            Utility.showProgress(EditProfileActivity.this);
            Call<ResponseBody> callLogin = apiService.ProfilePicUpload(body);
            HttpUrl httpUrl = callLogin.request().url();
            Log.e("Url==", httpUrl + "");
            callLogin.clone().enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                    Utility.dismissProgress();
                    try {
                        if (response.code() == 200) {


                            if (response.body() != null) {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                AppSharedPreferences.getSharePreference(EditProfileActivity.this).setProfilePic(jsonObject.optString("profileImageURL"));
                            }

                            callUpdateProfile(AppSharedPreferences.getSharePreference(EditProfileActivity.this).getUserID(), genderId,
                                    edtFirstName.getText().toString().trim(), edtLastName.getText().toString().trim(),
                                    edtTitle.getText().toString().trim(), edtYear.getText().toString().trim(), edtEducation.getText().toString().trim(),
                                    edtLicense.getText().toString().trim(), edtAbout.getText().toString().trim(),
                                    edtPhone.getText().toString().trim(), edtVideo.getText().toString().trim(),
                                    edtAvgCost.getText().toString().trim(), edtAddress.getText().toString().trim(), edtAddressSecond.getText().toString().trim()
                                    , edtCity.getText().toString().trim(),
                                    edtState.getText().toString().trim(),
                                    edtZipCode.getText().toString().trim(), checkbox.isChecked() + "",
                                    getFaithId(faithAdapter.getSelectFaith()),
                                    getForId(forAdapter.getSelectForOption()),
                                    getGenderPreferenceId(genderAdapter.getSelectGenderOption()),
                                    getLocationOptionId(locationAdapter.getSelectLocationOption()),
                                    getLanguageId(languageAdapter.getSelectLanguageOption()),
                                    getIssuranceId(insuranceAdapter.getSelectInsuranceOption())
                                    , getIssueId(issueAdapter.getSelectIssue()),
                                    getTherapyId(typeOfTherapyAdapter.getSelectedTypeOfTheraphy()));

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Utility.dismissProgress();
                    t.printStackTrace();
                }
            });
//

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void callUpdateProfile(String userID, int gender, String fName, String lName,
                                  String title, String year, String edu, String lic, String abt, String phonenumber,
                                  String url, String cost, String add1, String add2, String city, String state
            , String zipcode, String acceptded, JSONArray selectedfaith, JSONArray selectedfor
            , JSONArray selectedgebder, JSONArray selectedlocation, JSONArray selectedlan, JSONArray selectedLnsu,
                                  JSONArray selectedIssue, JSONArray getTherapyId) {

        try {

            Map<String, Object> jsonParams = new ArrayMap<>();
            jsonParams.put("UserId", userID);
            jsonParams.put("Gender", gender);
            jsonParams.put("FirstName", fName);
            jsonParams.put("LastName", lName);
            jsonParams.put("Title", title);
            jsonParams.put("YearsInService", year);
            jsonParams.put("Education", edu);
            jsonParams.put("License", lic);
            jsonParams.put("AboutMe", abt);
            jsonParams.put("PhoneNumber", phonenumber);
            if (url != null && url.length() > 0) {
                jsonParams.put("VideoUrl", url);
            }


            jsonParams.put("AvgCost", cost);
            jsonParams.put("Address1", add1);
            jsonParams.put("Address2", add2);
            jsonParams.put("City", city);
            jsonParams.put("State", state);
            jsonParams.put("ZipCode", zipcode);
            jsonParams.put("AcceptInsurance", acceptded);
            jsonParams.put("SelectedFaith", selectedfaith);
            jsonParams.put("SelectedFor", selectedfor);
            jsonParams.put("SelectedGenderPreference", selectedgebder);
            jsonParams.put("SelectedLocation", selectedlocation);
            jsonParams.put("SelectedLanguage", selectedlan);
            jsonParams.put("SelectedInsurance", selectedLnsu);
            jsonParams.put("SelectedIssues", selectedIssue);
            jsonParams.put("SelectedTherapistTypesOfTherapy", getTherapyId);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(jsonParams)).toString());
            Log.e("Json Object==", (new JSONObject(jsonParams)).toString());

            Utility.showProgress(EditProfileActivity.this);
            Call<ResponseBody> callLogin = apiService.EditProfile(body);
            HttpUrl httpUrl = callLogin.request().url();
            Log.e("Url==", httpUrl + "");
            callLogin.clone().enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    Log.e("Code===", response.code() + "");
                    try {
                        Utility.dismissProgress();
                        if (response.code() == 200) {
                            CommonValidation.showToast(EditProfileActivity.this, "Profile Update successfully.");
                            finish();
                        } else {
                            try {
                                CommonValidation.showToast(EditProfileActivity.this, response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    } catch (Throwable e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, Throwable t) {
                    Utility.dismissProgress();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void callGetSearchFilterList() {
        Call<SearchFilterData> callLogin = apiService.getSearchFilterData();
        httpGetFilterData = callLogin.request().url();
        callLogin.clone().enqueue(this);
        Utility.showProgress(this);
    }


    @Override
    public void onResponse(@NonNull Call<SearchFilterData> call, @NonNull Response<SearchFilterData> response) {
        Utility.dismissProgress();
        if (response.isSuccessful() && response.code() == 200) {
            try {
                SearchFilterData searchFilterData = response.body();

                if (searchFilterData.getFaithOpitions() != null && searchFilterData.getFaithOpitions().size() > 0) {
                    if (faith != null && faith.length() > 0) {
                        List<String> selectedList = Arrays.asList(faith.split(","));
                        Log.e("selectedList==", selectedList.toString());
                        mainloop:
                        for (int i = 0; i < selectedList.size(); i++) {
                            for (SearchFilterData.FaithOpition faithOpition : searchFilterData.getFaithOpitions()) {
                                if (selectedList.get(i).trim().equalsIgnoreCase(faithOpition.getKey())) {
                                    faithOpition.setSelected(true);
                                    continue mainloop;
                                }
                            }
                        }
                    }

                    faithAdapter = new FaithAdapter(this, searchFilterData.getFaithOpitions());
                    recyclerviewFaith.setAdapter(faithAdapter);
                }
//
                if (searchFilterData.getForOptions() != null && searchFilterData.getForOptions().size() > 0) {
                    if (forFilter != null && forFilter.length() > 0) {
                        List<String> selectedList = Arrays.asList(forFilter.split(","));
                        Log.e("selectedList==for==", selectedList.toString());
                        mainloop:
                        for (int i = 0; i < selectedList.size(); i++) {
                            for (SearchFilterData.ForOption forOption : searchFilterData.getForOptions()) {
                                if (selectedList.get(i).trim().equalsIgnoreCase(forOption.getKey())) {
                                    forOption.setSelected(true);
                                    continue mainloop;
                                }
                            }
                        }
                    }

                    forAdapter = new ForAdapter_Profile(this, searchFilterData.getForOptions());
                    recyclerviewFor.setAdapter(forAdapter);
                }

                if (searchFilterData.getGenderOptions() != null && searchFilterData.getGenderOptions().size() > 0) {

                    if (genderList.size() > 0) {
                        gendata = new GenderAdapter_Spinner(this, genderList);
                        spinnerGender.setAdapter(gendata);
                        int position = getIndex(genderList, gender_selected);
                        spinnerGender.setSelection(position);
                    }


                    if (GenderPreference != null && GenderPreference.length() > 0) {
                        List<String> selectedList = Arrays.asList(GenderPreference.split(","));
                        mainloop:
                        for (int i = 0; i < selectedList.size(); i++) {
                            for (SearchFilterData.GenderOption genderOption : searchFilterData.getGenderOptions()) {
                                if (selectedList.get(i).trim().equalsIgnoreCase(genderOption.getKey())) {
                                    genderOption.setSelected(true);
                                    continue mainloop;
                                }
                            }
                        }
                    }


                    genderAdapter = new GenderAdapter_Profile(this, searchFilterData.getGenderOptions());
                    recyclerviewGender.setAdapter(genderAdapter);
                }

                if (searchFilterData.getLocationOptions() != null && searchFilterData.getLocationOptions().size() > 0) {

                    if (LocationFilter != null && LocationFilter.length() > 0) {
                        List<String> selectedList = Arrays.asList(LocationFilter.split(","));
                        Log.e("selectedList==for==", selectedList.toString());
                        mainloop:
                        for (int i = 0; i < selectedList.size(); i++) {
                            for (SearchFilterData.LocationOption locationOption : searchFilterData.getLocationOptions()) {
                                if (selectedList.get(i).trim().equalsIgnoreCase(locationOption.getKey())) {
                                    locationOption.setSelected(true);
                                    continue mainloop;
                                }
                            }
                        }
                    }

                    locationAdapter = new LocationAdapter_Profile(this, searchFilterData.getLocationOptions());
                    recyclerviewLocation.setAdapter(locationAdapter);
                }

                if (searchFilterData.getLanguageOptions() != null && searchFilterData.getLanguageOptions().size() > 0) {

                    if (LanguageFilter != null && LanguageFilter.length() > 0) {
                        List<String> selectedList = Arrays.asList(LanguageFilter.split(","));
                        Log.e("selectedList==for==", selectedList.toString());
                        mainloop:
                        for (int i = 0; i < selectedList.size(); i++) {
                            for (SearchFilterData.LanguageOption languageOption : searchFilterData.getLanguageOptions()) {
                                if (selectedList.get(i).trim().equalsIgnoreCase(languageOption.getKey())) {
                                    languageOption.setSelected(true);
                                    continue mainloop;
                                }
                            }
                        }
                    }


                    languageAdapter = new LanguageAdapter(this, searchFilterData.getLanguageOptions());
                    recyclerviewLanguage.setAdapter(languageAdapter);
                }


                if (searchFilterData.getInsuranceOptions() != null && searchFilterData.getInsuranceOptions().size() > 0) {


                    if (InsuranceFilter != null && InsuranceFilter.length() > 0) {
                        List<String> selectedList = Arrays.asList(InsuranceFilter.split(","));
                        Log.e("selectedList==for==", selectedList.toString());
                        mainloop:
                        for (int i = 0; i < selectedList.size(); i++) {
                            for (SearchFilterData.InsuranceOption insuranceOption : searchFilterData.getInsuranceOptions()) {
                                if (selectedList.get(i).trim().equalsIgnoreCase(insuranceOption.getKey())) {
                                    insuranceOption.setSelected(true);
                                    continue mainloop;
                                }
                            }
                        }
                    }


                    insuranceAdapter = new InsuranceAdapter(this, searchFilterData.getInsuranceOptions());
                    recyclerviewInsurance.setAdapter(insuranceAdapter);
                }

                if (searchFilterData.getIssuesOptions() != null && searchFilterData.getIssuesOptions().size() > 0) {


                    if (IssuesFilter != null && IssuesFilter.length() > 0) {
                        List<String> selectedList = Arrays.asList(IssuesFilter.split(","));
                        Log.e("selectedList==for==", selectedList.toString());
                        mainloop:
                        for (int i = 0; i < selectedList.size(); i++) {
                            for (SearchFilterData.IssuesOption issuesOption : searchFilterData.getIssuesOptions()) {
                                if (selectedList.get(i).trim().equalsIgnoreCase(issuesOption.getKey())) {
                                    issuesOption.setSelected(true);
                                    continue mainloop;
                                }
                            }
                        }
                    }

                    issueAdapter = new IssueAdapter(this, searchFilterData.getIssuesOptions());
                    recyclerviewIssues.setAdapter(issueAdapter);


                }
//
                if (typeOfTheraphy != null && typeOfTheraphy.size() > 0) {
                    typeOfTherapyAdapter = new TypeOfTherapyAdapter(this, typeOfTheraphy);
                    recyclerviewTherapy.setAdapter(typeOfTherapyAdapter);
                }


            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE
                    && resultCode == AppCompatActivity.RESULT_OK) {
                Uri imageUri = CropImage.getPickImageResultUri(this, data);

                // For API >= 23 we need to check specifically that we have permissions to read external
                // storage,
                // but we don't know if we need to for the URI so the simplest is to try open the stream and
                // see if we get error.
                boolean requirePermissions = false;
                if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {

                    // request permissions and handle the result in onRequestPermissionsResult()
                    requirePermissions = true;
                    mCropImageUri = imageUri;
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
                } else {

                    setImageUri(imageUri);
                }
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                if (data != null) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    handleCropResult(result);
                }
            }
        }
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        handleCropResult(result);
    }

    /**
     * Set the image to show for cropping.
     */
    public void setImageUri(Uri imageUri) {
        //        mCropImageView.setImageUriAsync(imageUri);
        CropImage.activity(imageUri)   // .setAspectRatio(1, 1)    .setFixAspectRatio(true)
                .setRequestedSize(100, 100)
                .setFixAspectRatio(true)
                .setAspectRatio(1, 1)
                .start(EditProfileActivity.this);
    }

    private void handleCropResult(CropImageView.CropResult result) {
        if (result.getError() == null) {
            if (result.getUri() != null) {
                //                ivMusicBanner.setImageURI(result.getUri());
                onSetImageResult(result.getUri());
            }
        } else {
            Log.e("AIC", "Failed to crop image", result.getError());
            Toast.makeText(getApplicationContext(),
                    "Image crop failed: " + result.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG)
                        .show();
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null
                    && grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setImageUri(mCropImageUri);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    /*pwd :kasillas*/
    private void onSetImageResult(Uri imageUri) {
        try {

            //            Uri imageUri = data.getData();
            Bitmap thumbnail = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
            bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
           /* imageFile =
                    new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");*/
            imageFile = new File(getExternalFilesDir(null), "temp_image.jpg");
            FileOutputStream fo;
            try {
                imageFile.createNewFile();
                fo = new FileOutputStream(imageFile);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            imgUserHome.setImageURI(imageUri);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        if (error == null) {
            Toast.makeText(getApplicationContext(), "Image load successful", Toast.LENGTH_SHORT).show();
        } else {
            Log.e("AIC", "Failed to load image by URI", error);
            Toast.makeText(getApplicationContext(), "Image load failed: " + error.getMessage(),
                    Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onFailure(Call<SearchFilterData> call, Throwable t) {
        Utility.dismissProgress();
        t.printStackTrace();
    }

    public JSONArray getFaithId(ArrayList<SearchFilterData.FaithOpition> faithOpitions) {
//        StringBuilder sb = new StringBuilder();
        JSONArray SelectedFaith = new JSONArray();
        JSONObject jsonObject = null;

        for (SearchFilterData.FaithOpition a : faithOpitions) {
            try {
                jsonObject = new JSONObject();
                jsonObject.put("Id", a.getValue());
                jsonObject.put("Name", a.getKey());
                jsonObject.put("Checked", a.getSelected());
                jsonObject.put("itemFor", 1);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            SelectedFaith.put(jsonObject);

//            sb.append(a.getValue());
//            sb.append(",");
        }
        return SelectedFaith;
    }

    public JSONArray getIssueId(ArrayList<SearchFilterData.IssuesOption> issuesOptions) {
        StringBuilder sb = new StringBuilder();

        JSONArray IssueArray = new JSONArray();
        JSONObject jsonObject = null;

        for (SearchFilterData.IssuesOption a : issuesOptions) {
            try {
                jsonObject = new JSONObject();
                jsonObject.put("Id", a.getValue());
                jsonObject.put("Name", a.getKey());
                jsonObject.put("Checked", a.getSelected());
                jsonObject.put("itemFor", 4);
            } catch (Exception e) {
                e.printStackTrace();
            }
            IssueArray.put(jsonObject);
        }


        return IssueArray;
    }

    public JSONArray getIssuranceId(ArrayList<SearchFilterData.InsuranceOption> issuesOptions) {
        StringBuilder sb = new StringBuilder();

        JSONArray IssuranceArray = new JSONArray();
        JSONObject jsonObject = null;

        for (SearchFilterData.InsuranceOption a : issuesOptions) {
//            sb.append(a.getValue());
//            sb.append(",");

            try {
                jsonObject = new JSONObject();
                jsonObject.put("Id", a.getValue());
                jsonObject.put("Name", a.getKey());
                jsonObject.put("Checked", a.getSelected());
                jsonObject.put("itemFor", 5);
            } catch (Exception e) {
                e.printStackTrace();
            }

            IssuranceArray.put(jsonObject);
        }


        return IssuranceArray;
    }

    public JSONArray getLanguageId(ArrayList<SearchFilterData.LanguageOption> issuesOptions) {
        StringBuilder sb = new StringBuilder();

        JSONArray LanguageArray = new JSONArray();
        JSONObject jsonObject = null;

        for (SearchFilterData.LanguageOption a : issuesOptions) {
//            sb.append(a.getValue());
//            sb.append(",");
//
            try {
                jsonObject = new JSONObject();
                jsonObject.put("Id", a.getValue());
                jsonObject.put("Name", a.getKey());
                jsonObject.put("Checked", a.getSelected());
                jsonObject.put("itemFor", 7);
            } catch (Exception e) {
                e.printStackTrace();
            }
            LanguageArray.put(jsonObject);
        }


        return LanguageArray;
    }

    public JSONArray getForId(ArrayList<SearchFilterData.ForOption> forOptions) {
        StringBuilder sb = new StringBuilder();

        JSONArray ForArray = new JSONArray();
        JSONObject jsonObject = null;

        for (SearchFilterData.ForOption a : forOptions) {
//            sb.append(a.getValue());
//            sb.append(",");

            try {
                jsonObject = new JSONObject();
                jsonObject.put("Id", a.getValue());
                jsonObject.put("Name", a.getKey());
                jsonObject.put("Checked", a.getSelected());
                jsonObject.put("itemFor", 2);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ForArray.put(jsonObject);
        }


        return ForArray;
    }

    public JSONArray getGenderPreferenceId(ArrayList<SearchFilterData.GenderOption> forOptions) {
        StringBuilder sb = new StringBuilder();

        JSONArray GenderPreferenceArray = new JSONArray();
        JSONObject jsonObject = null;

        for (SearchFilterData.GenderOption a : forOptions) {
//            sb.append(a.getValue());
//            sb.append(",");

            try {
                jsonObject = new JSONObject();
                jsonObject.put("Id", a.getValue());
                jsonObject.put("Name", a.getKey());
                jsonObject.put("Checked", a.getSelected());
                jsonObject.put("itemFor", 6);
            } catch (Exception e) {
                e.printStackTrace();
            }
            GenderPreferenceArray.put(jsonObject);
        }


        return GenderPreferenceArray;
    }

    public JSONArray getLocationOptionId(ArrayList<SearchFilterData.LocationOption> forOptions) {
        StringBuilder sb = new StringBuilder();

        JSONArray LocationOptionArray = new JSONArray();
        JSONObject jsonObject = null;


//        for (int i = 0; i < forOptions.size(); i++) {
//            sb.append(forOptions.get(i).getValue());
//            if ((forOptions.size() - 1) != i) {
//                sb.append(",");
//            }
//        }

        for (SearchFilterData.LocationOption a : forOptions) {
//            sb.append(a.getValue());
//            sb.append(",");

            try {
                jsonObject = new JSONObject();
                jsonObject.put("Id", a.getValue());
                jsonObject.put("Name", a.getKey());
                jsonObject.put("Checked", a.getSelected());
                jsonObject.put("itemFor", 8);
            } catch (Exception e) {
                e.printStackTrace();
            }
            LocationOptionArray.put(jsonObject);
        }

        return LocationOptionArray;
    }


    public JSONArray getTherapyId(ArrayList<Profile.TherapistTypesOfTherapy> forOptions) {
        StringBuilder sb = new StringBuilder();

        JSONArray LocationOptionArray = new JSONArray();
        JSONObject jsonObject = null;


//        for (int i = 0; i < forOptions.size(); i++) {
//            sb.append(forOptions.get(i).getValue());
//            if ((forOptions.size() - 1) != i) {
//                sb.append(",");
//            }
//        }

        for (Profile.TherapistTypesOfTherapy typesOfTherapy : forOptions) {
//            sb.append(a.getValue());
//            sb.append(",");

            try {
                jsonObject = new JSONObject();
                jsonObject.put("Id", typesOfTherapy.getId());
                jsonObject.put("Name", typesOfTherapy.getName());
                jsonObject.put("Checked", typesOfTherapy.getChecked());
                jsonObject.put("itemFor", typesOfTherapy.getItemFor());
            } catch (Exception e) {
                e.printStackTrace();
            }
            LocationOptionArray.put(jsonObject);
        }

        return LocationOptionArray;
    }

    private int getIndex(ArrayList<GenderModel> spinner, int myString) {
        for (int i = 0; i < spinner.size(); i++) {
            if (spinner.get(i).getGenderId().equalsIgnoreCase(myString + "")) {
                return i;
            }
        }

        return 0;
    }

}
