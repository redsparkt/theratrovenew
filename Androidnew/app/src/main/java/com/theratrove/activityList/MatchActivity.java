package com.theratrove.activityList;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.theratrove.activityList.Adapter.ViewPagerAdapter;
import com.theratrove.R;
import com.theratrove.activityList.appUtils.AppSharedPreferences;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.model.FilterDataModel;
import com.theratrove.activityList.model.Profile;
import com.theratrove.activityList.model.SearchData;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatchActivity extends AppCompatActivity {

    TextView txttitle;
    ImageView imeMenu, imgSearch;

    TabLayout tabs;
    ViewPager viewPager;

    SearchData searchData;

    ApiInterface apiService;
//    public static String keyword = "", SelectedFor = "", SelectedFaith = "", SelectedGender = "", SelectedIssues = "", SelectedInsurance = "", SelectedLanguage = "",
//            MaxDistance = "", ZipCode = "", page = "1", pageSize = "25";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            imeMenu = toolbar.findViewById(R.id.imgBack);
//            imeMenu.setImageResource(R.drawable.menu);
            txttitle = toolbar.findViewById(R.id.txttitle);
            txttitle.setText(getResources().getString(R.string.match));
            imgSearch = toolbar.findViewById(R.id.imgSearch);
//            imgSearch.setVisibility(View.GONE);
        }

        if (getIntent().hasExtra(Constant.DATA)) {
            searchData = (SearchData) getIntent().getSerializableExtra(Constant.DATA);
        }

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MatchActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });


        apiService = ApiClient.getClient().create(ApiInterface.class);

        tabs = findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewPager);


        imeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (CommonValidation.checkInternetConnection(this)) {

            if (getIntent().hasExtra("Keyword")) {
                FilterDataModel filterDataModel = new FilterDataModel();
                filterDataModel.keyword = getIntent().getStringExtra("Keyword");
                filterDataModel.SelectedFor = getIntent().getStringExtra("SelectedFor");
                filterDataModel.SelectedFaith = getIntent().getStringExtra("SelectedFaith");
                filterDataModel.SelectedGender = getIntent().getStringExtra("SelectedGender");

                filterDataModel.SelectedIssues = getIntent().getStringExtra("SelectedIssues");

                filterDataModel.SelectedInsurance = getIntent().getStringExtra("SelectedInsurance");
                filterDataModel.SelectedLanguage = getIntent().getStringExtra("SelectedLanguage");
                filterDataModel.MaxDistance = getIntent().getStringExtra("MaxDistance");
                filterDataModel.ZipCode = getIntent().getStringExtra("ZipCode");

                ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), filterDataModel);
                viewPager.setOffscreenPageLimit(3);
                viewPager.setAdapter(viewPagerAdapter);
                tabs.setupWithViewPager(viewPager);
            }


        } else {
            CommonValidation.showToast(this, getString(R.string.no_internet));
        }


    }


}
