//
//  UpdateGallery.swift
//  Theratrove
//
//  Created by Dhruv Patel on 31/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class UpdateGallery: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet var topview_height: NSLayoutConstraint!
    var pickerImage : UIImagePickerController = UIImagePickerController()
    var image : UIImage!
    var therapitId = Int()
    var dictData = NSDictionary()
    @IBOutlet var clwGallery: UICollectionView!
    var arrGallery = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Device.DeviceType.IS_IPHON_X{
            self.topview_height.constant = 80
        }
        
        pickerImage.delegate = self
        self.arrGallery.addObjects(from: self.dictData.object(forKey: "TherapyGalleryList") as! [Any])
        therapitId = self.dictData.object(forKey: "UserId") as! Int
        
        self.clwGallery.delegate = self
        self.clwGallery.dataSource = self
        self.clwGallery.reloadData()

        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func UpdateGalleryClick(_ sender: Any) {
        let alert = UIAlertController(title: "Select One", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Choose From Gallery", style: UIAlertActionStyle.default, handler: { (action) in
            
            let pickerView = UIImagePickerController()
            pickerView.allowsEditing = true
            pickerView.delegate = self
            pickerView.sourceType = .photoLibrary
            self.present(pickerView, animated: true)
            
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (action) in
            let pickerView = UIImagePickerController()
            pickerView.allowsEditing = true
            pickerView.delegate = self
            pickerView.sourceType = .camera
            self.present(pickerView, animated: true)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        self.image = info[UIImagePickerControllerEditedImage] as! UIImage
        let compressData = UIImageJPEGRepresentation(self.image, 0.5) //max value is 1.0 and minimum is 0.0
        self.image = UIImage(data: compressData!)
        dismiss(animated: true, completion: nil)
        Utils().ShowSKLoader()
        perform(#selector(UploadPhoto), with: nil, afterDelay:1)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Filename() -> String{
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let time1 = calendar.component(.hour, from: date)
        let time2 = calendar.component(.minute, from: date)
        let time3 = calendar.component(.second, from: date)
        
        return "Profile_\(day)\(month)\(year)\(time1)\(time2)\(time3).jpg"
    }
  
    @objc func UploadPhoto(){
        
         var parameter : [String : Any] = [:]
        let data = UIImageJPEGRepresentation(self.image, 0.7)
        let fileName = self.Filename()
        
        parameter.updateValue(fileName, forKey: "PictureFile")
        parameter.updateValue(self.therapitId, forKey: "TherapistId")
        parameter.updateValue(self.getArrayOfBytesFromImage(imageData: data! as NSData), forKey: "PictureFile")
        
        WebService.postMethod(params: parameter as [String : AnyObject], apikey: Constants.GalleryPicUpload, completion: { (Json) in
            
            let Data : NSDictionary = Json as! NSDictionary
            DispatchQueue.main.async {
                
            
            let urlImage = Data.value(forKey: "urlImage") as! String
            let GalleryId = Data.value(forKey: "therapyGalleryId") as! Int
            
            let dictionary = NSMutableDictionary()
            dictionary.setValue(urlImage, forKey: "GalleryImageURL")
            dictionary.setValue("", forKey: "CreateDateTime")
            dictionary.setValue(self.therapitId, forKey: "UserId")
            dictionary.setValue(GalleryId, forKey: "Id")
            
                Utils().HideSKLoader()
            self.arrGallery.add(dictionary)
                self.clwGallery.delegate = self
                self.clwGallery.dataSource = self
                self.clwGallery.reloadData()
            }
            
            
        }, failure: {(Error) in
            
        })
    }
    
    
    
   func getArrayOfBytesFromImage(imageData:NSData) -> NSMutableArray {
    
    let count = imageData.length / MemoryLayout<UInt8>.size
    var bytes = [UInt8](repeating: 0, count: count)
    imageData.getBytes(&bytes, length:count)
    let byteArray:NSMutableArray = NSMutableArray()
    
    for i in 0..<count{
        byteArray.add(NSNumber(value: bytes[i]))
    }
        return byteArray
    }
    
    @objc func onDeleteTapped(sender: UIButton){
        let ImageData = self.arrGallery.object(at:sender.tag) as! NSDictionary
        let PictureId = ImageData.value(forKey: "Id") as! Int
        self.DeletePhoto(PictureId: PictureId,Index: sender.tag)
    }
    
    func DeletePhoto(PictureId : Int,Index : Int){
        var parameter : [String : Any] = [:]
        parameter.updateValue(self.therapitId, forKey: "TherapistId")
        parameter.updateValue(PictureId, forKey: "PictureId")
        Utils().ShowLoader()
        WebService.postMethod(params: parameter as [String : AnyObject], apikey: Constants.DeleteGalleryPic, completion: { (Json) in
            DispatchQueue.main.async {
                self.arrGallery.removeObject(at: Index)
                self.clwGallery.delegate = self
                self.clwGallery.dataSource = self
                self.clwGallery.reloadData()
            }
            
            
        }, failure: {(Error) in
            
        })
    }
    

}
extension UpdateGallery : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrGallery.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
        let ImageData = self.arrGallery.object(at: indexPath.row) as! NSDictionary
        let value = ImageData.value(forKey: "GalleryImageURL") as! String
        cell.imgContent.moa.url = value
        
        cell.bthDelete.addTarget(self, action: #selector(self.onDeleteTapped(sender:)), for: UIControlEvents.touchUpInside)
        cell.bthDelete.tag = indexPath.row
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: (self.clwGallery.frame.size.width / 2) - 10, height: 169)
        return size
        
    }
}
