//
//  MostPopular.swift
//  Theratrove
//
//  Created by Dhruv Patel on 17/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class MostPopular: UIViewController {
    var Parameter : [String : Any] = [:]
    var response = NSMutableArray()
    var ApiCalling = Int()
    var PageCount = Int()
    var NoRecordFound = Int()
    @IBOutlet var tblMostPopular: UITableView!
     var parentpage = MatchPage()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ApiCalling = 0
        self.PageCount = 1
        self.NoRecordFound = 0
        self.ProfileData()
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height {
            if self.ApiCalling == 0 && self.NoRecordFound == 0{
                self.ProfileData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func onCallTapped(sender: UIButton){
        let ImageData = self.response.object(at:sender.tag) as! NSDictionary
        let Phone = ImageData.value(forKey: "PhoneNumber") as! String
        if !Phone.isEmpty{
            self.call(phoneNumber: Phone)
        }
 
    }
    
    func call(phoneNumber:String) {
        let cleanPhoneNumber = phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        let urlString:String = "tel://\(cleanPhoneNumber)"
        if let phoneCallURL = URL(string: urlString) {
            if (UIApplication.shared.canOpenURL(phoneCallURL)) {
                UIApplication.shared.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func onEmailTapped(sender: UIButton){
        let ImageData = self.response.object(at:sender.tag) as! NSDictionary
        let Email = ImageData.value(forKey: "Email") as! String
        if !Email.isEmpty{
            
            let url = URL(string: "mailto:\(Email)")
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        }
    }
    
    
   
    
    func ProfileData(){
        self.Parameter.updateValue(self.PageCount, forKey: "PageNo")
        self.ApiCalling = 1
        Utils().ShowLoader()
       
        WebService.postMethod(params: self.Parameter as [String : AnyObject], apikey: Constants.SearchByRating, completion: { (Json) in
            let Data : NSDictionary = Json as! NSDictionary
            DispatchQueue.main.async {
                let temparray = NSMutableArray()
                temparray.addObjects(from: Data.object(forKey: "Result") as! [Any])
                
                if temparray.count>0{
                    self.PageCount = self.PageCount + 1
                    self.response.addObjects(from: Data.object(forKey: "Result") as! [Any])
                
                    self.tblMostPopular.delegate = self
                    self.tblMostPopular.dataSource = self
                    self.tblMostPopular.reloadData()
                }
                else
                {
                    self.NoRecordFound = 1
                }
            }
            self.ApiCalling = 0
            
        }, failure: {(error) in
            self.ApiCalling = 0
        })
    }
}

extension MostPopular : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.response.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MyCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? BestMatchCell
        if cell == nil {
            var nib = Bundle.main.loadNibNamed("BestMatchCell", owner: self, options: nil)
            cell = nib?[0] as? BestMatchCell
        }
        cell?.selectionStyle = .none
        
        let ItemData = self.response.object(at: indexPath.row) as! NSDictionary
        
        cell?.selectionStyle = .none
        cell?.uvwRating.backgroundColor = UIColor.clear
        cell?.uvwRating.delegate = self as FloatRatingViewDelegate
        cell?.uvwRating.contentMode = UIViewContentMode.scaleAspectFit
        cell?.uvwRating.type = .halfRatings
        cell?.uvwRating.rating = 3
        
        let therapistRating = ItemData.value(forKey: "TherapistRating") as! Double
        cell?.uvwRating.rating = therapistRating

        
        var firstname = ItemData.value(forKey: "FirstName") as? String
        var lastname = ItemData.value(forKey: "LastName") as? String
        if firstname == nil || firstname == "-"{
            firstname = ""
        }
        if lastname == nil || lastname == "-"{
            lastname = ""
        }
        if (firstname?.isEmpty)! && (lastname?.isEmpty)!{
            firstname = ItemData.value(forKey: "Email") as? String
        }
        cell?.lblUserName.text = "\(firstname!) \(lastname!)"
        
        var Title = ItemData.value(forKey: "Title") as? String
        if Title == nil || Title == "-"{
            Title = ""
        }
        else {
            Title = Title?.uppercased()
        }
        
        
        cell?.lblType.text = "\(Title!)"
        let aboutme = ItemData.value(forKey: "AboutMe") as? String
        cell?.lblAbout.text = aboutme?.htmlToString
        
        var city = ItemData.value(forKey: "City") as? String
        var State = ItemData.value(forKey: "State") as? String
        if State == nil || State == "-"{
            State = ""
        }
        if city == nil || city == "-"{
            city = ""
        }
        
        cell?.lblAddress.text = "\(city!) \(State!)"
        
        
        let avgcost = ItemData.value(forKey: "AvgCost") as? Int
        if avgcost != nil{
            
            cell?.lblCost.text = "$\(avgcost!)"
        }
        else{
            cell?.lblCost.text = ""
        }
        
        
        var Image = ItemData.object(forKey: "ProfilePicURL") as? String
        if Image == nil{
            Image = ""
        }
        
        if !(Image?.isEmpty)!
        {
            cell?.imgUserImage.moa.url = Image
        }
        else
        {
            cell?.imgUserImage.image = UIImage(named: "user-icon.png")
        }
        cell?.btnCall.addTarget(self, action: #selector(self.onCallTapped(sender:)), for: UIControlEvents.touchUpInside)
        cell?.btnCall.tag = indexPath.row
        
        cell?.btnMsg.addTarget(self, action: #selector(self.onEmailTapped(sender:)), for: UIControlEvents.touchUpInside)
        cell?.btnMsg.tag = indexPath.row
        
        var Phone = ItemData.value(forKey: "PhoneNumber") as? String
        if Phone == nil{
            Phone = ""
        }
        if (Phone?.isEmpty)!{
            cell?.btncall_width.constant = 0
            cell?.btnCall.isHidden = true
        }
        else
        {
            cell?.btncall_width.constant = 27
            cell?.btnCall.isHidden = false
        }
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let Profile = storyboard.instantiateViewController(withIdentifier: "Profile") as? Profile
        let ItemData = self.response.object(at: indexPath.row) as! NSDictionary
        let userid = ItemData.value(forKey: "Id") as! Int
        Profile?.forDetail = "1"
        Profile?.UserId = "\(userid)"
        parentpage.navigationController?.pushViewController(Profile!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        let ItemData = self.response.object(at: indexPath.row) as! NSDictionary
        
        var Title = ItemData.value(forKey: "Title") as? String
        if Title == nil{
            Title = ""
        }
        
        if Title != nil && !(Title?.isEmpty)!{
            var labelHeight = CGFloat()
            labelHeight = Utils().height(forText: Title, font: UIFont.systemFont(ofSize: 13), withinWidth: self.view.frame.size.width - 90)
            
            return labelHeight + 140
        }
        else {
            return 138
            
        }
        
    }
}

extension MostPopular: FloatRatingViewDelegate {
    
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        //        liveLabel.text = String(format: "%.2f", self.floatRatingView.rating)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        //        updatedLabel.text = String(format: "%.2f", self.floatRatingView.rating)
    }
    
}
