//
//  SignUpPage.swift
//  Theratrove
//
//  Created by Dhruv Patel on 17/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class SignUpPage: UIViewController {

    @IBOutlet var uvwtopview_height: NSLayoutConstraint!
    @IBOutlet var uvwContent: UIView!
    @IBOutlet var btnSignIn: UIButton!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var btnCompanyTerm: UIButton!
    @IBOutlet var btnAgree: UIButton!
    @IBOutlet var txtConfirmPswd: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var imgLogo: UIImageView!
    @IBOutlet var imgBackground: UIImageView!
    
    var isAgree = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if Device.DeviceType.IS_IPHONE_5_OR_LESS{
            self.imgBackground.image = UIImage(named: "background_5")
        }
        else if Device.DeviceType.IS_IPHONE_6{
            self.imgBackground.image = UIImage(named: "background_6")
        }
        else if Device.DeviceType.IS_IPHONE_6P{
            self.imgBackground.image = UIImage(named: "background_plus")
        }
        else{
            self.imgBackground.image = UIImage(named: "background_x")
            self.uvwtopview_height.constant = 80
        }
        

        self.isAgree = false
        self.txtUserName.setLeftPaddingPoints(5)
        self.txtPassword.setLeftPaddingPoints(5)
        self.txtEmail.setLeftPaddingPoints(5)
        self.txtConfirmPswd.setLeftPaddingPoints(5)
        
        self.txtUserName.CornerRadious(5)
        self.txtPassword.CornerRadious(5)
        self.txtEmail.CornerRadious(5)
        self.txtConfirmPswd.CornerRadious(5)
        
        self.btnSignIn.underline()
        self.btnCompanyTerm.underline()
        
        self.uvwContent.layer.cornerRadius = 5
        self.uvwContent.layer.masksToBounds = true
        
        
        self.btnSignUp.layer.cornerRadius = 5
        self.btnSignUp.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }

    @IBAction func btnAgreeClick(_ sender: Any) {
        if isAgree{
            self.isAgree = !self.isAgree
            self.btnAgree.setImage(UIImage(named: "uncheck_signup"), for: .normal)
        }
        else
        {
            self.isAgree = !self.isAgree
            self.btnAgree.setImage(UIImage(named: "check_signup"), for: .normal)
           
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTermsClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let webview = storyboard.instantiateViewController(withIdentifier: "webview") as? webview
        webview?.Title = "Terms and Conditions"
        self.navigationController?.pushViewController(webview ?? UIViewController(), animated: true)
    }
    func SignupClick(){
        self.view.endEditing(true)
        let username = self.txtUserName.text
        let email = self.txtEmail.text
        let confirmpwd = self.txtConfirmPswd.text
        let password = self.txtPassword.text
        
        if (username?.isEmpty)!{
            Utils().showMessage("User Name is required.")
            return
        }
        
        let email1 = Validation().email(email!)
        if(email1[0] as? String == "false"){
            Utils().showMessage(email1[1] as! String)
            return
        }
        
        let passw = Validation().loginPassword(password!)
        if(passw[0] as? String == "false"){
            Utils().showMessage(passw[1] as! String)
            return
        }
        
        let password1 = Validation().password(password!)
        if(password1[0] as? String == "false"){
            Utils().showMessage(password1[1] as! String)
            return
        }
        
        if (confirmpwd?.isEmpty)!{
            Utils().showMessage("Confirm Password is required.")
            return
        }
        
        if confirmpwd != password{
            Utils().showMessage("Pasword and Confirm Password must be same.")
            return
        }
        
        if !isAgree{
            Utils().showMessage("Please agree to the company term of Use.")
            return
        }
        
        
        let parameter : [String : Any] = [
            "userName": username!,
            "password": password!,
            "email": email!,
            "confirmPassword": confirmpwd!
        ]
        Utils().ShowLoader()
        WebService.postMethod(params: parameter as [String : AnyObject] , apikey: Constants.Signup, completion: { (Json) in
            
            let Dict : NSDictionary = Json as! NSDictionary
            DispatchQueue.main.async {
                Utils().showMessage("You have been successfully registered. Activation link has been sent to your register email address.")
                self.navigationController?.popViewController(animated: true)
            }
            
        }, failure: {(error) in
            
            
        })
        
    }
    
    @IBAction func btnSignUpClick(_ sender: Any) {
        self.SignupClick()
    }
    @IBAction func btnSignInClick(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
