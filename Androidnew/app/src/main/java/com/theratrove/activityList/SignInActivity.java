package com.theratrove.activityList;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.theratrove.BuildConfig;
import com.theratrove.activityList.appUtils.AppSharedPreferences;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;
import com.theratrove.R;

import java.io.IOException;

import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theratrove.activityList.appUtils.Constant.DEVICE_TYPE;

public class SignInActivity extends AppCompatActivity implements Callback<JsonObject> {


    TextView txtSignIn, txtSignUp, txtForgotPwd, txtSkip;
    EditText edtUserName, edtPassword;
    ApiInterface apiService;
    HttpUrl httpUrlCallLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        initVIew();
        apiService = ApiClient.getClient().create(ApiInterface.class);



    }

    public void initVIew() {


        txtSignIn = (TextView) findViewById(R.id.txtSignIn);
        txtSignUp = (TextView) findViewById(R.id.txtSignUp);
        edtUserName = findViewById(R.id.edtUserName);
        edtPassword = findViewById(R.id.edtPassword);
        txtSkip = findViewById(R.id.txtSkip);
        txtForgotPwd = (TextView) findViewById(R.id.txtForgotPwd);
        txtForgotPwd.setPaintFlags(txtForgotPwd.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

//        if (BuildConfig.DEBUG) {
//            edtUserName.setText("Redspark 026");
//            edtPassword.setText("Redspark1234");
//        }

        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                intent.putExtra("isfrom","signin");
                startActivity(intent);
                finishAffinity();

            }
        });

        txtSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    if (CommonValidation.checkInternetConnection(SignInActivity.this)) {
                        callLoginApi(edtUserName.getText().toString().trim(), edtPassword.getText().toString().trim());
                    } else {
                        CommonValidation.showToast(SignInActivity.this, getString(R.string.no_internet));
                    }
                }

            }
        });

        txtForgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, ForgotActivity.class);
                startActivity(intent);
            }
        });


    }

    public void callLoginApi(String userName, String password) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(Constant.LOGIN__USER_KEY, userName);
        jsonObject.addProperty(Constant.LOGIN__PASSWORD_KEY, password);
        jsonObject.addProperty(Constant.LOGIN__DEVICE_TOKEN, "");
        jsonObject.addProperty(Constant.LOGIN__DEVICE_TYPE, DEVICE_TYPE);
        Call<JsonObject> callLogin = apiService.login(jsonObject);
        httpUrlCallLogin = callLogin.request().url();
        callLogin.clone().enqueue(this);
        Utility.showProgress(SignInActivity.this);
    }

    @Override
    public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
        Utility.dismissProgress();
        if (response.isSuccessful() && response.code() == 200) {
            Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
            intent.putExtra("isfrom","signin");
            startActivity(intent);
            finishAffinity();
            JsonObject jsonObject = response.body();
            if (!jsonObject.get("ProfilePic").isJsonNull()) {
                AppSharedPreferences.getSharePreference(this).setProfilePic(jsonObject.get("ProfilePic").getAsString());
            }
            if (!jsonObject.get("UserName").isJsonNull()) {
                AppSharedPreferences.getSharePreference(this).setUserName(jsonObject.get("UserName").getAsString());
            }
            if (!jsonObject.get("UserId").isJsonNull()) {
                AppSharedPreferences.getSharePreference(this).setUserID(jsonObject.get("UserId").getAsString());
            }
            if (!jsonObject.get("Email").isJsonNull()) {
                AppSharedPreferences.getSharePreference(this).setUserEmail(jsonObject.get("Email").getAsString());
            }




            AppSharedPreferences.getSharePreference(this).setTherapistLogin(true);
        } else {
            try {
                CommonValidation.showToast(SignInActivity.this, response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
        Utility.dismissProgress();
        t.printStackTrace();
    }

    public boolean isValidate() {
        if (edtUserName.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(this, getString(R.string.username_blank_msg));
            return false;
        } else if (edtPassword.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(this, getString(R.string.passworde_blank_msg));
            return false;
        }
        return true;
    }
}
