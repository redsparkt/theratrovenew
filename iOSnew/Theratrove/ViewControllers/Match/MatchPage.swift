//
//  MatchPage.swift
//  Theratrove
//
//  Created by Dhruv Patel on 17/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit
import PageMenu


class MatchPage:UIViewController, CAPSPageMenuDelegate {
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    var FromSeacrhPage = String()
    var Response = NSMutableArray()
    var MainDict = NSDictionary()
    var Parameter : [String : Any] = [:]
    @IBOutlet var uvwtopview_height: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Device.DeviceType.IS_IPHON_X{
            self.uvwtopview_height.constant = 80
        }
        
        if self.FromSeacrhPage == "1"{
            self.Initialization()
        }
        else{
           self.DirectHome()
        }

        
    }
    
    func DirectHome(){
        
        self.Parameter.updateValue(1, forKey: "PageNo")
        self.Parameter.updateValue(25, forKey: "PageSize")
        
        Utils().ShowLoader()
        WebService.postMethod(params: self.Parameter as [String : AnyObject], apikey: Constants.Search, completion: { (Json) in
            let Data : NSDictionary = Json as! NSDictionary
            self.Response.addObjects(from: Data.object(forKey: "Result") as! [Any])
            self.Initialization()
            
        }, failure: {(error) in
            
        })
        
    }
    
    func Initialization()
    {
        
        print(self.Parameter)
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let controller1 = storyboard.instantiateViewController(withIdentifier: "BestMatch") as? BestMatch
        controller1?.title = "BEST MATCH"
        controller1?.parentpage = self
        controller1?.response = self.Response
        controller1?.Parameter = self.Parameter
        controllerArray.append(controller1!)
        
        
        let controller2 = storyboard.instantiateViewController(withIdentifier: "Nearest") as? Nearest
        controller2?.title = "NEAREST"
        controller2?.parentpage = self
        //controllerArray.append(controller2!)
        
        let controller3 = storyboard.instantiateViewController(withIdentifier: "MostPopular") as? MostPopular
        controller3?.title = "MOST POPULAR"
        controller3?.parentpage = self
        controller3?.Parameter = self.Parameter
        controllerArray.append(controller3!)
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0),
            .scrollMenuBackgroundColor(UIColor.white),
            .useMenuLikeSegmentedControl(true),
            .bottomMenuHairlineColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 10.0/255.0, green: 130.0/255.0, blue: 143.0/255.0, alpha: 1.0)),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.gray),
            .menuHeight(50.0),            
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize scroll menu
        if Device.DeviceType.IS_IPHON_X{
            self.uvwtopview_height.constant = 80
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 80.0, width: self.view.frame.size.width, height: self.view.frame.size.height), pageMenuOptions: parameters)
        }
        else
        {
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 60.0, width: self.view.frame.size.width, height: self.view.frame.size.height), pageMenuOptions: parameters)
        }
        
        
        
        // Optional delegate
        pageMenu!.delegate = self
        pageMenu?.currentPageIndex = 0
        self.view.addSubview(pageMenu!.view)
    }
        
    @IBAction func btnSearchClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let SearchPage = storyboard.instantiateViewController(withIdentifier: "SearchPage") as? SearchPage
        self.navigationController?.pushViewController(SearchPage ?? UIViewController(), animated: true)
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print("did move to page - \(index)")
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        print("will move to page")
    }
      

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func btnMenuClick(_ sender: Any) {
//        self.slideMenuController?.showLeft()
        self.navigationController?.popViewController(animated: true)
//        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }


}
