//
//  LoginPage.swift
//  Theratrove
//
//  Created by Dhruv Patel on 17/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit



class LoginPage: UIViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet var imgBackground: UIImageView!
    @IBOutlet var uvwImgback: UIView!
    @IBOutlet var btnSignIn: UIButton!
    @IBOutlet var btnSignup: UIButton!
    @IBOutlet var btnForgotPassword: UIButton!
    @IBOutlet var txtpassword: UITextField!
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var imgLogo: UIImageView!
    var ForSignUp = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if ForSignUp == "1"{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let SignUp = storyboard.instantiateViewController(withIdentifier: "SignUpPage") as? SignUpPage
            self.navigationController?.pushViewController(SignUp ?? UIViewController(), animated: false)
        }
        
        if Device.DeviceType.IS_IPHONE_5_OR_LESS{
            self.imgBackground.image = UIImage(named: "background_5")
        }
        else if Device.DeviceType.IS_IPHONE_6{
               self.imgBackground.image = UIImage(named: "background_6")
        }
        else if Device.DeviceType.IS_IPHONE_6P{
               self.imgBackground.image = UIImage(named: "background_plus")
        }
        else{
               self.imgBackground.image = UIImage(named: "background_x")
        }
        
       self.txtUserName.setLeftPaddingPoints(5)
       self.txtpassword.setLeftPaddingPoints(5)
        
        self.btnForgotPassword.underline()
        self.btnSignup.underline()
        
        
        
        self.txtUserName.layer.cornerRadius = 5
        self.txtUserName.layer.masksToBounds = true
        
        self.uvwImgback.layer.cornerRadius = 5
        self.uvwImgback.layer.masksToBounds = true
        
        self.txtpassword.layer.cornerRadius = 5
        self.txtpassword.layer.masksToBounds = true
        
        self.btnSignIn.layer.cornerRadius = 5
        self.btnSignIn.layer.masksToBounds = true
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func btnForgotPasswordClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ForgotPassword = storyboard.instantiateViewController(withIdentifier: "ForgotPassword") as? ForgotPassword
        self.navigationController?.pushViewController(ForgotPassword ?? UIViewController(), animated: true)
        
    }
    @IBAction func btnSignUpClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SignUp = storyboard.instantiateViewController(withIdentifier: "SignUpPage") as? SignUpPage
        self.navigationController?.pushViewController(SignUp ?? UIViewController(), animated: true)
        
        
    }
    func LoginClick(){
        self.view.endEditing(true)
        let username = self.txtUserName.text
        let password = self.txtpassword.text
        
        if (username?.isEmpty)!{
            Utils().showMessage("User Name is required.")
            return
        }
        if (password?.isEmpty)!{
            Utils().showMessage("Password is required.")
            return
        }

        let parameter : [String : Any] = [
            "userName": username!,
            "password": password!,
            "device_token":"1234",
            "device_type" : Constants.device_type
        ]
        Utils().ShowLoader()
        WebService.postMethod(params: parameter as [String : AnyObject] , apikey: Constants.Login, completion: { (Json) in
            
            let Dict : NSDictionary = Json as!  NSDictionary
            
            print("Dict ---------> \(Dict)")
            DispatchQueue.main.async {
                var profilepic = Dict.value(forKey: "ProfilePic") as? String
                if profilepic == nil{
                    profilepic = ""
                }
                Constants.appDelegate.beforLogin = 1
                let data = NSKeyedArchiver.archivedData(withRootObject: Dict)
                Constants.USERDEFAULTS.set(data, forKey: "LoggedInUserDict")
                Constants.USERDEFAULTS.set(username, forKey: "username")
                Constants.USERDEFAULTS.set(profilepic, forKey: "profilepic")
//                let skipIntro = Constants.USERDEFAULTS.value(forKey: "SkipIntro") as? Int
//                if skipIntro == 1
//                {
                    Constants.appDelegate.SetupMenu()
              //  }
//                else
//                {
//                    Constants.appDelegate.SetupIntroMenu()
//                }
            }
            
        }, failure: {(error) in
            
            
        })
        
    }
    
    @IBAction func LoginasGuest(_ sender: Any) {
        let skipIntro = Constants.USERDEFAULTS.value(forKey: "SkipIntro") as? Int
        if skipIntro == 1
        {
            Constants.appDelegate.SetupMenu()
        }
        else
        {
            Constants.appDelegate.SetupIntroMenu()
        }
        
    }
    @IBAction func btnSignInClick(_ sender: Any) {
        self.LoginClick()
        //self.appDelegate.SetupMenu()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func CornerRadious(_ cornerRadious:CGFloat){

        self.layer.cornerRadius = cornerRadious
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.masksToBounds = true

    }

}
extension UIButton {
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: text.count))
        
        self.setAttributedTitle(attributedString, for: .normal)
    }
}

