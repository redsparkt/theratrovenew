package com.theratrove.activityList.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.theratrove.activityList.HomeActivity;
import com.theratrove.activityList.MatchActivity;
import com.theratrove.R;
import com.theratrove.activityList.TermAndConditionActivity;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SearchFragment extends Fragment implements Callback<ResponseBody> {

    TextView txttitle, txtMatch;
    Activity activity;
    ApiInterface apiService;
    HttpUrl httpGetFilterData;

    public static SearchFragment newInstance() {
        SearchFragment myFragment = new SearchFragment();

        Bundle args = new Bundle();

        return myFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_search, container, false);

        txtMatch = view.findViewById(R.id.txtMatch);

        txtMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MatchActivity.class);
                startActivity(i);
            }
        });

        apiService = ApiClient.getClient().create(ApiInterface.class);
        if (CommonValidation.checkInternetConnection(activity)) {
//            callGetSearchFilterList();
        } else {
            CommonValidation.showToast(activity, getString(R.string.no_internet));
        }
        return view;
    }

//    public void callGetSearchFilterList() {
////        Call<ResponseBody> callLogin = apiService.getSearchFilterData();
//        httpGetFilterData = callLogin.request().url();
//        callLogin.clone().enqueue(this);
//        Utility.showProgress(activity);
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HomeActivity) {
            activity = (Activity) context;
        }
    }

    @Override
    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
        Utility.dismissProgress();
        if (response.isSuccessful() && response.code() == 200) {
            try {

                JSONObject jsonObject = new JSONObject(response.body().string());
                Log.e("Response===",jsonObject.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                CommonValidation.showToast(activity, response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<ResponseBody> call, Throwable t) {
        Utility.dismissProgress();
        t.printStackTrace();
    }
}