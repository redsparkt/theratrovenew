package com.theratrove.activityList.appUtils;

/*
 * Created by Redspark on 4/13/2017.
 * Endless Recycler On Scroll Listener used for pagination with recyclerview
 */

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;


import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;

public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {
    final String TAG = EndlessRecyclerOnScrollListener.class.getSimpleName();

    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = true; // True if we are still waiting for the last set of data to load.

    private int current_page = 1;

    private LinearLayoutManager mLinearLayoutManager;
    private Context context;
    private boolean reachedMax;

    protected EndlessRecyclerOnScrollListener(Context context, LinearLayoutManager linearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager;
        this.context = context;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);

        if (newState == SCROLL_STATE_IDLE) {


        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = mLinearLayoutManager.getItemCount();
        int firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
        Log.d(TAG, "onScrolled: outside");
        if (loading) {

            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        int visibleThreshold = 0;
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {

            // End has been reached
            // Do something
            if (!reachedMax) {

                current_page++;
                onLoadMore(current_page);
                loading = true;
            }
        }
    }

    public abstract void onLoadMore(int current_page);

    public void reset() {
        current_page = 0;
        previousTotal = 0;
    }

    public int getPreviousTotal() {
        return previousTotal;
    }

    public void setPreviousTotal(int previousTotal) {
        this.previousTotal = previousTotal;
    }

    public void reachedMax(boolean reachedMax) {
        this.reachedMax = reachedMax;
    }

    public boolean getReachMax() {
        return this.reachedMax;
    }
}