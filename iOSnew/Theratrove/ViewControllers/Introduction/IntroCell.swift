//
//  IntroCellCollectionViewCell.swift
//  Theratrove
//
//  Created by Dhruv Patel on 19/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class IntroCell: UICollectionViewCell {
    
    @IBOutlet var imgIntro: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
