//
//  EditProfile.swift
//  Theratrove
//
//  Created by Dhruv Patel on 28/12/18.
//  Copyright © 2018 Redspark. All rights reserved.


import UIKit

class EditProfile: UIViewController,HADropDownDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var pickerImage : UIImagePickerController = UIImagePickerController()
    var image : UIImage!
    @IBOutlet var topview_height: NSLayoutConstraint!

    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var clwType_height: NSLayoutConstraint!
    @IBOutlet var clwType: UICollectionView!
    @IBOutlet var clwIssue_height: NSLayoutConstraint!
    @IBOutlet var clwIssues: UICollectionView!
    @IBOutlet var clwInurance_height: NSLayoutConstraint!
    @IBOutlet var clwInsurance: UICollectionView!
    @IBOutlet var clwLanguage_height: NSLayoutConstraint!
    @IBOutlet var clwLanguage: UICollectionView!
    @IBOutlet var clwLocation_Height: NSLayoutConstraint!
    @IBOutlet var clwLoction: UICollectionView!
    @IBOutlet var clwGender_height: NSLayoutConstraint!
    @IBOutlet var clwGender: UICollectionView!
    @IBOutlet var clwFor_Height: NSLayoutConstraint!
    @IBOutlet var clwFor: UICollectionView!
    @IBOutlet var clwFaith_height: NSLayoutConstraint!
    @IBOutlet var clwFaith: UICollectionView!
    @IBOutlet var txtZipCode: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtAddress2: UITextField!
    @IBOutlet var txtAddress1: UITextField!
    @IBOutlet var txtAvgCost: UITextField!
    @IBOutlet var txtPhoneNumber: UITextField!
    @IBOutlet var txtVideo: UITextField!
    @IBOutlet var tvAboutMe: UITextView!
    @IBOutlet var txtLicense: UITextField!
    @IBOutlet var btnAcceptInsurance: UIButton!
    @IBOutlet var txtEducation: UITextField!
    @IBOutlet var txtYearInService: UITextField!
    @IBOutlet var uvwGender: HADropDown!
    @IBOutlet var txtTitle: UITextField!
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var imgUserProfile: UIImageView!
    
    var acceptInsu = Bool()
    var arrFaith = NSMutableArray()
    var arrFor = NSMutableArray()
    var arrGender = NSMutableArray()
    var arrLocation = NSMutableArray()
    var arrLanguage = NSMutableArray()
    var arrInsurance = NSMutableArray()
    var arrIssues = NSMutableArray()
    var arrType = NSMutableArray()
    
    
    var selectedGender = String()
    
    var arrGenderS = NSMutableArray()
    var arrGenderS_value = NSMutableArray()
    
    var MyDictionary = NSDictionary()
    
    var arrSelectedFaith = NSMutableArray()
    var arrSelectedFor = NSMutableArray()
    var arrSelectedGender = NSMutableArray()
    var arrSelectedLocation = NSMutableArray()
    var arrSelectedLanguage = NSMutableArray()
    var arrSelectedInsurance = NSMutableArray()
    var arrSelectedIssues = NSMutableArray()
    var arrSelectedType = NSMutableArray()
    
    
    
    var arrOldFaith = NSArray()
    var arrOldFor = NSArray()
    var arrOldGender = NSArray()
    var arrOldLocation = NSArray()
    var arrOldLanguage = NSArray()
    var arrOldInsurance = NSArray()
    var arrOldIssues = NSArray()
    var arrOldType = NSArray()
    
    var acceptInsurance = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uvwGender.layer.borderColor = UIColor.black.cgColor
        uvwGender.layer.borderWidth = 1.0
        uvwGender.layer.cornerRadius = 5
        uvwGender.layer.masksToBounds = true
        self.selectedGender = "0"
        self.DisplayData()
        self.SearchDetail()
        self.pickerImage.delegate = self
        
        self.imgUserProfile.layer.borderWidth = 2
        self.imgUserProfile.layer.borderColor = UIColor.white.cgColor
        self.imgUserProfile.layer.masksToBounds = true
        
    }
    
    @IBAction func btnAcceptInsuranceClick(_ sender: Any) {
        if acceptInsurance == 0{
            acceptInsurance = 1
            self.acceptInsu = true
            btnAcceptInsurance.setImage(UIImage(named: "select_checked"), for: .normal)
        }
        else
        {
            acceptInsurance = 0
            self.acceptInsu = false
            btnAcceptInsurance.setImage(UIImage(named: "select_unchecked"), for: .normal)
        }
    }
    func DisplayData(){
        
        self.arrGenderS = ["Select Gender","Female","Male"]
        
        let gender =  self.MyDictionary.value(forKey: "Gender") as? Int
        
        
        let imagedata = self.MyDictionary.value(forKey: "ProfilePic") as? String
        self.imgUserProfile.moa.url = imagedata
        
        self.uvwGender.items = self.arrGenderS as! [String]
        if gender == 1{
            self.uvwGender.title = "Male"
            self.selectedGender = "1"
        }
        else if gender == 2{
            self.uvwGender.title = "Female"
            self.selectedGender = "2"

        }
        else{
            self.uvwGender.title = "Select Gender"
            self.selectedGender = "0"
        }
        self.uvwGender.delegate = self
        acceptInsurance = (self.MyDictionary.value(forKey: "AcceptInsurance") as? Int)!

        if acceptInsurance == 0
        {
            self.acceptInsu = false
            btnAcceptInsurance.setImage(UIImage(named: "select_unchecked"), for: .normal)
        }
        else
        {
            self.acceptInsu = true
            btnAcceptInsurance.setImage(UIImage(named: "select_checked"), for: .normal)
        }
        
        let firstname = self.MyDictionary.value(forKey: "FirstName") as? String
        if firstname != nil
        {
        self.txtFirstName.text = firstname!
        }
        let lastname = self.MyDictionary.value(forKey: "LastName") as? String
        if lastname != nil
        {
        self.txtLastName.text = lastname!
        }
        let specialize = self.MyDictionary.value(forKey: "Title") as? String
        if specialize != nil
        {
        self.txtTitle.text = specialize!
        }
        let experiance = self.MyDictionary.value(forKey: "YearsInService") as? String
        if experiance != nil
        {
        self.txtYearInService.text = experiance!
        }
        let edu = self.MyDictionary.value(forKey: "Education") as? String
        if edu != nil
        {
        self.txtEducation.text = edu!
        }
        let License = self.MyDictionary.value(forKey: "License") as? String
        if License != nil
        {
        self.txtLicense.text = License!
        }
        let AboutMe = self.MyDictionary.value(forKey: "AboutMe") as? String
        if AboutMe != nil
        {
        self.tvAboutMe.text = AboutMe?.htmlToString
        }
        var vurl = self.MyDictionary.value(forKey: "VideoUrl") as? String
        if vurl == nil{
            vurl == ""
        }
        
        let Videourl = vurl
        if Videourl != nil
        {
        self.txtVideo.text = Videourl
        }
        let phone = self.MyDictionary.value(forKey: "PhoneNumber") as? String
        if phone != nil
        {
        self.txtPhoneNumber.text = phone!
        }
        let avgcost = self.MyDictionary.value(forKey: "AvgCost") as? Int
        if avgcost != nil
        {
        self.txtAvgCost.text = "\(avgcost!)"
        }
        let address1 = self.MyDictionary.value(forKey: "Address1") as? String
        if address1 != nil
        {
        self.txtAddress1.text = address1!
        }
        let address2 = self.MyDictionary.value(forKey: "Address2") as? String
        if address2 != nil
        {
         self.txtAddress2.text = address2!
        }
        let City = self.MyDictionary.value(forKey: "City") as? String
        if City != nil
        {
        self.txtCity.text = City!
        }
        let State = self.MyDictionary.value(forKey: "State") as? String
        if State != nil
        {
        self.txtState.text = State!
        }
        let ZipCode = self.MyDictionary.value(forKey: "ZipCode") as? String
        if ZipCode != nil
        {
        self.txtZipCode.text = ZipCode
        }
        
        
        let OldFaith = self.MyDictionary.value(forKey: "FaithFilter") as? String
        let OldFor = self.MyDictionary.value(forKey: "ForFilter") as? String
        let OldIssue = self.MyDictionary.value(forKey: "IssuesFilter") as? String
        let OldInsurance = self.MyDictionary.value(forKey: "InsuranceZipCodeFilter") as? String
        let OldGender = self.MyDictionary.value(forKey: "GenderPreferenceFilter") as? String
        let OldLanguage = self.MyDictionary.value(forKey: "LanguageFilter") as? String
        let OldLocation = self.MyDictionary.value(forKey: "LocationFilter") as? String
        let OldType = self.MyDictionary.value(forKey: "TherapistTypesOfTherapyFilter") as? String
        
        if OldFaith != nil && !(OldFaith?.isEmpty)!{
            
            //self.arrOldFaith = OldFaith?.components(separatedBy: ", ")! as NSArray
            self.arrOldFaith = OldFaith?.components(separatedBy: ", ") as! NSArray
        }
        if OldFor != nil && !(OldFor?.isEmpty)!{
            self.arrOldFor = OldFor?.components(separatedBy: ", ") as! NSArray
        }
        if OldIssue != nil && !(OldIssue?.isEmpty)!{
            self.arrOldIssues = OldIssue?.components(separatedBy: ", ") as! NSArray
        }
        if OldInsurance != nil && !(OldInsurance?.isEmpty)!{
            self.arrOldInsurance = OldInsurance?.components(separatedBy: ", ") as! NSArray
        }
        if OldGender != nil && !(OldGender?.isEmpty)!{
            self.arrOldGender = OldGender?.components(separatedBy: ", ") as! NSArray
        }
        if OldLanguage != nil && !(OldLanguage?.isEmpty)!{
            self.arrOldLanguage = OldLanguage?.components(separatedBy: ", ") as! NSArray
        }
        if OldLocation != nil && !(OldLocation?.isEmpty)!{
            self.arrOldLocation = OldLocation?.components(separatedBy: ", ") as! NSArray
        }
        if OldType != nil && !(OldType?.isEmpty)!{
            self.arrOldType = OldType?.components(separatedBy: ", ") as! NSArray
        }
        
    }

    @IBAction func btnSubmitClick(_ sender: Any) {
        self.EditProfile()
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func SearchDetail(){
        
        let parameter : [String : Any] = [:]
        Utils().ShowLoader()
        WebService.GetMethod(params: parameter as [String : AnyObject], apikey: Constants.GetSearchFilters, completion: { (Json) in
            let Dict : NSDictionary = Json as! NSDictionary
            
            DispatchQueue.main.async {
                self.arrFor.addObjects(from: Dict.object(forKey: "ForOptions") as! [Any])
                self.arrFaith.addObjects(from: Dict.object(forKey: "FaithOpitions") as! [Any])
                self.arrIssues.addObjects(from: Dict.object(forKey: "IssuesOptions") as! [Any])
                self.arrLocation.addObjects(from: Dict.object(forKey: "LocationOptions") as! [Any])
                self.arrInsurance.addObjects(from: Dict.object(forKey: "InsuranceOptions") as! [Any])
                self.arrGender.addObjects(from: Dict.object(forKey: "GenderOptions") as! [Any])
                self.arrLanguage.addObjects(from: Dict.object(forKey: "LanguageOptions") as! [Any])
                self.arrType.addObjects(from: self.MyDictionary.object(forKey: "SelectedTherapistTypesOfTherapy") as! [Any])
        
                if self.arrOldFaith.count>0{
                    for i in 0..<self.arrFaith.count
                    {
                        let ItemData = self.arrFaith.object(at: i) as! NSDictionary
                        let key = ItemData.value(forKey: "Key") as? String
                        for index in 0..<self.arrOldFaith.count
                        {
                            let ItemData1 = self.arrOldFaith.object(at: index) as? String
                            if (key == ItemData1){
                                self.arrSelectedFaith.add(i)
                            }
                        }
                       
                    }
                }
                
                if self.arrOldFor.count>0{
                    for i in 0..<self.arrFor.count
                    {
                        let ItemData = self.arrFor.object(at: i) as! NSDictionary
                        let key = ItemData.value(forKey: "Key") as? String
                        for index in 0..<self.arrOldFor.count
                        {
                            let ItemData1 = self.arrOldFor.object(at: index) as? String
                            if (key == ItemData1){
                                self.arrSelectedFor.add(i)
                            }
                        }
                        
                    }
                }
                
                if self.arrOldGender.count>0{
                    for i in 0..<self.arrGender.count
                    {
                        let ItemData = self.arrGender.object(at: i) as! NSDictionary
                        let key = ItemData.value(forKey: "Key") as? String
                        for index in 0..<self.arrOldGender.count
                        {
                            let ItemData1 = self.arrOldGender.object(at: index) as? String
                            if (key == ItemData1){
                                self.arrSelectedGender.add(i)
                            }
                        }
                        
                    }
                }
                
                if self.arrOldLocation.count>0{
                    for i in 0..<self.arrLocation.count
                    {
                        let ItemData = self.arrLocation.object(at: i) as! NSDictionary
                        let key = ItemData.value(forKey: "Key") as? String
                        for index in 0..<self.arrOldLocation.count
                        {
                            let ItemData1 = self.arrOldLocation.object(at: index) as? String
                            if (key == ItemData1){
                                self.arrSelectedLocation.add(i)
                            }
                        }
                        
                    }
                }
                
                if self.arrOldLanguage.count>0{
                    for i in 0..<self.arrLanguage.count
                    {
                        let ItemData = self.arrLanguage.object(at: i) as! NSDictionary
                        let key = ItemData.value(forKey: "Key") as? String
                        for index in 0..<self.arrOldLanguage.count
                        {
                            let ItemData1 = self.arrOldLanguage.object(at: index) as? String
                            if (key == ItemData1){
                                self.arrSelectedLanguage.add(i)
                            }
                        }
                        
                    }
                }
                
                if self.arrOldInsurance.count>0{
                    for i in 0..<self.arrInsurance.count
                    {
                        let ItemData = self.arrInsurance.object(at: i) as! NSDictionary
                        let key = ItemData.value(forKey: "Key") as? String
                        for index in 0..<self.arrOldInsurance.count
                        {
                            let ItemData1 = self.arrOldInsurance.object(at: index) as? String
                            if (key == ItemData1){
                                self.arrSelectedInsurance.add(i)
                            }
                        }
                        
                    }
                }
                
                if self.arrOldIssues.count>0{
                    for i in 0..<self.arrIssues.count
                    {
                        let ItemData = self.arrIssues.object(at: i) as! NSDictionary
                        let key = ItemData.value(forKey: "Key") as? String
                        for index in 0..<self.arrOldIssues.count
                        {
                            let ItemData1 = self.arrOldIssues.object(at: index) as? String
                            if (key == ItemData1){
                                self.arrSelectedIssues.add(i)
                            }
                        }
                        
                    }
                }
                
                if self.arrOldType.count>0{
                    for i in 0..<self.arrType.count
                    {
                        let ItemData = self.arrType.object(at: i) as! NSDictionary
                        let key = ItemData.value(forKey: "Checked") as? Int
                        
                        if key == 1{
                            self.arrSelectedType.add(i)
                        }
                    }
                }
                

                
                self.clwFaith_height.constant = CGFloat(self.arrFaith.count * 50)
                self.clwFaith.delegate = self
                self.clwFaith.delegate = self
                self.clwFaith.reloadData()
                
                self.clwFor_Height.constant = CGFloat(self.arrFor.count * 50)
                self.clwFor.delegate = self
                self.clwFor.delegate = self
                self.clwFor.reloadData()
                
                
                
                self.clwGender_height.constant = CGFloat(self.arrGender.count * 50)
                self.clwGender.delegate = self
                self.clwGender.delegate = self
                self.clwGender.reloadData()
                
                self.clwLocation_Height.constant = CGFloat(self.arrLocation.count * 50)
                self.clwLoction.delegate = self
                self.clwLoction.delegate = self
                self.clwLoction.reloadData()
                
                self.clwLanguage_height.constant = CGFloat(self.arrLanguage.count * 50)
                self.clwLanguage.delegate = self
                self.clwLanguage.delegate = self
                self.clwLanguage.reloadData()

                
                self.clwInurance_height.constant = CGFloat(self.arrInsurance.count * 50)
                self.clwInsurance.delegate = self
                self.clwInsurance.delegate = self
                self.clwInsurance.reloadData()
                
                self.clwIssue_height.constant = CGFloat(self.arrIssues.count * 50)
                self.clwIssues.delegate = self
                self.clwIssues.delegate = self
                self.clwIssues.reloadData()
                
                self.clwType_height.constant = CGFloat(self.arrType.count * 50)
                self.clwType.delegate = self
                self.clwType.delegate = self
                self.clwType.reloadData()
                
               
                
            }
        }, failure: {(error) in
            
        })
    }
    
    func didSelectItem(dropDown: HADropDown, at index: Int)
        {
            let value = self.arrGenderS.object(at: index) as! String
            
            if value == "Male"
            {
                self.selectedGender = "1"
            }
            else if value == "Female"{
                self.selectedGender = "2"
            }
            else{
                self.selectedGender = "0"
            }
        }
    
    
    @IBAction func UpdateGalleryClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let UpdateGallery = storyboard.instantiateViewController(withIdentifier: "UpdateGallery") as? UpdateGallery
        UpdateGallery?.dictData = self.MyDictionary
        self.navigationController?.pushViewController(UpdateGallery ?? UIViewController(), animated: true)
    }
    func didShow(dropDown: HADropDown) {
        self.view .endEditing(true)
    }
    
    @IBAction func btnProfilePicUpdate(_ sender: Any) {let alert = UIAlertController(title: "Select One", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Choose From Gallery", style: UIAlertActionStyle.default, handler: { (action) in
            
            let pickerView = UIImagePickerController()
            pickerView.allowsEditing = true
            pickerView.delegate = self
            pickerView.sourceType = .photoLibrary
            self.present(pickerView, animated: true)
            
        }))
        alert.addAction(UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: { (action) in
            let pickerView = UIImagePickerController()
            pickerView.allowsEditing = true
            pickerView.delegate = self
            pickerView.sourceType = .camera
            self.present(pickerView, animated: true)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
       
       
         self.image = info[UIImagePickerControllerEditedImage] as! UIImage
        let compressData = UIImageJPEGRepresentation(self.image, 0.5) //max value is 1.0 and minimum is 0.0
        self.image = UIImage(data: compressData!)
        dismiss(animated: true, completion: nil)
        self.imgUserProfile.image = self.image
        Utils().ShowSKLoader()
        perform(#selector(UploadPhoto), with: nil, afterDelay:0.5)
        
        
    }
    
    func Filename() -> String{
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let time1 = calendar.component(.hour, from: date)
        let time2 = calendar.component(.minute, from: date)
        let time3 = calendar.component(.second, from: date)
        
        return "Profile_\(day)\(month)\(year)\(time1)\(time2)\(time3).jpg"
    }
    
   @objc  func UploadPhoto(){
        var parameter : [String : Any] = [:]
        let newimage = Utils().resizeImage(image: self.image, height: 100, Width: 100)
        let data = UIImageJPEGRepresentation(newimage, 1.0)
        let userid = self.MyDictionary.value(forKey: "UserId") as! Int
    
        let profileName = self.Filename()
        
        parameter.updateValue(userid, forKey: "TherapistId")
        parameter.updateValue(profileName, forKey: "PictureFileName")
        parameter.updateValue(self.getArrayOfBytesFromImage(imageData: data! as NSData), forKey: "PictureFile")
        
        WebService.postMethod(params: parameter as [String : AnyObject], apikey: Constants.ProfilePicUpload, completion: { (Json) in
            Utils().HideSKLoader()
            let Data : NSDictionary = Json as! NSDictionary
            let ProfileUrl = Data.value(forKey: "profileImageURL") as! String
            Constants.USERDEFAULTS.set(ProfileUrl, forKey: "profilepic")
            
//            let outData = UserDefaults.standard.data(forKey: "LoggedInUserDict")
//            let Dict = NSKeyedUnarchiver.unarchiveObject(with: outData!) as! NSDictionary
//           
//           
//
//            Dict.setValue(ProfileUrl, forKey: "ProfilePic")
//            let data = NSKeyedArchiver.archivedData(withRootObject: Dict)
//            Constants.USERDEFAULTS.set(data, forKey: "LoggedInUserDict")
            
            NotificationCenter.default.post(name: Notification.Name("profilepicUpdate"), object: nil)
        }, failure: {(Error) in
            Utils().HideSKLoader()
        })
    }
    
    func getArrayOfBytesFromImage(imageData:NSData) -> NSMutableArray {
        
        let count = imageData.length / MemoryLayout<UInt8>.size
        var bytes = [UInt8](repeating: 0, count: count)
        imageData.getBytes(&bytes, length:count)
        let byteArray:NSMutableArray = NSMutableArray()
        
        for i in 0..<count{
            byteArray.add(NSNumber(value: bytes[i]))
        }
        return byteArray
    }
    
    func EditProfile(){
        var parameter = NSMutableDictionary()
        let userid = self.MyDictionary.value(forKey: "UserId") as! Int
        parameter.setObject(userid, forKey: "UserId" as NSCopying)
        parameter.setObject(self.selectedGender, forKey: "Gender" as NSCopying)
        let firstname = self.txtFirstName.text
        if !(firstname?.isEmpty)!{
           parameter.setObject(firstname!, forKey: "FirstName" as NSCopying)
        }
        let lastname = self.txtLastName.text
        if !(lastname?.isEmpty)!{
            parameter.setObject(lastname!, forKey: "LastName" as NSCopying)
        }
        let title = self.txtTitle.text
        if !(title?.isEmpty)!{
            parameter.setObject(title!, forKey: "Title" as NSCopying)
        }
        let service = self.txtYearInService.text
        if !(service?.isEmpty)!{
            parameter.setObject(service!, forKey: "YearsInService" as NSCopying)
        }
        let Edu = self.txtEducation.text
        if !(Edu?.isEmpty)!{
           parameter.setObject(Edu!, forKey: "Education" as NSCopying)
        }
        let License = self.txtLicense.text
        if !(License?.isEmpty)!{
            parameter.setObject(License!, forKey: "License" as NSCopying)
        }
        let AboutMe = self.tvAboutMe.text
        if !(AboutMe?.isEmpty)!{
            parameter.setObject(AboutMe!, forKey: "AboutMe" as NSCopying)
        }
        let Phone = self.txtPhoneNumber.text
        if !(AboutMe?.isEmpty)!{
            parameter.setObject(Phone!, forKey: "PhoneNumber" as NSCopying)
        }
        let video = self.txtVideo.text
        if !(video?.isEmpty)!{
            parameter.setObject(video!, forKey: "VideoUrl" as NSCopying)
        }
        let avg = self.txtAvgCost.text
        if !(avg?.isEmpty)!{
           parameter.setObject(avg!, forKey: "AvgCost" as NSCopying)
        }
        let address1 = self.txtAddress1.text
        if !(address1?.isEmpty)!{
            parameter.setObject(address1!, forKey: "Address1" as NSCopying)
        }
        let address2 = self.txtAddress2.text
        if !(address2?.isEmpty)!{
            parameter.setObject(address2!, forKey: "Address2" as NSCopying)
        }
        let City = self.txtCity.text
        if !(City?.isEmpty)!{
            parameter.setObject(City!, forKey: "City" as NSCopying)
        }
        let state = self.txtState.text
        if !(AboutMe?.isEmpty)!{
            parameter.setObject(state!, forKey: "State" as NSCopying)
        }
        
        let zipcode = self.txtZipCode.text
        if !(zipcode?.isEmpty)!{
            parameter.setObject(zipcode!, forKey: "ZipCode" as NSCopying)
        }
        parameter.setObject("\(self.acceptInsu)", forKey: "AcceptInsurance" as NSCopying)
        
        let arrSelectedFaithDict = NSMutableArray()
        
        
        for index in 0..<self.arrFaith.count{
            let ItemData = self.arrFaith.object(at: index) as! NSDictionary
            let KeyValue = ItemData.value(forKey: "Value") as! Int
            let Key = ItemData.value(forKey: "Key") as! String
            let NSDictionary = NSMutableDictionary()
            if self.arrSelectedFaith.contains(index)
            {
                NSDictionary.setObject(KeyValue, forKey: "Id" as NSCopying)
                NSDictionary.setObject(Key, forKey: "Name" as NSCopying)
                NSDictionary.setObject("true", forKey: "Checked" as NSCopying)
                NSDictionary.setObject("1", forKey: "itemFor" as NSCopying)
            }
            else
            {
                NSDictionary.setObject(KeyValue, forKey: "Id" as NSCopying)
                NSDictionary.setObject(Key, forKey: "Name" as NSCopying)
                NSDictionary.setObject("false", forKey: "Checked" as NSCopying)
                NSDictionary.setObject("1", forKey: "itemFor" as NSCopying)
            }
            arrSelectedFaithDict.insert(NSDictionary, at: index)
            
        }
        
        parameter.setObject(arrSelectedFaithDict, forKey: "SelectedFaith" as NSCopying)
      //  parameter.updateValue(arrSelectedFaithDict, forKey: "SelectedFaith")
        print(parameter)
        
        
        
        let arrSelectedForDict = NSMutableArray()
        for index in 0..<self.arrFor.count{
            let ItemData = self.arrFor.object(at: index) as! NSDictionary
            let KeyValue = ItemData.value(forKey: "Value") as! Int
            let Key = ItemData.value(forKey: "Key") as! String
            let NSDictionary = NSMutableDictionary()
            if self.arrSelectedFor.contains(index){
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("true", forKey: "Checked")
                NSDictionary.setValue("2", forKey: "itemFor")
            }
            else{
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("false", forKey: "Checked")
                NSDictionary.setValue("2", forKey: "itemFor")
            }
            arrSelectedForDict.add(NSDictionary)
        }
        
       parameter.setObject(arrSelectedForDict, forKey: "SelectedFor" as NSCopying)
        
        
        let arrSelectedGenDict = NSMutableArray()
        
        for index in 0..<self.arrGender.count{
            let ItemData = self.arrGender.object(at: index) as! NSDictionary
            let KeyValue = ItemData.value(forKey: "Value") as! Int
            let Key = ItemData.value(forKey: "Key") as! String
            let NSDictionary = NSMutableDictionary()
            if self.arrSelectedGender.contains(index){
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("true", forKey: "Checked")
                NSDictionary.setValue("6", forKey: "itemFor")
            }
            else{
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("false", forKey: "Checked")
                NSDictionary.setValue("6", forKey: "itemFor")
            }
            arrSelectedGenDict.add(NSDictionary)
        }
       parameter.setObject(arrSelectedGenDict, forKey: "SelectedGenderPreference" as NSCopying)
        
        
        
        let arrSelectedLocationDict = NSMutableArray()
        
        for index in 0..<arrLocation.count
        {
            let ItemData = self.arrLocation.object(at: index) as! NSDictionary
            let KeyValue = ItemData.value(forKey: "Value") as! String
            let Key = ItemData.value(forKey: "Key") as! String
            let NSDictionary = NSMutableDictionary()
            if self.arrSelectedLocation.contains(index){
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("true", forKey: "Checked")
                NSDictionary.setValue("8", forKey: "itemFor")
            }
            else{
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("false", forKey: "Checked")
                NSDictionary.setValue("8", forKey: "itemFor")
            }
            arrSelectedLocationDict.add(NSDictionary)
        }
        
        
        parameter.setObject(arrSelectedLocationDict, forKey: "SelectedLocation" as NSCopying)
        
        
        let arrSelectedLanguageDict = NSMutableArray()
        
        for index in 0..<arrLanguage.count
        {
            let ItemData = self.arrLanguage.object(at: index) as! NSDictionary
            let KeyValue = ItemData.value(forKey: "Value") as! Int
            let Key = ItemData.value(forKey: "Key") as! String
            let NSDictionary = NSMutableDictionary()
            if self.arrSelectedLanguage.contains(index){
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("true", forKey: "Checked")
                NSDictionary.setValue("7", forKey: "itemFor")
            }
            else{
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("false", forKey: "Checked")
                NSDictionary.setValue("7", forKey: "itemFor")
            }
            arrSelectedLanguageDict.add(NSDictionary)
        }
        
        
        parameter.setObject(arrSelectedLanguageDict, forKey: "SelectedLanguage" as NSCopying)
        
        
        
        let arrSelectedInsuranceDict = NSMutableArray()
        for index in 0..<arrInsurance.count
        {
            let ItemData = self.arrInsurance.object(at: index) as! NSDictionary
            let KeyValue = ItemData.value(forKey: "Value") as! Int
            let Key = ItemData.value(forKey: "Key") as! String
            let NSDictionary = NSMutableDictionary()
            if self.arrSelectedInsurance.contains(index){
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("true", forKey: "Checked")
                NSDictionary.setValue("5", forKey: "itemFor")
            }
            else{
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("false", forKey: "Checked")
                NSDictionary.setValue("5", forKey: "itemFor")
            }
            arrSelectedInsuranceDict.add(NSDictionary)
        }
        parameter.setObject(arrSelectedInsuranceDict, forKey: "SelectedInsurance" as NSCopying)
        
        
        let arrSelectedIssuesDict = NSMutableArray()
        for index in 0..<arrIssues.count
        {
            let ItemData = self.arrIssues.object(at: index) as! NSDictionary
            let KeyValue = ItemData.value(forKey: "Value") as! Int
            let Key = ItemData.value(forKey: "Key") as! String
            let NSDictionary = NSMutableDictionary()
            if self.arrSelectedIssues.contains(index){
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("true", forKey: "Checked")
                NSDictionary.setValue("4", forKey: "itemFor")
            }
            else{
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("false", forKey: "Checked")
                NSDictionary.setValue("4", forKey: "itemFor")
            }
            arrSelectedIssuesDict.add(NSDictionary)
        }
        parameter.setObject(arrSelectedIssuesDict, forKey: "SelectedIssues" as NSCopying)
        
        
        let arrSelectedTypeDict = NSMutableArray()
        for index in 0..<arrType.count
        {
            let ItemData = self.arrType.object(at: index) as! NSDictionary
            let KeyValue = ItemData.value(forKey: "Id") as! Int
            let Key = ItemData.value(forKey: "Name") as! String
            let NSDictionary = NSMutableDictionary()
            if self.arrSelectedType.contains(index){
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("true", forKey: "Checked")
                NSDictionary.setValue("0", forKey: "itemFor")
            }
            else{
                NSDictionary.setValue(KeyValue, forKey: "Id")
                NSDictionary.setValue(Key, forKey: "Name")
                NSDictionary.setValue("false", forKey: "Checked")
                NSDictionary.setValue("0", forKey: "itemFor")
            }
            arrSelectedTypeDict.add(NSDictionary)
        }
       parameter.setObject(arrSelectedTypeDict, forKey: "SelectedTherapistTypesOfTherapy" as NSCopying)
        
        WebService.postMethod(params: parameter as! [String : AnyObject], apikey: Constants.EditProfile, completion: { (Json) in
            let data : NSDictionary = Json as! NSDictionary
            
            var ProfileUrl = data.value(forKey: "profileImageURL") as? String
            if ProfileUrl == nil{
                ProfileUrl = ""
            }
            Constants.USERDEFAULTS.set(ProfileUrl, forKey: "profilepic")
            Utils().showMessage("Profile Update successfully.")
            self.navigationController?.popViewController(animated: true)
            
        }, failure: {(Error) in
            
        })
        
    }
    

}



extension EditProfile: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.clwFaith{
            return self.arrFaith.count
        }
        else if collectionView == self.clwFor{
            return self.arrFor.count
        }
        else if collectionView == self.clwGender{
            return self.arrGender.count
        }
        else if collectionView == self.clwLoction{
            return self.arrLocation.count
        }
        else if collectionView == self.clwLanguage{
            return self.arrLanguage.count
        }
        else if collectionView == self.clwInsurance{
            return self.arrInsurance.count
        }
        else if collectionView == self.clwIssues{
            return self.arrIssues.count
        }
        else{
            return self.arrType.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! SearchForCell
        if collectionView == self.clwFaith{
            let ItemData = self.arrFaith.object(at: indexPath.row) as! NSDictionary
            cell.lblContent.text = ItemData.value(forKey: "Key") as? String
                if self.arrSelectedFaith.contains(indexPath.row)
                {
                        cell.imgTick.image = UIImage(named: "select_checked")
                }
                else
                {
                       cell.imgTick.image = UIImage(named: "select_unchecked")
                 }
            return cell
        }
        else if collectionView == self.clwFor{
           
            let ItemData = self.arrFor.object(at: indexPath.row) as! NSDictionary
            cell.lblContent.text = ItemData.value(forKey: "Key") as? String
             if self.arrSelectedFor.contains(indexPath.row)
             {
                 cell.imgTick.image = UIImage(named: "select_checked")
             }
             else
             {
                 cell.imgTick.image = UIImage(named: "select_unchecked")
             }
            return cell
        }
        else if collectionView == self.clwGender{
            
            let ItemData = self.arrGender.object(at: indexPath.row) as! NSDictionary
            cell.lblContent.text = ItemData.value(forKey: "Key") as! String
             if self.arrSelectedGender.contains(indexPath.row)
             {
                 cell.imgTick.image = UIImage(named: "select_checked")
             }
             else
             {
                 cell.imgTick.image = UIImage(named: "select_unchecked")
             }
            return cell
        }
        else if collectionView == self.clwLoction{
           
            let ItemData = self.arrLocation.object(at: indexPath.row) as! NSDictionary
            cell.lblContent.text = ItemData.value(forKey: "Key") as? String
             if self.arrSelectedLocation.contains(indexPath.row)
             {
                 cell.imgTick.image = UIImage(named: "select_checked")
             }
             else
             {
                 cell.imgTick.image = UIImage(named: "select_unchecked")
             }
            return cell
        }
        else if collectionView == self.clwLanguage{
            
            let ItemData = self.arrLanguage.object(at: indexPath.row) as! NSDictionary
            cell.lblContent.text = ItemData.value(forKey: "Key") as? String
             if self.arrSelectedLanguage.contains(indexPath.row)
             {
                 cell.imgTick.image = UIImage(named: "select_checked")
             }
             else
             {
                 cell.imgTick.image = UIImage(named: "select_unchecked")
             }
            return cell
        }
        else if collectionView == self.clwInsurance{
            
            let ItemData = self.arrInsurance.object(at: indexPath.row) as! NSDictionary
            cell.lblContent.text = ItemData.value(forKey: "Key") as? String
                if self.arrSelectedInsurance.contains(indexPath.row)
                {
                    cell.imgTick.image = UIImage(named: "select_checked")
                }
                else
                {
                    cell.imgTick.image = UIImage(named: "select_unchecked")
                }
            return cell
        }
        else if collectionView == self.clwIssues{
            
            let ItemData = self.arrIssues.object(at: indexPath.row) as! NSDictionary
            cell.lblContent.text = ItemData.value(forKey: "Key") as? String
                  if self.arrSelectedIssues.contains(indexPath.row)
                  {
                      cell.imgTick.image = UIImage(named: "select_checked")
                  }
                  else
                  {
                      cell.imgTick.image = UIImage(named: "select_unchecked")
                  }
            return cell
        }
        else
        {
            
            let ItemData = self.arrType.object(at: indexPath.row) as! NSDictionary
            cell.lblContent.text = ItemData.value(forKey: "Name") as? String
                        if self.arrSelectedType.contains(indexPath.row)
                        {
                            cell.imgTick.image = UIImage(named: "select_checked")
                        }
                        else
                        {
                            cell.imgTick.image = UIImage(named: "select_unchecked")
                        }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.clwFaith{
            if self.arrSelectedFaith.contains(indexPath.row){
                self.arrSelectedFaith.remove(indexPath.row)
            }
            else{
                self.arrSelectedFaith.add(indexPath.row)
            }
            
            self.clwFaith.reloadData()
        }
        else if collectionView == self.clwFor{
            if self.arrSelectedFor.contains(indexPath.row){
                self.arrSelectedFor.remove(indexPath.row)
            }
            else{
                self.arrSelectedFor.add(indexPath.row)
            }
            
            self.clwFor.reloadData()
        }
        else if collectionView == self.clwGender{
            if self.arrSelectedGender.contains(indexPath.row){
                self.arrSelectedGender.remove(indexPath.row)
            }
            else{
                self.arrSelectedGender.add(indexPath.row)
            }
            self.clwGender.reloadData()
        }
        else if collectionView == self.clwLoction{
            if self.arrSelectedLocation.contains(indexPath.row){
                self.arrSelectedLocation.remove(indexPath.row)
            }
            else{
                self.arrSelectedLocation.add(indexPath.row)
            }
            self.clwLoction.reloadData()
        }
        else if collectionView == self.clwLanguage{
            if self.arrSelectedLanguage.contains(indexPath.row){
                self.arrSelectedLanguage.remove(indexPath.row)
            }
            else{
                self.arrSelectedLanguage.add(indexPath.row)
            }
            self.clwLanguage.reloadData()
        }
        else if collectionView == self.clwInsurance{
            if self.arrSelectedInsurance.contains(indexPath.row){
                self.arrSelectedInsurance.remove(indexPath.row)
            }
            else{
                self.arrSelectedInsurance.add(indexPath.row)
            }
            self.clwInsurance.reloadData()
        }
        else if collectionView == self.clwIssues{
            if self.arrSelectedIssues.contains(indexPath.row){
                self.arrSelectedIssues.remove(indexPath.row)
            }
            else{
                self.arrSelectedIssues.add(indexPath.row)
            }
            self.clwIssues.reloadData()
        }
        else{
            if self.arrSelectedType.contains(indexPath.row){
                self.arrSelectedType.remove(indexPath.row)
            }
            else{
                self.arrSelectedType.add(indexPath.row)
            }
            self.clwType.reloadData()
        }
    }
}


