//
//  ChangePassword.swift
//  Theratrove
//
//  Created by Dhruv Patel on 28/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class ChangePassword: UIViewController {

    var username = String()
    @IBOutlet var uvwmainview: UIView!
    @IBOutlet var topview_height: NSLayoutConstraint!
    @IBOutlet var txtConfirmPassword: UITextField!
    @IBOutlet var txtPAssword: UITextField!
    @IBOutlet var txtUsername: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()


        self.uvwmainview.layer.cornerRadius = 5
        self.uvwmainview.layer.masksToBounds = true

        self.txtPAssword.setLeftPaddingPoints(5)
        self.txtConfirmPassword.setLeftPaddingPoints(5)
        
        self.txtPAssword.layer.cornerRadius = 5
        self.txtPAssword.layer.borderColor = UIColor.black.cgColor
        self.txtPAssword.layer.borderWidth = 1.0
        self.txtPAssword.layer.masksToBounds = true
        
        self.txtConfirmPassword.layer.cornerRadius = 5
        self.txtConfirmPassword.layer.borderColor = UIColor.black.cgColor
        self.txtConfirmPassword.layer.borderWidth = 1.0
        self.txtConfirmPassword.layer.masksToBounds = true
        
        
        let outData = UserDefaults.standard.data(forKey: "LoggedInUserDict")
        let Dict = NSKeyedUnarchiver.unarchiveObject(with: outData!)as!NSDictionary
        self.username = Dict.value(forKey: "UserName") as! String
        //self.txtUsername.text = username
        // Do any additional setup after loading the view.
    }

    @IBAction func btnChanegPasswordClick(_ sender: Any) {
        self.view.endEditing(true)
        
        if Device.DeviceType.IS_IPHON_X{
            self.topview_height.constant = 80
        }
        
        let outData = UserDefaults.standard.data(forKey: "LoggedInUserDict")
        let Dict = NSKeyedUnarchiver.unarchiveObject(with: outData!)as!NSDictionary
        let userid = Dict.value(forKey: "UserId")
        let email = Dict.value(forKey: "Email") as! String
        
        let password  = self.txtPAssword.text
        let confirm  = self.txtConfirmPassword.text
        
      
        let passw = Validation().loginPassword(password!)
        if(passw[0] as? String == "false"){
            Utils().showMessage(passw[1] as! String)
            return
        }
        
        let password1 = Validation().password(password!)
        if(password1[0] as? String == "false"){
            Utils().showMessage(password1[1] as! String)
            return
        }
        
        if (confirm?.isEmpty)!{
            Utils().showMessage("Please enter valid confirm password.")
            return
        }
        
        if confirm != password{
            Utils().showMessage("Password and Confirm password are diffrent.")
            return
        }
        
        let parameter : [String : Any] = [
            "UserId":userid!,
            "UserName":self.username,
            "Password":password!,
            "email":email,
            "ConfirmPassword":confirm!
        ]
        Utils().ShowLoader()
        WebService.postMethod(params: parameter as [String : AnyObject] , apikey: Constants.ChangePassword, completion: { (Json) in
           
            let Data : NSDictionary = Json as! NSDictionary
            let Message = Data.value(forKey: "Message") as! String
            
            Utils().showMessage(Message)
            
        }, failure: {(error) in
            
            
        })
        
    }
    @IBAction func btnBackClick(_ sender: Any) {
         sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
