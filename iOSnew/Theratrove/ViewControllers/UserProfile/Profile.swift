//
//  Profile.swift
//  Theratrove
//
//  Created by Dhruv Patel on 18/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit
import PageMenu
import moa

class Profile:UIViewController, CAPSPageMenuDelegate {
    var pageMenu : CAPSPageMenu?
    
    @IBOutlet var imgProfile_width: NSLayoutConstraint!
    @IBOutlet var imgProfile_height: NSLayoutConstraint!
    @IBOutlet var imgLocation: UIImageView!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnContact_height: NSLayoutConstraint!
    @IBOutlet var imgUSerImage: UIImageView!
    @IBOutlet var lblSpecialize: UILabel!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var uvwtopview_height: NSLayoutConstraint!
    @IBOutlet var uvwMain_height: NSLayoutConstraint!
    @IBOutlet var uvwMain: UIView!
    var tabTitles: NSArray = NSArray()
    var forDetail = String()
    var UserId = String()
    var VideoUrl = String()
    @IBOutlet var btnContact: UIButton!
    @IBOutlet var lblUserAddress: UILabel!
    @IBOutlet var lblUSerName: UILabel!
    var MyDetailDictionary = NSDictionary()
    var DetailResponse = NSDictionary()
    var controllerArray : [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Device.DeviceType.IS_IPHON_X{
            self.uvwtopview_height.constant = 300
        }
        
        self.btnContact.layer.cornerRadius = self.btnContact.frame.size.height / 2
        self.btnContact.layer.masksToBounds = true
        
        self.imgUSerImage.layer.borderWidth = 2
        self.imgUSerImage.layer.borderColor = UIColor.white.cgColor
        self.imgUSerImage.layer.masksToBounds = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.forDetail == "1"{
            self.btnBack.setImage(UIImage(named: "profile_back"), for: .normal)
            self.btnContact.isHidden = false
            self.btnContact_height.constant = 30
            self.ProfileData()
            self.btnEdit.isHidden = true
//            self.resizeProfileImage()
        }
        else
        {
            let outData = UserDefaults.standard.data(forKey: "LoggedInUserDict")
            self.MyDetailDictionary = NSKeyedUnarchiver.unarchiveObject(with: outData!) as! NSDictionary
            let value = self.MyDetailDictionary.value(forKey: "UserId")
            self.UserId = "\(value!)"
            self.ProfileData()
            self.btnBack.setImage(UIImage(named: "list_icon"), for: .normal)
            self.btnContact_height.constant = 0
            self.btnContact.isHidden = true
            self.btnEdit.isHidden = false
//            self.resizeProfileImage()
        }
        
        
    }
    
    @objc func resizeProfileImage()
    {
            let image = self.imgUSerImage.image
            let newImage = Utils().resizeImage(image: image!, height: 70, Width: 70)
            self.imgUSerImage.image = newImage
            self.imgProfile_height.constant = newImage.size.height
            self.imgProfile_width.constant = newImage.size.width
    }
    
    func pagedata(){
        controllerArray.removeAll()
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let controller1 = storyboard.instantiateViewController(withIdentifier: "Description") as? Description
        controller1?.title = "DESCRIPTION"
        controller1?.dictData = self.DetailResponse
        if self.forDetail == "1"{
            controller1?.fromdetail = "1"
        }
        else{
            controller1?.fromdetail = "0"
        }
        controllerArray.append(controller1!)
        
        
        let controller2 = storyboard.instantiateViewController(withIdentifier: "Focus") as? Focus
        controller2?.title = "FOCUS"
        controller2?.MyDictionary = self.DetailResponse
        
        var AllDataFor = Int()
        AllDataFor = 0
        let value =  self.DetailResponse.value(forKey: "ForFilter") as? String
        if value != nil && !(value?.isEmpty)!{
            AllDataFor = AllDataFor + 1
        }
        
        
        
        let value1 =  self.DetailResponse.value(forKey: "FaithFilter") as? String
        if value1 != nil && !(value1?.isEmpty)!{
           AllDataFor = AllDataFor + 1
        }
       
        
        
        let value2 =  self.DetailResponse.value(forKey: "GenderPreferenceFilter") as? String
        if value2 != nil && !(value2?.isEmpty)!{
            AllDataFor = AllDataFor + 1
        }
       
        
        
        let value3 =  self.DetailResponse.value(forKey: "InsuranceZipCodeFilter") as? String
        if value3 != nil && !(value3?.isEmpty)!{
             AllDataFor = AllDataFor + 1
        }
        
        
        
        let value4 =  self.DetailResponse.value(forKey: "LanguageFilter") as? String
        if value4 != nil && !(value4?.isEmpty)!{
             AllDataFor = AllDataFor + 1
        }
       
        
        
        if AllDataFor > 0{
            controllerArray.append(controller2!)
        }
        
        let controller3 = storyboard.instantiateViewController(withIdentifier: "Gallery") as? Gallery
        controller3?.title = "GALLERY"
        controller3?.dictData = self.DetailResponse
        
        let arrGallery = NSMutableArray()
        arrGallery.addObjects(from: self.DetailResponse.object(forKey: "TherapyGalleryList") as! [Any])
        
        if arrGallery.count > 0{
            controllerArray.append(controller3!)
        }
        
        let controller4 = storyboard.instantiateViewController(withIdentifier: "Video") as? Video
        controller4?.title = "VIDEO"
        controller4?.VideoUrl = self.VideoUrl
        if self.VideoUrl != nil && !(self.VideoUrl.isEmpty) && self.VideoUrl != "-"{
            controllerArray.append(controller4!)
        }
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0),
            .scrollMenuBackgroundColor(UIColor.white),
            .useMenuLikeSegmentedControl(true),
            .bottomMenuHairlineColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 10.0/255.0, green: 130.0/255.0, blue: 143.0/255.0, alpha: 1.0)),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.gray),
            .menuHeight(50.0),
            .menuItemFont(UIFont.systemFont(ofSize: 13.0)),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height), pageMenuOptions: parameters)
        
        
        // Optional delegate
        pageMenu!.delegate = self
        pageMenu?.currentPageIndex = 0
        self.uvwMain.addSubview(pageMenu!.view)
        self.uvwMain_height.constant = self.view.frame.size.height
        
        
        //perform(#selector(resizeProfileImage), with: nil, afterDelay:0.5)
    }
    
    func ProfileData(){
        let Parameter : [String : Any] = [:]
        Utils().ShowLoader()
        WebService.GetMethod(params: Parameter as [String : AnyObject], apikey: "\(Constants.UserDetail)\(self.UserId)", completion: { (Json) in
            self.DetailResponse = Json as! NSDictionary
           // self.UserId = self.DetailResponse.value(forKey: "FirstName") as? String
            var firstname = self.DetailResponse.value(forKey: "FirstName") as? String
            var lastname = self.DetailResponse.value(forKey: "LastName") as? String
            if firstname == nil || firstname == "-"{
                firstname = ""
            }
            if lastname == nil || lastname == "-"{
                lastname = ""
            }
            if (firstname?.isEmpty)! && (lastname?.isEmpty)!{
                firstname = self.DetailResponse.value(forKey: "UserName") as? String
            }
            self.lblUSerName.text = "\(firstname!) \(lastname!)"
            var address1 = self.DetailResponse.value(forKey: "Address1") as? String
            var address2 = self.DetailResponse.value(forKey: "Address2") as? String
            if address1 == nil || address1 == "-"{
                address1 = ""
            }
            if address2 == nil || address2 == "-"{
                address2 = ""
            }
            var City = self.DetailResponse.value(forKey: "City") as? String
            if City == nil || City == "-"{
                City = ""
            }
            if (address1!.isEmpty) && (address2!.isEmpty) && (City!.isEmpty){
                self.imgLocation.isHidden = true
            }
            else{
                self.imgLocation.isHidden = false
            }
            
            self.lblUserAddress.text = "\(address1!) \(address2!) \(City!)"
            
            self.lblSpecialize.text = self.DetailResponse.value(forKey: "Title") as? String
            
            var videourl = self.DetailResponse.value(forKey: "VideoUrl") as? String
            
            if videourl == nil{
                videourl = ""
            }
            else{
                self.VideoUrl = videourl!
            }
            let imagedata = self.DetailResponse.value(forKey: "ProfilePic") as? String
            //let imagedata = Constants.USERDEFAULTS.value(forKey: "ProfilePic") as? String
             if imagedata != nil && !(imagedata?.isEmpty)!{
                self.imgUSerImage.moa.url = imagedata
                Constants.USERDEFAULTS.set(imagedata, forKey: "profilepic")
                NotificationCenter.default.post(name: Notification.Name("profilepicUpdate"), object: nil)
            }
            
        
            
            self.pagedata()
           
            
        }, failure: {(error) in
            
        })
    }
    
    @IBAction func btnEditProfileClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let EditProfile = storyboard.instantiateViewController(withIdentifier: "EditProfile") as? EditProfile
        EditProfile?.MyDictionary = self.DetailResponse
        self.navigationController?.pushViewController(EditProfile ?? UIViewController(), animated: true)
    }
    func convierteImagen(cadenaImagen: String) -> UIImage? {
        var strings = cadenaImagen.components(separatedBy: ",")
        var bytes = [UInt8]()
        for i in 0..<strings.count {
            if let signedByte = Int8(strings[i]) {
                bytes.append(UInt8(bitPattern: signedByte))
            } else {
                // Do something with this error condition
            }
        }
        let datos: NSData = NSData(bytes: bytes, length: bytes.count)
        return UIImage(data: datos as Data) // Note it's optional. Don't force unwrap!!!
    }
       
    @IBAction func btnContactClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let ContactUs = storyboard.instantiateViewController(withIdentifier: "ContactUs") as? ContactUs
        ContactUs?.strTitle = "Contact"
        ContactUs?.userid = self.UserId
        self.navigationController?.pushViewController(ContactUs ?? UIViewController(), animated: true)
    }
    @IBAction func btnMenuClick(_ sender: Any) {
        if self.forDetail == "1"{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            sideMenuController?.showLeftView(animated: true, completionHandler: nil)
        }
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
       // self.slideMenuController?.showLeft()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


