package com.theratrove.activityList.Adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Rating;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.theratrove.R;
import com.theratrove.activityList.ProfileActivity;
import com.theratrove.activityList.appUtils.AppSharedPreferences;
import com.theratrove.activityList.model.SearchData;

import java.util.ArrayList;

public class BestAdapter extends RecyclerView.Adapter<BestAdapter.MyViewHolder> {

    private ArrayList<SearchData.Result> bestList;
    Activity activity;

    class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout llMain;
        TextView txtName, txtSpecial, txtDes, txtAvgcost, txtCity;
        ImageView imgProfile, imgCall, imgmessge;
        RatingBar rating;

        MyViewHolder(View view) {
            super(view);
            llMain = view.findViewById(R.id.llMain);
            txtName = view.findViewById(R.id.txtName);
            txtAvgcost = view.findViewById(R.id.txtAvgcost);
            txtSpecial = view.findViewById(R.id.txtSpecial);
            txtDes = view.findViewById(R.id.txtDes);
            imgCall = view.findViewById(R.id.imgCall);
            imgProfile = view.findViewById(R.id.imgProfile);
            rating = view.findViewById(R.id.rating);
            imgmessge = view.findViewById(R.id.imgmessge);
            txtCity = view.findViewById(R.id.txtCity);

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public BestAdapter(Activity activity, ArrayList<SearchData.Result> bestList) {
        this.bestList = bestList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_matches, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {


        if ((bestList.get(position).getFirstName() != null && bestList.get(position).getFirstName().length() > 0) ||
                (bestList.get(position).getLastName() != null &&
                        bestList.get(position).getLastName().length() > 0)) {
            holder.txtName.setText(bestList.get(position).getFirstName() + " " + bestList.get(position).getLastName());
        } else {
            holder.txtName.setText(bestList.get(position).getEmail());
        }


        holder.txtSpecial.setText(bestList.get(position).getTitle());
        if (bestList.get(position).getAboutMe() != null && bestList.get(position).getAboutMe().length() > 0) {
            holder.txtDes.setText(Html.fromHtml(bestList.get(position).getAboutMe()));
        }
        if (bestList.get(position).getProfilePicURL() != null && bestList.get(position).getProfilePicURL().length() > 0) {
            Glide.with(activity)
                    .load(bestList.get(position).getProfilePicURL())
                    .into(holder.imgProfile);
        }

        if (bestList.get(position).getAvgCost() != null) {
            holder.txtAvgcost.setText("$" + bestList.get(position).getAvgCost() + "");
        }


        holder.txtCity.setText(bestList.get(position).getCity() + " " + bestList.get(position).getState());


        holder.rating.setRating(bestList.get(position).getTherapistRating());
//        holder.rating.setEnabled(false);
        holder.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ProfileActivity.class);
                intent.putExtra("id", bestList.get(position).getId());
                activity.startActivity(intent);
            }
        });
        if (bestList.get(position).getPhoneNumber() != null && bestList.get(position).getPhoneNumber().length() > 0) {
            holder.imgCall.setVisibility(View.VISIBLE);
        } else {
            holder.imgCall.setVisibility(View.GONE);
        }

        if (bestList.get(position).getEmail() != null && bestList.get(position).getEmail().length() > 0) {
            holder.imgmessge.setVisibility(View.VISIBLE);
        } else {
            holder.imgmessge.setVisibility(View.GONE);
        }

        holder.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                if (bestList.get(position).getPhoneNumber() != null && bestList.get(position).getPhoneNumber().length() > 0) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + bestList.get(position).getPhoneNumber()));
                activity.startActivity(intent);
//                }
            }
        });

        holder.imgmessge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (bestList.get(position).getEmail() != null && bestList.get(position).getEmail().length() > 0) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", bestList.get(position).getEmail(), null));
                activity.startActivity(Intent.createChooser(emailIntent, "Send email..."));
//                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return bestList.size();
    }

    public void updateList(ArrayList<SearchData.Result> data) {
        bestList.addAll(data);
        notifyDataSetChanged();
    }
}