package com.theratrove.activityList.appUtils.connectivityChangeDetectorUtility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.theratrove.activityList.appUtils.CommonValidation;


/**
 * Created by Redspark on 12/18/2017.
 * This is a network change receiver that receives change in network connectivity
 */

public class NetworkChangeReceiver  extends BroadcastReceiver
{
  @Override public void onReceive(Context context, Intent intent)
  {
    if(CommonValidation.checkInternetConnection(context))
    {
      ((ConnectivityListener)context).connectivityChanged(true);
    }else
    {
      ((ConnectivityListener)context).connectivityChanged(false);
    }
  }
}
