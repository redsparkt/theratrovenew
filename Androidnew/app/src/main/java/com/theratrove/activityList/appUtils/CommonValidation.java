package com.theratrove.activityList.appUtils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.lang.reflect.Field;

import okhttp3.ResponseBody;


import com.theratrove.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;

import static java.lang.Integer.parseInt;

public class CommonValidation {

    public static int getHeight(Activity context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        return height;
    }

    public static void ShowLog(String stringTitle, String stringMsg) {
        Log.e(stringTitle, stringMsg);
    }

    public static int getWidth(Activity context) {
        Display mDisplay = context.getWindowManager().getDefaultDisplay();
        final int width = mDisplay.getWidth();
        return width;
    }

    public static boolean checkInternetConnection(Context activity) {
        ConnectivityManager cm = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileNetwork = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            Log.i("wifi", "connection available");
            return true;
        } else if (mobileNetwork != null && mobileNetwork.isConnected()) {
            Log.i("mobile", "connection available");
            return true;
        } else if (activeNetwork != null && activeNetwork.isConnected()) {
            Log.i("active network", "connection available");
            return true;
        } else {
            Toast.makeText(activity, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public static int dpToPx(Activity activity, int dp) {
        Resources r = activity.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public static boolean isEdittextEmpty(EditText editText) {
        if (editText.getText().toString().trim().length() > 0) {
            return false;
        }
        return true;
    }

    public static boolean isTextEmpty(TextView textView) {
        String sText = textView.getText().toString().trim();
        if (sText != null && !"".equals(sText)) {
            return false;
        }
        return true;
    }

    public static boolean isCheckboxUnselect(CheckBox checkbox) {
        if (checkbox.isChecked()) {
            return false;
        }
        return true;
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void closeKeybroad(Activity v) {
        View view = v.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm =
                    (InputMethodManager) v.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void openKeybroad(Activity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static String getStringValue(Object object) {
//        if (object instanceof TextView) {
        return ((TextView) object).getText().toString().trim();
       /* } else if (object instanceof EditText) {
            return ((EditText) object).getText().toString().trim();
        } else {*/
//            return "";
//        }
    }

    public static String checkString(String target) {
        if (target == null || target.equals("")) {
            target = "-";
        } else {

        }
        return target;
    }

    public static boolean checkWriteExternalPermission(Context context) {
        //        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public static boolean checkRecordAudioPermission(Context context) {
        //        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        String permission = Manifest.permission.RECORD_AUDIO;
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public static void setEmptyLayout(RelativeLayout rl) {
        rl.setVisibility(View.VISIBLE);
    }

    public static void launchMarket(Context context) {
        //        final String appPackageName = "com.redspark.coupon_fayda";
        final String appPackageName = context.getPackageName();
        try {
            Uri uri = Uri.parse("market://details?id=" + appPackageName);
            Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
            context.startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, " Sorry, Not able to open!", Toast.LENGTH_SHORT).show();
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static void clearFlag(Activity activity) {
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public static void setFlag(Activity activity) {
        activity.getWindow()
                .setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    // Set Flag Dialog
    public static void setDialogFlag(Dialog dialog) {
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public static void clearDialogFlag(Dialog dialog) {
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    public static boolean cancelRequest(Call<ResponseBody> callRequest, Call<ResponseBody> call) {
        if (callRequest != null && !callRequest.isCanceled() && call.equals(callRequest)) {
            return true;
        }
        return true;
    }

    // Get Time

    public static String getDuration(File file) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(file.getAbsolutePath());
        String durationStr = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        return formateMilliSeccondConvert(Long.parseLong(durationStr));
    }

    public static String formateMilliSeccond(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);

        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "00" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        //      return  String.format("%02d Min, %02d Sec",
        //                TimeUnit.MILLISECONDS.toMinutes(milliseconds),
        //                TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
        //                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));

        // return timer string
        return finalTimerString;
    }


    public static String formateMilliSeccondConvert(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);

        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "00" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = String.valueOf((hours * 3600) + (minutes * 60) + seconds);
//        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        //      return  String.format("%02d Min, %02d Sec",
        //                TimeUnit.MILLISECONDS.toMinutes(milliseconds),
        //                TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
        //                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));

        // return timer string
        return finalTimerString;
    }

    public static void fileDownload(Context mContext, String file_url, String fileName) {
        DownloadManager downloadManager =
                (DownloadManager) mContext.getSystemService(mContext.DOWNLOAD_SERVICE);
        Uri Download_Uri = Uri.parse(file_url);
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setTitle(fileName);
        request.setDestinationInExternalPublicDir(mContext.getString(R.string.app_name), fileName);
        downloadManager.enqueue(request);
    }
    // FB ID 260571917640834

    public static void setHtmlText(Object object, String htmlString) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (object instanceof EditText) {
                ((EditText) object).setText(Html.fromHtml(htmlString, Html.FROM_HTML_MODE_LEGACY));
            } else if (object instanceof TextView) {
                ((TextView) object).setText(Html.fromHtml(htmlString, Html.FROM_HTML_MODE_LEGACY));
            }
        } else {
            if (object instanceof EditText) {
                ((EditText) object).setText(Html.fromHtml(htmlString));
            } else if (object instanceof TextView) {
                ((TextView) object).setText(Html.fromHtml(htmlString));
            }
        }
    }


  /*  public static  void shareContent(Context context) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, Constant.SHARE_TITLE);
        String strShareMessage = "\nLet me recommend you this application\n\n";
        strShareMessage = strShareMessage + "https://play.google.com/store/apps/details?id=" + context.getPackageName();
//                Uri screenshotUri = Uri.parse("android.resource://com.mastrrmix/drawable/live_music.png");
        sharingIntent.setType("image/png");
//                sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, strShareMessage);
        context.startActivity(Intent.createChooser(sharingIntent, context.getResources().getString(R.string.share_title)));
    }*/


    public static boolean checkLoaction(Context context) {
//        String permission = "android.permission.WRITE_EXTERNAL_STORAGE";
        String permission = Manifest.permission.ACCESS_FINE_LOCATION;
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }


    public static boolean validEmail(EditText editText, Context context) {
        if (CommonValidation.isEdittextEmpty(editText)) {
//            editText.setError("Please enter email id.");
            Toast.makeText(context, "Please enter email id.", Toast.LENGTH_SHORT).show();
        } else {
            if (CommonValidation.isValidEmail(editText.getText().toString().trim())) {
                return true;
            } else {
//                editText.setError("Please enter valid email id.");
                Toast.makeText(context, "Please enter valid email id", Toast.LENGTH_SHORT).show();
            }
        }
        return false;
    }

    public static boolean validPhoneNumber(EditText editText, Context context) {
        if (CommonValidation.isEdittextEmpty(editText)) {
            showToast(context, "Please enter phone number");
//            editText.setError("Please enter mobile number");
        } else {
            String pwd = editText.getText().toString().trim();
            if (pwd.length() < 5 || pwd.length() > 15) {
                showToast(context, "Please enter minimum 5 & maximum 15 digit mobile number");
//                editText.setError("Please enter valid phone number");
            } else {
                return true;
            }
        }
        return false;
    }

    public static boolean validPassword(EditText editText, Context context) {
        if (CommonValidation.isEdittextEmpty(editText)) {
//            editText.setError("Please enter password.");
            showToast(context, "Please enter password.");
        } else {
            String pwd = editText.getText().toString().trim();
            if (pwd.length() > 5) {
                return true;
            } else {
//                editText.setError("Please enter minimum 6 character");
                showToast(context, "Please enter minimum 6 character");
            }
        }
        return false;
    }

    public static boolean validCompareConfirmPassword(EditText newPwdEditText, EditText confirmPwdEditText, Context context) {
        String pwd = newPwdEditText.getText().toString().trim();
        String confirmPwd = confirmPwdEditText.getText().toString().trim();


        if (CommonValidation.isEdittextEmpty(newPwdEditText)) {
            showToast(context, "Please enter password.");
        } else if (CommonValidation.isEdittextEmpty(confirmPwdEditText)) {
            showToast(context, "Please enter confirm password");
        } else {
//            String pwd = newPwdEditText.getText().toString().trim();

            if (pwd.length() < 5) {
                newPwdEditText.setError("Please enter minimum 6 character password");
            } else if (pwd.equals(confirmPwd)) {
                return true;
            } else {
//                confirmPwdEditText.setError("Please enter both password same");
                showToast(context, "Please enter both password same");
            }
        }
        return false;
    }


    public static void setSpinnerDropDownHeight(Spinner spinner, int height) {
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow =
                    (android.widget.ListPopupWindow) popup.get(spinner);

            // Set popupWindow height to 500px
            popupWindow.setHeight(height);
        } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }
    }


    public static String convertTwoPlacesForInteger(String num) {
        int number;
        if (parseInt(num) > 9999f) {
            number = parseInt(num.substring(0, 5));
        } else {
            number = parseInt(num);
        }
        String text = (number < 10 ? "0" : "") + number;
        return text.substring(0, 2);
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {


            ssb.setSpan(new MySpannable(false) {
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "See Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, ".. See More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }

    @SuppressLint("SimpleDateFormat")
    public static String ConvertDateFormate(String date) {


        @SuppressLint("SimpleDateFormat") SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date newDate;
        try {
            newDate = spf.parse(date);
            spf = new SimpleDateFormat("dd MMM yyyy hh:mm");
            date = spf.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,12}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }
}
