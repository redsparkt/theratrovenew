package com.theratrove.activityList.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.theratrove.activityList.Fragment.BestFragment;
import com.theratrove.activityList.Fragment.MostPopularFragment;
import com.theratrove.activityList.model.FilterDataModel;
import com.theratrove.activityList.model.SearchData;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private FilterDataModel filterDataModel;
    private SearchData mostPuplar;

    public ViewPagerAdapter(FragmentManager fm, FilterDataModel filterDataModel) {
        super(fm);
        this.filterDataModel = filterDataModel;
    }

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = BestFragment.newInstance(filterDataModel);
        } else if (position == 1) {
            fragment = MostPopularFragment.newInstance(filterDataModel);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "BEST MATCH";
        } else if (position == 1) {
            title = "MOST POPULAR";
        } else if (position == 2) {
            title = "MOST POPULAR";
        }
        return title;
    }
}
