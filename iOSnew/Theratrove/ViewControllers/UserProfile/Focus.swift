//
//  Focus.swift
//  Theratrove
//
//  Created by Dhruv Patel on 18/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class Focus: UIViewController {

    @IBOutlet var lblGenlbl_height: NSLayoutConstraint!
    @IBOutlet var lblinslbl_height: NSLayoutConstraint!
    @IBOutlet var lbllanlb_height: NSLayoutConstraint!
    @IBOutlet var lblforlbl_height: NSLayoutConstraint!
    @IBOutlet var lblfaithlbl_height: NSLayoutConstraint!
    @IBOutlet var lblGenderlbl: UILabel!
    @IBOutlet var lblInsurancelbl: UILabel!
    @IBOutlet var lblLanguagelbl: UILabel!
    @IBOutlet var lblForlbl: UILabel!
    @IBOutlet var lblFaithlabel: UILabel!
    @IBOutlet var lblInsurane_width: NSLayoutConstraint!
    @IBOutlet var lblLanguage_width: NSLayoutConstraint!
    @IBOutlet var lblFaith_width: NSLayoutConstraint!
    @IBOutlet var lblFor_width: NSLayoutConstraint!
    @IBOutlet var lblGender_height: NSLayoutConstraint!
    @IBOutlet var lblInsurance_height: NSLayoutConstraint!
    @IBOutlet var lblLanguage_height: NSLayoutConstraint!
    @IBOutlet var lblFor_height: NSLayoutConstraint!
    @IBOutlet var lblFaith_height: NSLayoutConstraint!
    
    @IBOutlet var lblLanguage: UILabel!
    @IBOutlet var lblInsurance: UILabel!
    @IBOutlet var lblGender: UILabel!
    @IBOutlet var lblFaith: UILabel!
    @IBOutlet var lblFor: UILabel!
    var DefaultWidth = CGFloat()
    var MyDictionary = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.DefaultWidth = self.view.frame.size.width - 20
        self.lblFor_width.constant = self.DefaultWidth
        self.lblFaith_width.constant = self.DefaultWidth
        self.lblInsurane_width.constant = self.DefaultWidth
        self.lblLanguage_width.constant = self.DefaultWidth
        
         var value =  self.MyDictionary.value(forKey: "ForFilter") as? String
         value = value?.trimmingCharacters(in: .whitespacesAndNewlines)
         if value != nil && !(value?.isEmpty)!{
            self.lblFor.text = value
            self.lblFor.isHidden = false
            self.lblFor_height.constant = 21
            var Height = CGFloat()
            Height = Utils().height(forText: value, font: UIFont.systemFont(ofSize: 17), withinWidth: self.DefaultWidth)
            self.lblFor_height.constant = Height
         }
         else{
                self.lblforlbl_height.constant = 0
                self.lblForlbl.isHidden = true
                self.lblFor_height.constant = 0
                self.lblFor.isHidden = true
         }
        
        var value1 =  self.MyDictionary.value(forKey: "FaithFilter") as? String
        value1 = value1?.trimmingCharacters(in: .whitespacesAndNewlines)
             if value1 != nil && !(value1?.isEmpty)!{
                  self.lblFaith.text = value1
                self.lblFaith.isHidden = false
                self.lblFaith_height.constant = 21
                var Height = CGFloat()
                Height = Utils().height(forText:  value1, font: UIFont.systemFont(ofSize: 17), withinWidth: self.DefaultWidth)
                self.lblFaith_height.constant = Height
        }
             else{
                self.lblfaithlbl_height.constant = 0
                self.lblFaithlabel.isHidden = true
                 self.lblFaith_height.constant = 0
                  self.lblFaith.isHidden = true
        }
        
        var value2 =  self.MyDictionary.value(forKey: "GenderPreferenceFilter") as? String
         value2 = value2?.trimmingCharacters(in: .whitespacesAndNewlines)
                 if value2 != nil && !(value2?.isEmpty)!{
                     self.lblGender.text = value2
                    self.lblGender.isHidden = false
                    self.lblGender_height.constant = 21
                    var Height = CGFloat()
                    Height = Utils().height(forText:  value2, font: UIFont.systemFont(ofSize: 17), withinWidth: self.DefaultWidth)
                    self.lblGender_height.constant = Height
        }
                 else{
                    self.lblGenlbl_height.constant = 0
                    self.lblGenderlbl.isHidden = true
                    self.lblGender_height.constant = 0
                    self.lblGender.isHidden = true
        }
        var value3 =  self.MyDictionary.value(forKey: "InsuranceZipCodeFilter") as? String
        value3 = value3?.trimmingCharacters(in: .whitespacesAndNewlines)
                     if value3 != nil && !(value3?.isEmpty)!{
                        self.lblInsurance.text = value3
                        self.lblInsurance.isHidden = false
                        self.lblInsurance_height.constant = 21
                        var Height = CGFloat()
                        Height = Utils().height(forText:  value3, font: UIFont.systemFont(ofSize: 17), withinWidth: self.DefaultWidth)
                        self.lblInsurance_height.constant = Height
        }
                     else{
                        self.lblinslbl_height.constant = 0
                        self.lblInsurancelbl.isHidden = true
                        self.lblInsurance_height.constant = 0
                        self.lblInsurance.isHidden = true
        }
        var value4 =  self.MyDictionary.value(forKey: "LanguageFilter") as? String
        value4 = value4?.trimmingCharacters(in: .whitespacesAndNewlines)
                         if value4 != nil && !(value4?.isEmpty)!{
                            self.lblLanguage.text = value4
                            self.lblLanguage.text = value3
                            self.lblLanguage.isHidden = false
                            self.lblLanguage_height.constant = 21
                            var Height = CGFloat()
                            Height = Utils().height(forText:  value4, font: UIFont.systemFont(ofSize: 17), withinWidth: self.DefaultWidth)
                            self.lblLanguage_height.constant = Height
        }
                         else{
                            self.lbllanlb_height.constant = 0
                            self.lblLanguagelbl.isHidden = true
                            self.lblLanguage_height.constant = 0
                            self.lblLanguage.isHidden = true
        }
        }
     
        // Do any additional setup after loading the view.
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
