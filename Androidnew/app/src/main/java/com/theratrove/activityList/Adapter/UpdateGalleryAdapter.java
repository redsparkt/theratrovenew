package com.theratrove.activityList.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theratrove.R;
import com.theratrove.activityList.model.Profile;
import com.theratrove.activityList.model.SearchData;

import java.util.ArrayList;

public class UpdateGalleryAdapter extends RecyclerView.Adapter<UpdateGalleryAdapter.MyViewHolder> {

    private ArrayList<Profile.TherapyGalleryList> bestList;
    Activity activity;
    RequestOptions options;
    DeleteAction deleteAction;

    public interface DeleteAction {
        public void selectedItem(int position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {


        ImageView image, imgDelete;
        RelativeLayout RlDelete;


        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            imgDelete = view.findViewById(R.id.imgDelete);
            RlDelete= view.findViewById(R.id.RlDelete);

        }
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    public UpdateGalleryAdapter(Activity activity, ArrayList<Profile.TherapyGalleryList> bestList, UpdateGalleryAdapter.DeleteAction deleteAction) {
        this.bestList = bestList;
        this.activity = activity;
        this.deleteAction = deleteAction;
        options = new RequestOptions()
//                .centerCrop()
                .placeholder(R.drawable.dummyuserimage)
                .error(R.drawable.dummyuserimage);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_update_gallry, parent, false);
//        int width = parent.getMeasuredWidth() / 2;
//        view.setMinimumWidth(width);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (bestList.get(position).getGalleryImage() != null && bestList.get(position).getGalleryImage().length() > 0) {
            Glide.with(activity).load(bestList.get(position).getGalleryImage()).apply(options).into(holder.image);
        }
        holder.RlDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAction.selectedItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return bestList.size();
    }

    public void updateList(ArrayList<Profile.TherapyGalleryList> data) {
        bestList.addAll(data);
        notifyDataSetChanged();
    }
}