﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using theratrove.data;
using theratrove.Helper;
using theratrove.Models;

namespace theratrove.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var indexPageData = new theratrove.Models.IndexPageData();
            var theratroveDB = new TheratroveDB();
            var todayDate = DateTime.Now.Date;

            this.GetStatistics(indexPageData);
            //indexPageData.DailySearches = theratroveDB.SearchFilterLogs.Where(e => DbFunctions.TruncateTime(e.DateCreated) == todayDate).Count();
            //indexPageData.NoOfTherapists = theratroveDB.Therapists.Count();
            //indexPageData.Members = theratroveDB.Users.Where(e => e.UserType == (byte)UserTypeEnum.Therapist).Count();
            //indexPageData.TherapistsByCondition = theratroveDB.TherapyTypes.ToList();
            indexPageData.TherapistsByCondition = ApplicationCashManager.therapistsCondition;//theratroveDB.TherapyTypes.Select(f => f.Name).ToList();
            indexPageData.TherapistsByLocation = theratroveDB.Therapists.Where(e => e.User.UserType == (byte)UserTypeEnum.Therapist && e.City != null && e.City != "").Select(d => d.City).Distinct().ToList();
            indexPageData.TestimonialList = theratroveDB.Testimonials.Where(e => e.Active == true).ToList();
            FillSearchFilters(indexPageData.SearchResult);
            return View(indexPageData);
        }

        public ActionResult About()
        {
            var theratroveDB = new TheratroveDB();
            var page = theratroveDB.PageContents.Where(e => e.PageName == ContentType.About.ToString()).FirstOrDefault();
            return View(page);

        }
        public ActionResult AboutNoneMenu()
        {
            var theratroveDB = new TheratroveDB();
            var page = theratroveDB.PageContents.Where(e => e.PageName == ContentType.About.ToString()).FirstOrDefault();
            return View(page);

        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public ActionResult Contact(string name, string email, string subject, string message)
        {
            ViewBag.ErrorMessage = "";
            var theratroveDB = new TheratroveDB();
            ContactInfo contactInfo = new ContactInfo();
            contactInfo.CreateDate = DateTime.Now;
            contactInfo.Email = email;
            contactInfo.Message = message;
            contactInfo.Name = name;
            contactInfo.Subject = subject;
            theratroveDB.ContactInfoes.Add(contactInfo);
            theratroveDB.SaveChanges();
            ViewBag.SucessMessage = "Thank you for contact.";


            string body = "Thank you for contacting us. One of our support staff will contact you to respond to your question.<br><br>";
            body = body + "Name:" + name + "<br>";
            body = body + "Email:" + email + "<br>";
            body = body + "Subject:" + subject + "<br>";
            body = body + "Message:" + message + "<br>";
            body = body + "Date:" + DateTime.Now.ToShortDateString() + "<br>";

            var adminUser = theratroveDB.Users.Where(e => e.UserType == (byte)UserTypeEnum.Admin && e.Active == true).ToList();
            foreach (User user in adminUser)
            {
                MailAddress mailAddressTo = new MailAddress(user.Email, "Theratrove");
                theratrove.Infrastructure.Helper.SendEmail(mailAddressTo, body, "Theratrove - Contact Us");
            }
            return View("Sucess");
        }
        [HttpPost]
        public JsonResult SendContactEmail(string fullName, string emailAddress, string message, int therapistId)
        {
            string body = "";
            body = body + "Name:" + fullName + "<br>";
            body = body + "Email:" + emailAddress + "<br>";
            body = body + "Message:" + message + "<br>";
            body = body + "Date:" + DateTime.Now.ToShortDateString() + "<br>";
            var theratroveDB = new TheratroveDB();
            var user = theratroveDB.Users.Where(e => e.UserId == therapistId).FirstOrDefault();
            MailAddress mailAddressTo = new MailAddress(user.Email, "Theratrove");
            theratrove.Infrastructure.Helper.SendEmail(mailAddressTo, body, "Contact");
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FAQ()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Login(LoginVM loginSignUpVM)
        {
            if (ModelState.IsValid)
            {
                var theratroveDB = new TheratroveDB();
                var userCount = theratroveDB.Users.Where(e => e.UserName == loginSignUpVM.UserName).FirstOrDefault();
                if (userCount == null)
                {
                    ViewBag.ErrorMessage = "invalid username/password";
                    return View("Login");
                }
                else
                {
                    if (userCount.Password == loginSignUpVM.Password && userCount.Active == true)
                    {
                        userCount.LastLogin = DateTime.Now;
                        theratroveDB.SaveChanges();
                        ViewBag.SucessMessage = "Successfully logged in";
                        Session["UserId"] = userCount.UserId;
                        Session["UserName"] = userCount.UserName;
                        return RedirectToAction("Profile");
                    }
                    else
                    {
                        userCount.FailedLoginAttempts = userCount.FailedLoginAttempts.GetValueOrDefault(0) + 1;
                        theratroveDB.SaveChanges();
                        ViewBag.ErrorMessage = "invalid username/password";
                        return View("Login");
                    }
                }
            }
            else
            {
                string messages = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));
                ModelState.AddModelError("", messages);
            }
            return View("Login", loginSignUpVM);
        }

        public ActionResult SignUp()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
        [HttpPost]
        public ActionResult SignUp(SignUpVM signUpVM)
        {
            if (ModelState.IsValid)
            {
                ViewBag.ErrorMessage = "";
                if (signUpVM.Password != signUpVM.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Password and Confirm Password must be same.");
                    return View("SignUp", signUpVM);
                }
                var theratroveDB = new TheratroveDB();
                var userCheck = theratroveDB.Users.Where(e => e.UserName == signUpVM.UserName || e.Email == signUpVM.Email).FirstOrDefault();
                if (userCheck == null)
                {
                    User user = new data.User();
                    user.Email = signUpVM.Email;
                    user.UserName = signUpVM.UserName;
                    user.Password = signUpVM.Password;
                    user.CreateDate = DateTime.Now;
                    user.UserType = (byte)UserTypeEnum.Therapist;
                    Guid id = Guid.NewGuid();
                    user.ActivationCode = id.ToString();
                    theratroveDB.Users.Add(user);
                    theratroveDB.SaveChanges();
                    ViewBag.SucessMessage = "You have been successfully registered. Activation link has been sent to your register email address.";

                    string mailBodyhtml = "Hi " + user.UserName + " and welcome to Theratrove. <br><br>";
                    mailBodyhtml = mailBodyhtml + " TheraTrove is an innovative platform to increase the success of the client/therapist fit. At TheraTrove our mission is to make finding a therapist as easy and efficient as possible. <br><br>";
                    mailBodyhtml = mailBodyhtml + " As a Therapist we are excited to help you get listed on our therapist searching website.  <br><br>";
                    mailBodyhtml = mailBodyhtml + " As a next step please take some time to fill out your profile so that you can be listed and searched by users seeking your type of services.  <br><br>";
                    mailBodyhtml = mailBodyhtml + " If you have any questions or need any assistance please don’t hesitate to contact us.  <br><br>";
                    mailBodyhtml = mailBodyhtml + " Let TheraTrove unlock the treasures of therapy today!  <br><br><br>";
                    mailBodyhtml = mailBodyhtml + " To Active your account please click this link to active your account.  <br>";
                    string url = Request.Url.ToString().Replace(Request.Url.AbsolutePath, "");

                    mailBodyhtml = mailBodyhtml + " <a href = '" + string.Format("{0}/Home/Activation?activationcode={1}", url, id.ToString()) + "'>Click here to activate your account.</a><br>";
                    MailAddress mailAddressTo = new MailAddress(user.Email, user.Email);
                    theratrove.Infrastructure.Helper.SendEmail(mailAddressTo, mailBodyhtml, "Welcome Theratrove");
                    return View("Sucess");
                }
                else
                {
                    if (string.IsNullOrEmpty(userCheck.UserName))
                        ModelState.AddModelError("", "username already exists");
                    else if (string.IsNullOrEmpty(userCheck.Email))
                        ModelState.AddModelError("", "Email already exists");
                    else
                        ModelState.AddModelError("", "username or email address already exists");

                    return View("SignUp", signUpVM);
                }
            }
            else
            {
                string messages = string.Join(Environment.NewLine, ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                ModelState.AddModelError("", messages);

            }
            return View("SignUp", signUpVM);
        }
        public ActionResult HowItWorks()
        {
            var theratroveDB = new TheratroveDB();
            var page = theratroveDB.PageContents.Where(e => e.PageName == ContentType.HowItWork.ToString()).FirstOrDefault();
            return View(page);
        }
        public ActionResult HowItWorksNoneMenu()
        {
            var theratroveDB = new TheratroveDB();
            var page = theratroveDB.PageContents.Where(e => e.PageName == ContentType.HowItWork.ToString()).FirstOrDefault();
            return View(page);
        }
        public ActionResult ForgotPassword()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
        public ActionResult ResetPassword(string userName)
        {
            var theratroveDB = new TheratroveDB();
            var user = theratroveDB.Users.Where(e => e.UserName == userName || e.Email ==userName).FirstOrDefault();
            if (user == null)
            {
                ViewBag.ErrorMessage = "username or email does not exists";
                return View("ForgotPassword");
            }
            else
            {
                string mailBodyhtml = "Your Theratrove username is "+ user.UserName +" password  is: " + user.Password ;
                MailAddress mailAddressTo = new MailAddress(user.Email, "Theratrove");
                theratrove.Infrastructure.Helper.SendEmail(mailAddressTo, mailBodyhtml, "Theratrove");
            }
            ViewBag.ErrorMessage = "password sent on your email address";
            return View("ForgotPassword");
        }

        public ActionResult MyAccount()
        {
            int userId = Convert.ToInt32(Session["UserId"]);
            if (userId == 0)
            {
                return RedirectToAction("login");
            }

            ViewBag.Message = "Your contact page.";
            return View();
        }


        public ActionResult ChangePassword()
        {
            int userId = Convert.ToInt32(Session["UserId"]);
            if (userId == 0)
            {
                return RedirectToAction("login");
            }
            ChangePasswordVM userModel = new ChangePasswordVM();
            var theratroveDB = new TheratroveDB();
            var userCount = theratroveDB.Users.Where(e => e.UserId == userId).FirstOrDefault();
            userModel.UserId = userCount.UserId;
            
            return View(userModel);
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordVM userModel)
        {
            ViewBag.ErrorMessage = "";
            int userId = Convert.ToInt32(Session["UserId"]);
            if (userId == 0)
            {
                return RedirectToAction("login");
            }

            if (ModelState.IsValid)
            {
                if (userModel.Password != userModel.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Password and Confirm Password must be same");
                    return View(userModel);
                }
                var theratroveDB = new TheratroveDB();
                var userCount = theratroveDB.Users.Where(e => e.UserId == userId).FirstOrDefault();
                if (userCount != null)
                {
                    userCount.Password = userModel.Password;
                    theratroveDB.SaveChanges();
                    ViewBag.SucessMessage = "your password has been successfully changed";
                    return View("Sucess");
                }
                else
                {
                    ViewBag.ErrorMessage = "Your password is not changes!";
                    return View("ChangePassword");
                }
            }
            else
            {
                string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                ModelState.AddModelError("", messages);
            }
            return View(userModel);
        }

        public ActionResult PrivacyPolicy()
        {
            var theratroveDB = new TheratroveDB();
            var page = theratroveDB.PageContents.Where(e => e.PageName == ContentType.PrivacyPolicy.ToString()).FirstOrDefault();
            return View(page);
        }
        public ActionResult TermsAndConditions()
        {
            var theratroveDB = new TheratroveDB();
            var page = theratroveDB.PageContents.Where(e => e.PageName == ContentType.TermsAndConditions.ToString()).FirstOrDefault();
            return View(page);
        }
        public ActionResult TermsAndConditionsNoneMenu()
        {
            var theratroveDB = new TheratroveDB();
            var page = theratroveDB.PageContents.Where(e => e.PageName == ContentType.TermsAndConditions.ToString()).FirstOrDefault();
            return View(page);
        }

        
        public ActionResult Profile()
        {

            int userId = Convert.ToInt32(Session["UserId"]);
            if (userId == 0)
            {
                return RedirectToAction("login");
            }

            var theratroveDB = new TheratroveDB();
            User user = theratroveDB.Users.Where(e => e.UserId == userId).FirstOrDefault();
            UserModel userModel = new UserModel();
            userModel.UserId = userId;
            userModel.Email = user.Email;
            userModel.ProfilePic = user.ProfilePicURL;
            userModel.TherapyGalleryList = user.TherapyGalleries.ToList();
            if (user.Therapist != null)
            {
                userModel.AboutMe = (string.IsNullOrEmpty(user.Therapist.AboutMe) ? "-" : user.Therapist.AboutMe);
                userModel.AcceptInsurance = user.Therapist.AcceptInsurance;
                userModel.Address1 = (string.IsNullOrEmpty(user.Therapist.Address1) ? "-" : user.Therapist.Address1);
                userModel.Address2 = (string.IsNullOrEmpty(user.Therapist.Address2) ? "-" : user.Therapist.Address2);
                userModel.AvgCost = user.Therapist.AvgCost;
                userModel.City = (string.IsNullOrEmpty(user.Therapist.City) ? "-" : user.Therapist.City);
                userModel.Education = (string.IsNullOrEmpty(user.Therapist.Education) ? "-" : user.Therapist.Education);
                userModel.FirstName = (string.IsNullOrEmpty(user.Therapist.FirstName) ? "-" : user.Therapist.FirstName);
                userModel.Gender = user.Therapist.Gender;
                userModel.LastName = (string.IsNullOrEmpty(user.Therapist.LastName) ? "-" : user.Therapist.LastName);
                userModel.License = (string.IsNullOrEmpty(user.Therapist.License) ? "-" : user.Therapist.License);
                userModel.Location = (string.IsNullOrEmpty(user.Therapist.Location) ? "-" : user.Therapist.Location);
                userModel.PhoneNumber = (string.IsNullOrEmpty(user.Therapist.PhoneNumber) ? "-" : user.Therapist.PhoneNumber);
                userModel.State = (string.IsNullOrEmpty(user.Therapist.State) ? "-" : user.Therapist.State);
                userModel.Title = (string.IsNullOrEmpty(user.Therapist.Title) ? "-" : user.Therapist.Title);
                userModel.VideoUrl = (string.IsNullOrEmpty(user.Therapist.VideoUrl) ? "-" : user.Therapist.VideoUrl);
                userModel.YearsInService = (string.IsNullOrEmpty(user.Therapist.YearsInService) ? "-" : user.Therapist.YearsInService);
                userModel.ZipCode = (string.IsNullOrEmpty(user.Therapist.ZipCode) ? "-" : user.Therapist.ZipCode);
                userModel.FaithFilter = string.Join(", ", user.Therapist.TherapistExpertiseFaiths.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Faith).Select(e => e.SearchFilterValue.Name));
                userModel.ForFilter = string.Join(", ", user.Therapist.TherapistExpertiseFors.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.For).Select(e => e.SearchFilterValue.Name));
                userModel.IssuesFilter = string.Join(", ", user.Therapist.TherapistExpertiseIssues.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Issues).Select(e => e.SearchFilterValue.Name));
                userModel.InsuranceZipCodeFilter = string.Join(", ", user.Therapist.TherapistExpertiseInsurances.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Insurance).Select(e => e.SearchFilterValue.Name));
                userModel.GenderPreferenceFilter = string.Join(", ", user.Therapist.TherapistExpertiseGenderPreferences.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.GenderPreference).Select(e => e.SearchFilterValue.Name));
                userModel.LanguageFilter = string.Join(", ", user.Therapist.TherapistExpertiseLanguages.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Language).Select(e => e.SearchFilterValue.Name));
                userModel.LocationFilter = string.Join(", ", user.Therapist.TherapistExpertiseLocations.Where(e => e.SearchFilter.FilterType == (byte)FilterTypeEnum.Location).Select(e => e.SearchFilterValue.Name));

            }
            return View(userModel);

        }


        public ActionResult MyProfile()
        {
            int userId = Convert.ToInt32(Session["UserId"]);
            if (userId == 0)
            {
                return RedirectToAction("login");
            }
            var theratroveDB = new TheratroveDB();
            User user = theratroveDB.Users.Where(e => e.UserId == userId).FirstOrDefault();
            UserWebModel userModel = new UserWebModel();
            userModel.ProfilePic = user.ProfilePicURL;
            userModel.TherapyGalleryList = user.TherapyGalleries.ToList();
            var SearchFilter = theratroveDB.SearchFilterValues.Where(e => e.Active == true).OrderBy(e=>e.DisplayOrder) .ToList();
            foreach (var item in SearchFilter)
            {
                CheckboxModel checkboxModel = new CheckboxModel();
                checkboxModel.Id = item.Id;
                checkboxModel.Name = item.Name;
                checkboxModel.itemFor = item.SearchFilterID;
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Faith)
                    userModel.SelectedFaith.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.For)
                    userModel.SelectedFor.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.GenderPreference)
                    userModel.SelectedGenderPreference.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Insurance)
                    userModel.SelectedInsurance.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Issues)
                    userModel.SelectedIssues.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Language)
                    userModel.SelectedLanguage.Add(checkboxModel);
                if (item.SearchFilter.FilterType == (byte)FilterTypeEnum.Location)
                    userModel.SelectedLocation.Add(checkboxModel);
            }
            var typesOfTherapy = theratroveDB.TherapyTypes.Where(e => e.Active == true).ToList();
            foreach (var item in typesOfTherapy)
            {
                CheckboxModel checkboxModel = new CheckboxModel();
                checkboxModel.Id = item.Id;
                checkboxModel.Name = item.Name;
                userModel.SelectedTherapistTypesOfTherapy.Add(checkboxModel);
            }


            userModel.UserId = userId;
            if (user.Therapist != null)
            {
                userModel.AboutMe = user.Therapist.AboutMe;
                userModel.AcceptInsurance = user.Therapist.AcceptInsurance;
                userModel.Address1 = user.Therapist.Address1;
                userModel.Address2 = user.Therapist.Address2;
                userModel.AvgCost = user.Therapist.AvgCost;
                userModel.City = user.Therapist.City;
                userModel.Education = user.Therapist.Education;
                userModel.FirstName = user.Therapist.FirstName;
                userModel.Gender = user.Therapist.Gender;
                userModel.LastName = user.Therapist.LastName;
                userModel.License = user.Therapist.License;
                userModel.Location = user.Therapist.Location;
                userModel.PhoneNumber = user.Therapist.PhoneNumber;
                userModel.State = user.Therapist.State;
                userModel.Title = user.Therapist.Title;
                userModel.VideoUrl = user.Therapist.VideoUrl;
                userModel.YearsInService = user.Therapist.YearsInService;
                userModel.ZipCode = user.Therapist.ZipCode;
                var therapistTypesOfTherapiesList = user.Therapist.TherapistTypesOfTherapies.ToList();
                foreach (var therapistTypesOfTherapie in therapistTypesOfTherapiesList)
                {
                    var item = userModel.SelectedTherapistTypesOfTherapy.Where(e => e.Id == therapistTypesOfTherapie.TherapyTypesID).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }


                var therapistExpertisesListFaiths = user.Therapist.TherapistExpertiseFaiths.ToList();
                foreach (var therapistExpertiseFaiths in therapistExpertisesListFaiths)
                {
                    var item = userModel.SelectedFaith.Where(e => e.Id == therapistExpertiseFaiths.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }

                var therapistExpertisesListFor = user.Therapist.TherapistExpertiseFors.ToList();
                foreach (var therapistExpertiseFors in therapistExpertisesListFor)
                {
                    var item = userModel.SelectedFor.Where(e => e.Id == therapistExpertiseFors.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }


                var therapistExpertisesListIssues = user.Therapist.TherapistExpertiseIssues.ToList();
                foreach (var therapistExpertiseIssues in therapistExpertisesListIssues)
                {
                    var item = userModel.SelectedIssues.Where(e => e.Id == therapistExpertiseIssues.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }

                var therapistExpertisesListInsurance = user.Therapist.TherapistExpertiseInsurances.ToList();
                foreach (var therapistExpertiseInsurance in therapistExpertisesListInsurance)
                {
                    var item = userModel.SelectedInsurance.Where(e => e.Id == therapistExpertiseInsurance.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }

                var therapistExpertisesListGenderPreference = user.Therapist.TherapistExpertiseGenderPreferences.ToList();
                foreach (var therapistExpertiseGenderPreference in therapistExpertisesListGenderPreference)
                {
                    var item = userModel.SelectedGenderPreference.Where(e => e.Id == therapistExpertiseGenderPreference.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }

                var therapistExpertisesListLanguage = user.Therapist.TherapistExpertiseLanguages.ToList();
                foreach (var therapistExpertiseLanguage in therapistExpertisesListLanguage)
                {
                    var item = userModel.SelectedLanguage.Where(e => e.Id == therapistExpertiseLanguage.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }

                var therapistExpertisesListLocation = user.Therapist.TherapistExpertiseLocations.ToList();
                foreach (var therapistExpertiseLocation in therapistExpertisesListLocation)
                {
                    var item = userModel.SelectedLocation.Where(e => e.Id == therapistExpertiseLocation.SearchFilterValueId).FirstOrDefault();
                    if (item != null)
                    {
                        item.Checked = true;
                    }
                }

                //var therapistExpertisesList = user.Therapist.TherapistExpertises.ToList();
                //foreach (var therapistExpertises in therapistExpertisesList)
                //{
                //    switch ((FilterTypeEnum)therapistExpertises.SearchFilter.FilterType)
                //    {
                //        case FilterTypeEnum.Faith:
                //            {
                //                var item = userModel.SelectedFaith.Where(e => e.Id == therapistExpertises.SearchFilterValueId).FirstOrDefault();
                //                if (item != null)
                //                {
                //                    item.Checked = true;
                //                }
                //            }
                //            break;
                //        case FilterTypeEnum.For:
                //            {
                //                var item = userModel.SelectedFor.Where(e => e.Id == therapistExpertises.SearchFilterValueId).FirstOrDefault();
                //                if (item != null)
                //                {
                //                    item.Checked = true;
                //                }
                //            }
                //            break;
                //        case FilterTypeEnum.Issues:
                //            {
                //                var item = userModel.SelectedIssues.Where(e => e.Id == therapistExpertises.SearchFilterValueId).FirstOrDefault();
                //                if (item != null)
                //                {
                //                    item.Checked = true;
                //                }
                //            }
                //            break;
                //        case FilterTypeEnum.Insurance:
                //            {
                //                var item = userModel.SelectedInsurance.Where(e => e.Id == therapistExpertises.SearchFilterValueId).FirstOrDefault();
                //                if (item != null)
                //                {
                //                    item.Checked = true;
                //                }
                //            }
                //            break;
                //        case FilterTypeEnum.GenderPreference:
                //            {
                //                var item = userModel.SelectedGenderPreference.Where(e => e.Id == therapistExpertises.SearchFilterValueId).FirstOrDefault();
                //                if (item != null)
                //                {
                //                    item.Checked = true;
                //                }
                //            }
                //            break;
                //        case FilterTypeEnum.Language:
                //            {
                //                var item = userModel.SelectedLanguage.Where(e => e.Id == therapistExpertises.SearchFilterValueId).FirstOrDefault();
                //                if (item != null)
                //                {
                //                    item.Checked = true;
                //                }
                //            }
                //            break;
                //        case FilterTypeEnum.Location:
                //            {
                //                var item = userModel.SelectedLocation.Where(e => e.Id == therapistExpertises.SearchFilterValueId).FirstOrDefault();
                //                if (item != null)
                //                {
                //                    item.Checked = true;
                //                }
                //            }
                //            break;
                //        default:
                //            break;
                //    }
                //}
            }
            return View(userModel);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult MyProfile(UserWebModel userModel)
        {
            var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(userModel).ToString();
            if (ModelState.IsValid)
            {
                TheratroveDB theratroveDB = new TheratroveDB();
                User user = theratroveDB.Users.Where(e => e.UserId == userModel.UserId).FirstOrDefault();
                if (user.Therapist == null)
                    user.Therapist = new Therapist();

                user.Therapist.AboutMe = (userModel.AboutMe == "<br>" ? "" : userModel.AboutMe);
                user.Therapist.AcceptInsurance = userModel.AcceptInsurance;
                user.Therapist.Active = true;
                user.Therapist.Address1 = userModel.Address1;
                user.Therapist.Address2 = userModel.Address2;
                user.Therapist.AvgCost = userModel.AvgCost;
                user.Therapist.City = userModel.City;
                user.Therapist.Education = userModel.Education;
                user.Therapist.FirstName = userModel.FirstName;
                user.Therapist.Gender = userModel.Gender;
                user.Therapist.LastName = userModel.LastName;
                user.Therapist.License = userModel.License;
                user.Therapist.Location = userModel.Location;
                user.Therapist.PhoneNumber = userModel.PhoneNumber;
                user.Therapist.State = userModel.State;
                user.Therapist.Title = userModel.Title;
                user.Therapist.VideoUrl = userModel.VideoUrl;
                user.Therapist.YearsInService = userModel.YearsInService;
                user.Therapist.ZipCode = userModel.ZipCode;

                theratroveDB.TherapistExpertiseFaiths.RemoveRange(user.Therapist.TherapistExpertiseFaiths);
                theratroveDB.TherapistExpertiseFors.RemoveRange(user.Therapist.TherapistExpertiseFors);
                theratroveDB.TherapistExpertiseGenderPreferences.RemoveRange(user.Therapist.TherapistExpertiseGenderPreferences);
                theratroveDB.TherapistExpertiseInsurances.RemoveRange(user.Therapist.TherapistExpertiseInsurances);
                theratroveDB.TherapistExpertiseIssues.RemoveRange(user.Therapist.TherapistExpertiseIssues);
                theratroveDB.TherapistExpertiseLanguages.RemoveRange(user.Therapist.TherapistExpertiseLanguages);
                theratroveDB.TherapistExpertiseLocations.RemoveRange(user.Therapist.TherapistExpertiseLocations);

                var itemList = userModel.SelectedFaith.Where(e => e.Checked == true);
                foreach (var item in itemList)
                {
                    TherapistExpertiseFaith therapistExpertise = new TherapistExpertiseFaith();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseFaiths.Add(therapistExpertise);
                }
                var itemListFor = userModel.SelectedFor.Where(e => e.Checked == true);
                foreach (var item in itemListFor)
                {
                    TherapistExpertiseFor therapistExpertise = new TherapistExpertiseFor();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseFors.Add(therapistExpertise);
                }
                var itemListIssues = userModel.SelectedIssues.Where(e => e.Checked == true);
                foreach (var item in itemListIssues)
                {
                    TherapistExpertiseIssue therapistExpertise = new TherapistExpertiseIssue();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseIssues.Add(therapistExpertise);
                }
                var itemListInsurance = userModel.SelectedInsurance.Where(e => e.Checked == true);
                foreach (var item in itemListInsurance)
                {
                    TherapistExpertiseInsurance therapistExpertise = new TherapistExpertiseInsurance();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseInsurances.Add(therapistExpertise);
                }
                var itemListGenderPreference = userModel.SelectedGenderPreference.Where(e => e.Checked == true);
                foreach (var item in itemListGenderPreference)
                {
                    TherapistExpertiseGenderPreference therapistExpertise = new TherapistExpertiseGenderPreference();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseGenderPreferences.Add(therapistExpertise);
                }

                var itemListLanguage = userModel.SelectedLanguage.Where(e => e.Checked == true);
                foreach (var item in itemListLanguage)
                {
                    TherapistExpertiseLanguage therapistExpertise = new TherapistExpertiseLanguage();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseLanguages.Add(therapistExpertise);
                }

                var itemListLocation = userModel.SelectedLocation.Where(e => e.Checked == true);
                foreach (var item in itemListLocation)
                {
                    TherapistExpertiseLocation therapistExpertise = new TherapistExpertiseLocation();
                    therapistExpertise.SearchFilterId = item.itemFor;
                    therapistExpertise.SearchFilterValueId = item.Id;
                    user.Therapist.TherapistExpertiseLocations.Add(therapistExpertise);
                }

                if (user.Therapist.TherapistTypesOfTherapies.Count > 0)
                {
                    theratroveDB.TherapistTypesOfTherapies.RemoveRange(user.Therapist.TherapistTypesOfTherapies);
                }

                var itemTypesOfTherapyList = userModel.SelectedTherapistTypesOfTherapy.Where(e => e.Checked == true);
                foreach (var item in itemTypesOfTherapyList)
                {
                    TherapistTypesOfTherapy therapistTypesOfTherapy = new TherapistTypesOfTherapy();
                    therapistTypesOfTherapy.TherapyTypesID = item.Id;
                    user.Therapist.TherapistTypesOfTherapies.Add(therapistTypesOfTherapy);
                }
                theratroveDB.SaveChanges();
                return RedirectToAction("Profile");
            }
            else
            {
                string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                ModelState.AddModelError("", messages);

            }
            return View(userModel);
        }

        public ActionResult Activation(string activationcode)
        {
            var theratroveDB = new TheratroveDB();
            User user = theratroveDB.Users.Where(e => e.ActivationCode == activationcode).FirstOrDefault();
            if (user == null)
            {
                ViewBag.Message = "Activation unsuccessful.";
            }
            else
            {

                user.Active = true;
                theratroveDB.SaveChanges();
                ViewBag.Message = "Activation successful.";

            }
            return View();
        }

        private void FillSearchFilters(SearchResults searchResults)
        {
            //TheratroveDB db = new TheratroveDB();
            //var fiters = db.SearchFilters.ToList();
            //foreach (var item in fiters.OrderBy(d => d.DisplayOrderNo).ToList())
            //{
            //    var filterdetail = new FiltersDetail
            //    {
            //        FilterName = item.Name,
            //        FilteType = item.FilterType,
            //        FilterControlType = item.FilterControlType.Value,
            //    };

            //    foreach (var filtervalues in item.SearchFilterValues.Where(f => f.Active == true).OrderBy(d => d.DisplayOrder).ToList())
            //    {
            //        //   filterdetail.FilterValues.Add(new System.Collections.Generic.KeyValuePair<string, string>(filtervalues.Name, filtervalues.Value));
            //        if (filtervalues.SearchFilter.FilterType == (int)FilterTypeEnum.Location)
            //        {
            //            filterdetail.FilterValues.Add(new SelectListItem() { Text = filtervalues.Name, Value = filtervalues.Value.ToString() });
            //        }
            //        else
            //        {
            //            filterdetail.FilterValues.Add(new SelectListItem() { Text = filtervalues.Name, Value = filtervalues.Id.ToString() });
            //        }
            //    }

            //    searchResults.FilterOptions.Add(filterdetail);
            //}

            searchResults.FilterOptions = ApplicationCashManager.filterOptions;
        }

        public JsonResult GetZipCode()
        {
            var theratroveDB = new TheratroveDB();

            var zipCodeList = theratroveDB.Therapists.Where(e => e.ZipCode != null && e.ZipCode != "").Distinct().Select(s => s.ZipCode).ToList();

            return Json(zipCodeList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProfilePicUpload(HttpPostedFileBase fileUpload)
        {
            var theratroveDB = new TheratroveDB();
            int userId = Convert.ToInt32(Session["UserId"]);
            User user = theratroveDB.Users.Where(e => e.UserId == userId).FirstOrDefault();

            string urlImage = theratrove.Infrastructure.Helper.SaveImage(fileUpload, "profile");
            user.ProfilePicURL = urlImage;
            theratroveDB.SaveChanges();

            return RedirectToAction("MyProfile");
        }
        public ActionResult GalleryPicUpload(HttpPostedFileBase fileUpload)
        {
            var theratroveDB = new TheratroveDB();
            int userId = Convert.ToInt32(Session["UserId"]);
            User user = theratroveDB.Users.Where(e => e.UserId == userId).FirstOrDefault();
            TherapyGallery therapyGallery = new TherapyGallery();

            //therapyGallery.GalleryImage  = theratrove.Infrastructure.Helper.ImageToByte(System.Drawing.Image.FromStream(fileUpload.InputStream), 80, 80);
            //using (Stream inputStream = fileUpload.InputStream)
            //{
            //    MemoryStream memoryStream = inputStream as MemoryStream;
            //    if (memoryStream == null)
            //    {
            //        memoryStream = new MemoryStream();
            //        inputStream.CopyTo(memoryStream);
            //    }
            //    therapyGallery.GalleryImage = memoryStream.ToArray();
            //}
            string urlImage = theratrove.Infrastructure.Helper.SaveImage(fileUpload, "therapygallery");
            therapyGallery.GalleryImageURL = urlImage;
            therapyGallery.CreateDateTime = DateTime.Now;
            user.TherapyGalleries.Add(therapyGallery);
            theratroveDB.SaveChanges();
            return RedirectToAction("MyProfile");
        }

        public ActionResult DeleteGalleryPic(int Id)
        {
            var theratroveDB = new TheratroveDB();
            int userId = Convert.ToInt32(Session["UserId"]);
            TherapyGallery therapyGallery = theratroveDB.TherapyGalleries.Where(e => e.Id == Id && e.UserId == userId).FirstOrDefault();
            if (therapyGallery != null)
            {
                theratroveDB.TherapyGalleries.Remove(therapyGallery);
                theratroveDB.SaveChanges();
            }
            return RedirectToAction("MyProfile");
        }

        public void GetStatistics(IndexPageData indexPageData)
        {
            SqlParameter currentdate;

            currentdate = new SqlParameter
            {
                ParameterName = "currentDate",
                Value = DateTime.Today,
                DbType = System.Data.DbType.DateTime
            };

            using (var db = new TheratroveDB())
            {
                // If using Code First we need to make sure the model is built before we open the connection
                // This isn't required for models created with the EF Designer
                //db.Database.Initialize(force: false);

                // Create a SQL command to execute the sproc
                //Get student name of string type
                var indexPageCounts = db.Database.SqlQuery<IndexPageCounts>("SP_GetStatistics @currentDate", currentdate).FirstOrDefault();

                indexPageData.NoOfTherapists = indexPageCounts.NoofTherapist;
                indexPageData.DailySearches = indexPageCounts.TotalSearch;
                indexPageData.Members = indexPageCounts.Members;
            }

        }
    }
}