//
//  WebService.swift
//  Orion
//
//  Created by Dhruv Patel on 26/09/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit
import Alamofire
//import
class WebService: NSObject {

    static func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }

    static func jsonToDictionary(from text: String) -> [String: Any]? {
        guard let data = text.data(using: .utf8) else { return nil }
        let anyResult = try? JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String: Any]
    }
    
    class func postMethod (params: [String : AnyObject], apikey: String, completion: @escaping (Any) -> Void, failure:@escaping (Error) -> Void)
    {
        if Utils().isConnectedToNetwork() == false{
            Utils().showMessage(Constants.NetworkUnavailable)
            return
        }
        print(params)
        
        let jsonData = try! JSONSerialization.data(withJSONObject: params)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
        
        let params1 = self.jsonToDictionary(from: jsonString! as String) ?? [String : Any]() 
        print(params1)
        
        let strURL = Constants.BASE_URL + apikey
        let url = URL(string: strURL)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(url!, method: .post, parameters: params1, encoding: JSONEncoding.default)
            .responseJSON
            {
                response in
                switch (response.result)
                {
                case .success:
                    let statusCode = response.response?.statusCode
                    if statusCode == 200{
                        if (response.result.value as? [String: AnyObject]) != nil
                     {
                        let jsonResponse = response.result.value as! NSDictionary
                        print(jsonResponse)
                        completion(jsonResponse)
                     }
                        else if (response.result.value as? Bool) != nil{
                            Utils().showMessage("Message sent successfully.")
                        }
                        else if (response.result.value as? NSArray) != nil{
                        let jsonResponse = response.result.value as! NSArray
                       print(jsonResponse)
                        completion(jsonResponse)

                     }
                        else if (response.result.value as? Int) != nil{
                        let jsonResponse = response.result.value as! Int
                        print(jsonResponse)
                        completion(jsonResponse)
                        
                     }
                        else if (response.result.value as? NSDictionary) != nil{
                            let jsonResponse = response.result.value as! NSDictionary
                            print(jsonResponse)
                            completion(jsonResponse)
                            
                        }
                     else{
                        let Message = response.result.value as! String
                            let MyDict = NSMutableDictionary()
                            MyDict.setValue(Message, forKey: "Message")
                            completion(MyDict)
                        }
                        
                    }
                    else{
                        
                        let Message = response.result.value as! String
                         Utils().showMessage(Message)
                    }
                    
                    Utils().HideLoader()
                case .failure(let error):                    
                    Utils().showMessage("Something went wrong")
                    failure(error)
                    Utils().HideLoader()
                    break
                }
        }
    }
    
    class func GetMethod (params: [String : AnyObject], apikey: String, completion: @escaping (Any) -> Void, failure:@escaping (Error) -> Void)
    {
        if Utils().isConnectedToNetwork() == false
        {
            Utils().showMessage(Constants.NetworkUnavailable)
            return
        }
        
        //Utils().ShowLoader()
        let strURL = Constants.BASE_URL + apikey
        let url = URL(string: strURL)
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.request(url!, method: .get, parameters: params)
            .responseJSON
            {
                response in
                switch (response.result)
                {
                case .success:
                    let statusCode = response.response?.statusCode
                    if statusCode == 200{
                        if let dict = response.result.value as? [String: AnyObject]
                        {
                            let jsonResponse = response.result.value as! NSDictionary
                            print(jsonResponse)
                            completion(jsonResponse)
                        }
                        else if let ivalue = response.result.value as? NSDictionary
                        {
                            let jsonResponse = response.result.value as! NSDictionary
                            print(jsonResponse)
                            completion(jsonResponse)
                            
                        }
                        else{
                            let Message = response.result.value as! String
                            Utils().showMessage(Message)
                        }
                        
                    }
                    else{
                        let Message = response.result.value as! String
                        Utils().showMessage(Message)
                    }
                    
                    Utils().HideLoader()
                case .failure(let error):
                    Utils().showMessage("Something went wrong")
                    failure(error)
                    Utils().HideLoader()
                    break
                }
        }
    }
    
   class func postMultiPartdata (params: [String : AnyObject], apikey: String, completion: @escaping (Any) -> Void, failure:@escaping (Error) -> Void){

        if Utils().isConnectedToNetwork() == false
        {
            Utils().showMessage(Constants.NetworkUnavailable)
            return
        }

        //Utils().ShowLoader()
        let strURL = Constants.BASE_URL + apikey
        let url = URL(string: strURL)

        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120


    Alamofire.upload(
        multipartFormData: { MultipartFormData in
            //    multipartFormData.append(imageData, withName: "user", fileName: "user.jpg", mimeType: "image/jpeg")

            for (key, value) in params {

                if value is Data{
                    let imgData = params[key] as! Data
                    MultipartFormData.append(imgData, withName: "file_profile_image", fileName: "user.jpg", mimeType: "image/jpeg")
                }
                else{
                    MultipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }

            }
    }, to: url!) { (result) in

        switch (result)
        {
        case .success(let upload, _, _):
            upload.responseJSON { response in
                print("Succesfully uploaded")
                print(response)
                let jsonResponse = response.result.value as! NSDictionary
                print(jsonResponse)
                completion(jsonResponse)
                Utils().HideLoader()

            }
        case .failure(let encodingError):
            print(encodingError)
        }
    }
    }

}




