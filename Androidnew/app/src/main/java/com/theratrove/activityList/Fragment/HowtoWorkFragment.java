package com.theratrove.activityList.Fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.theratrove.R;
import com.theratrove.activityList.SignUpActivity;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HowtoWorkFragment extends Fragment implements Callback<ResponseBody> {


    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txttitle)
    TextView txttitle;
    @BindView(R.id.toolbar)
    Toolbar toolbarTop;
    @BindView(R.id.myWebView)
    WebView myWebView;
    Unbinder unbinder;

    ApiInterface apiService;
    HttpUrl httpUrlCallLogin;


    public static HowtoWorkFragment newInstance() {
        HowtoWorkFragment myFragment = new HowtoWorkFragment();

        Bundle args = new Bundle();

        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_us, container, false);


        unbinder = ButterKnife.bind(this, view);

        apiService = ApiClient.getClient().create(ApiInterface.class);

        myWebView.reload();
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        myWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        myWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        myWebView.setWebChromeClient(new WebChromeClient());
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP){
            myWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        if (CommonValidation.checkInternetConnection(getActivity())) {
            callHowtowork();
        }else {
            CommonValidation.showToast(getActivity(), getString(R.string.no_internet));
        }

//        myWebView.loadUrl("http://theratrove-staging.azurewebsites.net/Home/About");

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void callHowtowork() {
        Call<ResponseBody> callLogin = apiService.CmsPage("HowItWork");
        httpUrlCallLogin = callLogin.request().url();
        callLogin.clone().enqueue(this);
        Utility.showProgress(getActivity());
    }

    @Override
    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
        Utility.dismissProgress();
        if (response.isSuccessful() && response.code() == 200) {
            try {

                JSONObject jsonObject = new JSONObject(response.body().string());
                String data = jsonObject.getString("pageContent");
                myWebView.loadData(data, "text/html", null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                CommonValidation.showToast(getActivity(), response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        Utility.dismissProgress();
        t.printStackTrace();
    }
}