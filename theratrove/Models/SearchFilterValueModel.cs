﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using theratrove.data;

namespace theratrove.Models
{
    public class SearchFilterValueModel  
    {
        public SearchFilterValueModel()
        {
            Filters = new List<SelectListItem>();
            SearchFilterValuesList = new List<SearchFilterValue>();
        }
        public List<SelectListItem> Filters { get; set; }

        public SearchFilter searchFilter { get; set; }

        public SearchFilterValue SelectedValue { get; set; }

        public List<SearchFilterValue> SearchFilterValuesList { get; set; }
    }
}