//
//  SearchPage.swift
//  Theratrove
//
//  Created by Dhruv Patel on 17/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class SearchPage: UIViewController,HADropDownDelegate,UITextFieldDelegate {

    @IBOutlet var uvwtopview_height: NSLayoutConstraint!
    @IBOutlet var clwLanguage_height: NSLayoutConstraint!
    @IBOutlet var clwInsurance_height: NSLayoutConstraint!
    @IBOutlet var clwIsue_height: NSLayoutConstraint!
    @IBOutlet var clwfaith_height: NSLayoutConstraint!
    
    @IBOutlet var txtZipCode: UITextField!
    @IBOutlet var txtsearch: UITextField!
    
    @IBOutlet var clwLanguage: UICollectionView!
    @IBOutlet var clwInsurance: UICollectionView!
    @IBOutlet var clwIssues: UICollectionView!
    @IBOutlet var clwFaith: UICollectionView!
    
    @IBOutlet var Gender: HADropDown!
    @IBOutlet var For: HADropDown!
    @IBOutlet var AreaRange: HADropDown!
    
    @IBOutlet var btnSearch: UIButton!
    
    var arrselectedFaith = NSMutableArray()
    var arrselectedIssue = NSMutableArray()
    var arrselectedInsurance = NSMutableArray()
    var arrselectedLanguage = NSMutableArray()
    
    var Keyword = String()
    var zipcode = String()
    var faith = String()
    var distance = String()
    var selectedFor = String()
    var selectedGender = String()
    
    var selectedFor_value = String()
    var selectedIssue_value = String()
    var selectedInsurance_value = String()
    var selectedGender_value = String()
    var selectedLanguage_value = String()
    
    
    var arrAreaRange = NSMutableArray()
    var arrAreaRange_value = NSMutableArray()
    var arrFaith = NSMutableArray()
    var arrFaith_value = NSMutableArray()
    var arrFor = NSMutableArray()
    var arrFor_value = NSMutableArray()
    var arrIssue = NSMutableArray()
    var arrIssue_value = NSMutableArray()
    var arrInsurance = NSMutableArray()
    var arrInsurance_value = NSMutableArray()
    var arrGender = NSMutableArray()
    var arrGender_value = NSMutableArray()
    var arrLanguage = NSMutableArray()
    var arrLanguage_value = NSMutableArray()
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Device.DeviceType.IS_IPHON_X{
            self.uvwtopview_height.constant = 80
        }
        
        self.txtsearch.layer.borderColor = UIColor.black.cgColor
        self.txtsearch.layer.borderWidth = 1.0
        self.txtsearch.layer.cornerRadius = 5.0
        self.txtsearch.layer.masksToBounds = true
        
        self.txtZipCode.layer.borderColor = UIColor.black.cgColor
        self.txtZipCode.layer.borderWidth = 1.0
        self.txtZipCode.layer.cornerRadius = 5.0
        self.txtZipCode.layer.masksToBounds = true
        
        self.distance = ""
        self.selectedFor = ""
        self.selectedGender = ""
        
        For.layer.borderColor = UIColor.black.cgColor
        For.layer.borderWidth = 1.0
        For.layer.cornerRadius = 5
        For.layer.masksToBounds = true
        
        Gender.layer.borderColor = UIColor.black.cgColor
        Gender.layer.borderWidth = 1.0
        Gender.layer.cornerRadius = 5
        Gender.layer.masksToBounds = true
        
        AreaRange.layer.borderColor = UIColor.black.cgColor
        AreaRange.layer.borderWidth = 1.0
        AreaRange.layer.cornerRadius = 5
        AreaRange.layer.masksToBounds = true
        
        self.btnSearch.layer.borderColor = UIColor.black.cgColor
        self.btnSearch.layer.borderWidth = 1.0
        self.btnSearch.layer.cornerRadius = 5
        self.btnSearch.layer.masksToBounds = true
        
        self.txtsearch.delegate = self
        self.txtZipCode.delegate = self

        self.SearchDetail()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtsearch{
        self.txtsearch.resignFirstResponder()
        }
        else{
          self.txtZipCode.resignFirstResponder()
        }
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnFindMatchesClick(_ sender: Any) {
        self.FindMatches()
    }
    func FindMatches(){
        self.Keyword = self.txtsearch.text!
        self.zipcode = self.txtZipCode.text!
        
        
        let selectedfaith = NSMutableArray()
        if self.arrselectedFaith.count>0
        {
            for index in 0..<arrselectedFaith.count
            {
            
                let value = self.arrselectedFaith.object(at: index)
                let valuefaith = self.arrFaith_value.object(at: value as! Int)
                selectedfaith.add(valuefaith)
    
            }
        }
        
        let selectedissue = NSMutableArray()
        if self.arrselectedIssue.count>0
        {
            for index in 0..<arrselectedIssue.count
            {
                    let value = self.arrselectedIssue.object(at: index)
                    let valuefaith = self.arrIssue_value.object(at: value as! Int)
                    selectedissue.add(valuefaith)
            }
        }
        
        let selectedinsurence = NSMutableArray()
        if self.arrselectedInsurance.count>0
        {
            for index in 0..<arrselectedInsurance.count
            {
                    let value = self.arrselectedInsurance.object(at: index)
                    let valuefaith = self.arrInsurance_value.object(at: value as! Int)
                    selectedinsurence.add(valuefaith)
            }
        }
       
        let selectedLanguage = NSMutableArray()
      
        if self.arrselectedLanguage.count>0
        {
            for index in 0..<arrselectedLanguage.count
            {
                    let value = self.arrselectedLanguage.object(at: index)
                    let valuefaith = self.arrLanguage_value.object(at: value as! Int)
                    selectedLanguage.add(valuefaith)
            }
        }
      
        var parameter : [String : Any] = [:]
        
        if !self.Keyword.isEmpty{
            self.Keyword = self.Keyword.trimmingCharacters(in: .whitespacesAndNewlines)
            parameter.updateValue(self.Keyword, forKey: "Keyword")
        }
        if !self.selectedFor.isEmpty{
            let selectedfor = NSMutableArray()
            selectedfor.add(self.selectedFor)
            parameter.updateValue(selectedfor, forKey: "SelectedFor")
        }
        if selectedfaith.count > 0{
            
            parameter.updateValue(selectedfaith, forKey: "SelectedFaith")
        }
        if !self.selectedGender.isEmpty{
            let selectedGen = NSMutableArray()
            selectedGen.add(self.selectedGender)
            parameter.updateValue(selectedGen, forKey: "SelectedGender")
        }
        if selectedissue.count > 0{
            parameter.updateValue(selectedissue, forKey: "SelectedIssues")
        }
        if selectedinsurence.count > 0{
            parameter.updateValue(selectedinsurence, forKey: "SelectedInsurance")
        }
        if !self.distance.isEmpty{
            
            var distdata = String()
            if self.distance == "52"{
                distdata = "10"
            }
            else if self.distance == "53"{
                distdata = "20"
            }
            else{
                 distdata = "30"
            }
            
            parameter.updateValue(distdata, forKey: "MaxDistance")
        }
        if selectedLanguage.count > 0{
            parameter.updateValue(selectedLanguage, forKey: "SelectedLanguage")
        }
        if !zipcode.isEmpty{
            parameter.updateValue(zipcode, forKey: "ZipCode")
        }
         parameter.updateValue(1, forKey: "PageNo")
         parameter.updateValue(25, forKey: "PageSize")

        Utils().ShowLoader()
        WebService.postMethod(params: parameter as [String : AnyObject], apikey: Constants.Search, completion: { (Json) in
            let Data : NSDictionary = Json as! NSDictionary
            var arrSearchResults = NSMutableArray()
            //arrSearchResults = Data.object(forKey: "Result") as! NSMutableArray
            arrSearchResults.addObjects(from: Data.object(forKey: "Result") as! [Any])
            if arrSearchResults.count > 0
            {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let MatchPage = storyboard.instantiateViewController(withIdentifier: "MatchPage") as? MatchPage
                MatchPage?.Response = arrSearchResults
                MatchPage?.FromSeacrhPage = "1"
                MatchPage?.Parameter = parameter
                self.navigationController?.pushViewController(MatchPage ?? UIViewController(), animated: true)
            }
            else{
                Utils().showMessage("No Record Found.")
            }
            
            
        }, failure: {(error) in
            
        })
        
    }
    
    func SearchDetail(){
        
        let parameter : [String : Any] = [:]
        Utils().ShowLoader()
        WebService.GetMethod(params: parameter as [String : AnyObject], apikey: Constants.GetSearchFilters, completion: { (Json) in
            let Dict : NSDictionary = Json as! NSDictionary
            
            DispatchQueue.main.async {
                
                let temparrFor = NSMutableArray()
                temparrFor.addObjects(from: Dict.object(forKey: "ForOptions") as! [Any])
                let temparrFaith = NSMutableArray()
                temparrFaith.addObjects(from: Dict.object(forKey: "FaithOpitions") as! [Any])
                let temparrIssue = NSMutableArray()
                temparrIssue.addObjects(from: Dict.object(forKey: "IssuesOptions") as! [Any])
                let temparrAreaRange = NSMutableArray()
                temparrAreaRange.addObjects(from: Dict.object(forKey: "LocationOptions") as! [Any])
                let temparrInsurance = NSMutableArray()
                temparrInsurance.addObjects(from: Dict.object(forKey: "InsuranceOptions") as! [Any])
                let temparrGender = NSMutableArray()
                temparrGender.addObjects(from: Dict.object(forKey: "GenderOptions") as! [Any])
                let temparrLanguage = NSMutableArray()
                temparrLanguage.addObjects(from: Dict.object(forKey: "LanguageOptions") as! [Any])
                
                for i in 0..<temparrFor.count{
                    let ItemData = temparrFor.object(at: i) as! NSDictionary
                    self.arrFor.add(ItemData.value(forKey: "Key") as? String)
                    self.arrFor_value.add(ItemData.value(forKey: "Value")!)
                }
                for i in 0..<temparrFaith.count{
                    let ItemData = temparrFaith.object(at: i) as! NSDictionary
                    self.arrFaith.add(ItemData.value(forKey: "Key") as? String)
                    self.arrFaith_value.add(ItemData.value(forKey: "Value")!)
                }
                for i in 0..<temparrIssue.count{
                    let ItemData = temparrIssue.object(at: i) as! NSDictionary
                    self.arrIssue.add(ItemData.value(forKey: "Key") as? String)
                    self.arrIssue_value.add(ItemData.value(forKey: "Value")!)
                }
                for i in 0..<temparrAreaRange.count{
                    let ItemData = temparrAreaRange.object(at: i) as! NSDictionary
                    self.arrAreaRange.add(ItemData.value(forKey: "Key") as? String)
                    self.arrAreaRange_value.add(ItemData.value(forKey: "Value")!)
                }
                for i in 0..<temparrInsurance.count{
                    let ItemData = temparrInsurance.object(at: i) as! NSDictionary
                    self.arrInsurance.add(ItemData.value(forKey: "Key") as? String)
                    self.arrInsurance_value.add(ItemData.value(forKey: "Value")!)
                }
                for i in 0..<temparrGender.count{
                    let ItemData = temparrGender.object(at: i) as! NSDictionary
                    self.arrGender.add(ItemData.value(forKey: "Key") as? String)
                    self.arrGender_value.add(ItemData.value(forKey: "Value")!)
                }
                for i in 0..<temparrLanguage.count{
                    let ItemData = temparrLanguage.object(at: i) as! NSDictionary
                    self.arrLanguage.add(ItemData.value(forKey: "Key") as? String)
                    self.arrLanguage_value.add(ItemData.value(forKey: "Value")!)
                }
                
                self.AreaRange.items = self.arrAreaRange as! [String]
                self.AreaRange.title = "Select Area"
                
                self.For.items = self.arrFor as! [String]
                self.For.title = "Select For"
                
                
                
                self.clwfaith_height.constant = CGFloat(self.arrFaith.count * 50)
                self.clwFaith.delegate = self
                self.clwFaith.delegate = self
                self.clwFaith.reloadData()
                
                self.clwIsue_height.constant = CGFloat(self.arrIssue.count * 50)
                self.clwIssues.delegate = self
                self.clwIssues.delegate = self
                self.clwIssues.reloadData()
                
                self.clwInsurance_height.constant = CGFloat(self.arrInsurance.count * 50)
                self.clwInsurance.delegate = self
                self.clwInsurance.delegate = self
                self.clwInsurance.reloadData()
                
                self.Gender.items = self.arrGender as! [String]
                self.Gender.title = "Select Gender"
                self.Gender.delegate = self as HADropDownDelegate
                
                self.clwLanguage_height.constant = CGFloat(self.arrLanguage.count * 50)
                self.clwLanguage.delegate = self
                self.clwLanguage.delegate = self
                self.clwLanguage.reloadData()
                
                
                self.AreaRange.delegate = self as HADropDownDelegate
                self.For.delegate = self as HADropDownDelegate
                
            }
        }, failure: {(error) in
            
        })
    }
    
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        if dropDown == self.AreaRange{
            self.distance = (self.arrAreaRange_value.object(at: index) as? String)!
        }
        else if dropDown == self.For{
            let value = self.arrFor_value.object(at: index)
            self.selectedFor = "\(value)"
        }
        else{
            let value = self.arrGender_value.object(at: index)
            self.selectedGender = "\(value)"
        }
    }
    
    func didShow(dropDown: HADropDown) {
        self.view.endEditing(true)
    }
    
    @IBAction func btnMenuClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SearchPage: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.clwFaith{
            return self.arrFaith.count
        }
        else if collectionView == self.clwIssues{
            return self.arrIssue.count
        }
        else if collectionView == self.clwInsurance{
            return self.arrInsurance.count
        }
        else{
            return self.arrLanguage.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! SearchForCell
        if collectionView == self.clwFaith{
            cell.lblContent.text = self.arrFaith.object(at: indexPath.row) as? String
            if self.arrselectedFaith.contains(indexPath.row)
            {
                cell.imgTick.image = UIImage(named: "select_checked")
            }
            else
            {
                cell.imgTick.image = UIImage(named: "select_unchecked")
            }
            return cell
        }
        else if collectionView == clwIssues{
            cell.lblContent.text = self.arrIssue.object(at: indexPath.row) as? String
            if arrselectedIssue.contains(indexPath.row){
                cell.imgTick.image = UIImage(named: "select_checked")
            }
            else
            {
                cell.imgTick.image = UIImage(named: "select_unchecked")
            }
            return cell
        }
        else if collectionView == self.clwInsurance{
            cell.lblContent.text = self.arrInsurance.object(at: indexPath.row) as? String
            if arrselectedInsurance.contains(indexPath.row){
                cell.imgTick.image = UIImage(named: "select_checked")
            }
            else{
                cell.imgTick.image = UIImage(named: "select_unchecked")
            }
            return cell
        }
        else{
            cell.lblContent.text = self.arrLanguage.object(at: indexPath.row) as? String
            if arrselectedLanguage.contains(indexPath.row){
                cell.imgTick.image = UIImage(named: "select_checked")
            }
            else{
                cell.imgTick.image = UIImage(named: "select_unchecked")
            }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.clwFaith{
            if self.arrselectedFaith.contains(indexPath.row){
                self.arrselectedFaith.remove(indexPath.row)
            }
            else{
                self.arrselectedFaith.add(indexPath.row)
            }
            
            self.clwFaith.reloadData()
        }
        else if collectionView == self.clwIssues{
            if self.arrselectedIssue.contains(indexPath.row){
                self.arrselectedIssue.remove(indexPath.row)
            }
            else{
                self.arrselectedIssue.add(indexPath.row)
            }
            
            self.clwIssues.reloadData()
        }
        else if collectionView == self.clwInsurance{
            if self.arrselectedInsurance.contains(indexPath.row){
                self.arrselectedInsurance.remove(indexPath.row)
            }
            else{
                self.arrselectedInsurance.add(indexPath.row)
            }
            self.clwInsurance.reloadData()
        }
        else{
            if self.arrselectedLanguage.contains(indexPath.row){
                self.arrselectedLanguage.remove(indexPath.row)
            }
            else{
                self.arrselectedLanguage.add(indexPath.row)
            }
            self.clwLanguage.reloadData()
        }
    }
}

