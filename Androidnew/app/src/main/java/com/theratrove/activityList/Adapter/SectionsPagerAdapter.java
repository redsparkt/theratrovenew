package com.theratrove.activityList.Adapter;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;


import com.theratrove.activityList.Fragment.DescrptionFragment;
import com.theratrove.activityList.Fragment.GalleryFragment;
import com.theratrove.activityList.Fragment.VideoFragment;

import java.util.ArrayList;
import java.util.List;

public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mFragmentList = new ArrayList<>();
    private  ArrayList<String> mFragmentTitleList = new ArrayList<>();
    FragmentManager manager;

    public SectionsPagerAdapter(FragmentManager manager, ArrayList<Fragment> fragments,ArrayList<String> fragmentstitleList) {
        super(manager);
        this.manager=manager;
        this.mFragmentList = fragments;
        this.mFragmentTitleList=fragmentstitleList;

    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
        notifyDataSetChanged();
    }


    @Override
    public CharSequence getPageTitle(int position) {
//        return mFragmentTitleList.get(position);
        return mFragmentTitleList.get(position);
    }


//    @Override
//    public Parcelable saveState()
//    {
//        Bundle bundle = (Bundle) super.saveState();
//        if (bundle != null)
//        {
//            // Never maintain any states from the base class, just null it out
//            bundle.putParcelableArray("states", null);
//        } else
//        {
//            // do nothing
//        }
//        return bundle;
//    }

}