package com.theratrove.activityList.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.theratrove.R;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DescrptionFragment extends Fragment implements Callback<ResponseBody> {

    Activity activity;

    ApiInterface apiService;
    HttpUrl httpUrl;
    int id = -1;

    public static DescrptionFragment newInstance(String aboutMe, String rating, String year, String education, String avgCost
            , boolean accept, String lic, String theraphist, String issue, int id, boolean hiderating) {
        DescrptionFragment myFragment = new DescrptionFragment();
        Bundle args = new Bundle();
        args.putString("aboutMe", aboutMe);
        args.putString("rating", rating);
        args.putString("year", year);
        args.putString("education", education);
        args.putString("avgCost", avgCost);
        args.putBoolean("accept", accept);
        args.putString("lic", lic);
        args.putSerializable("theraphist", theraphist);
        args.putSerializable("issue", issue);
        args.putInt("id", id);
        args.putBoolean("hiderating", hiderating);
        myFragment.setArguments(args);
        return myFragment;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_desc, container, false);
        RatingBar rating = view.findViewById(R.id.rating);
        TextView txtDes = view.findViewById(R.id.txtDes);
        TextView txttitleAbout = view.findViewById(R.id.txttitleAbout);
        TextView txttitlebasic = view.findViewById(R.id.txttitlebasic);
        TextView txtyear = view.findViewById(R.id.txtyear);
        TextView txtedu = view.findViewById(R.id.txtedu);
        TextView txtavgCost = view.findViewById(R.id.txtavgCost);
        TextView txtAccept = view.findViewById(R.id.txtAccept);
        TextView txtlicense = view.findViewById(R.id.txtlicense);
        TextView txttitletheraphist = view.findViewById(R.id.txttitletheraphist);
        TextView txtThera = view.findViewById(R.id.txtThera);
        TextView txtIssue = view.findViewById(R.id.txtIssue);
        TextView txttitleissue = view.findViewById(R.id.txttitleissue);
        LinearLayout LlRating = view.findViewById(R.id.LlRating);

        apiService = ApiClient.getClient().create(ApiInterface.class);

        if (getArguments() != null) {

            if (getArguments().getBoolean("hiderating")) {
                LlRating.setVisibility(View.GONE);
            }

            if (getArguments().getString("aboutMe") != null && Objects.requireNonNull(getArguments().getString("aboutMe")).length() > 0
                    && !getArguments().getString("aboutMe").equalsIgnoreCase("-")) {
                txtDes.setText(Html.fromHtml(getArguments().getString("aboutMe")));
                txttitleAbout.setVisibility(View.VISIBLE);
            } else {
                txtDes.setVisibility(View.GONE);
                txttitleAbout.setVisibility(View.GONE);
            }

            if (getArguments().getString("year") != null && Objects.requireNonNull(getArguments().getString("year")).length() > 0
                    && !Objects.requireNonNull(getArguments().getString("year")).equalsIgnoreCase("-")) {
                txtyear.setText(getResources().getString(R.string.years_in_practice, Html.fromHtml(getArguments().getString("year"))));
                txtyear.setVisibility(View.VISIBLE);
            }else {
                txtyear.setVisibility(View.GONE);
            }
            if (getArguments().getString("education") != null &&
                    Objects.requireNonNull(getArguments().getString("education")).length() > 0
                    && !Objects.requireNonNull(getArguments().getString("education")).equalsIgnoreCase("-")) {
                txtedu.setText(getResources().getString(R.string.education_datat, Html.fromHtml(getArguments().getString("education"))));
                txtedu.setVisibility(View.VISIBLE);
            }else {
                txtedu.setVisibility(View.GONE);
            }

            if (getArguments().getString("avgCost") != null &&
                    Objects.requireNonNull(getArguments().getString("avgCost")).length() > 0
                    && !Objects.requireNonNull(getArguments().getString("education")).equalsIgnoreCase("-")
                    && !Objects.requireNonNull(getArguments().getString("education")).equalsIgnoreCase("null")) {
                txtavgCost.setText(getResources().getString(R.string.avg_cost_data, " $" + getArguments().getString("avgCost")));
                txtavgCost.setVisibility(View.VISIBLE);
            }else {
                txtavgCost.setVisibility(View.GONE);
            }

            if (getArguments().getBoolean("accept")) {
                txtAccept.setText("Accept Insurance: " + "Yes");
            } else {
                txtAccept.setText("Accept Insurance: " + "No");
            }


            if (getArguments().getString("lic") != null && Objects.requireNonNull(getArguments().getString("lic")).length() > 0
                    && !Objects.requireNonNull(getArguments().getString("lic")).equalsIgnoreCase("-")) {

                txtlicense.setText(getResources().getString(R.string.license_data, Html.fromHtml(getArguments().getString("lic"))));
                txtlicense.setVisibility(View.VISIBLE);
            }else {
                txtlicense.setVisibility(View.GONE);
            }

            if (getArguments().getSerializable("theraphist") != null &&
                    !Objects.requireNonNull(getArguments().getSerializable("theraphist")).equals("")) {
                Log.e("datat", getArguments().getSerializable("theraphist") + "");
                txtThera.setText(getArguments().getSerializable("theraphist") + "");
                txtThera.setVisibility(View.VISIBLE);
                txttitletheraphist.setVisibility(View.VISIBLE);
            } else {
                txttitletheraphist.setVisibility(View.GONE);
                txtThera.setVisibility(View.GONE);
            }

            if (getArguments().getString("issue") != null && Objects.requireNonNull(getArguments().getString("issue")).length() > 0) {
                txtIssue.setText(getArguments().getString("issue") + "");
                txtIssue.setVisibility(View.VISIBLE);
                txtIssue.setVisibility(View.VISIBLE);
            } else {
                txttitleissue.setVisibility(View.GONE);
                txtIssue.setVisibility(View.GONE);
            }


            rating.setRating(Float.parseFloat(getArguments().getString("rating")));


            if (getArguments().getInt("id") != -1) {
                id = getArguments().getInt("id");
            }

            rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    System.out.println("Rate for Module is" + ratingBar.getRating());
                    if (CommonValidation.checkInternetConnection(activity)) {
                        callRating(id + "", ratingBar.getRating());
                    } else {
                        CommonValidation.showToast(getActivity(), getString(R.string.no_internet));
                    }


                }
            });

        }
//

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    public void callRating(String TherapisttId, float rating) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("TherapisttId", TherapisttId);
        jsonObject.addProperty("rating", rating);
        Call<ResponseBody> callLogin = apiService.rating(jsonObject);
        httpUrl = callLogin.request().url();
        callLogin.clone().enqueue(this);
        Utility.showProgress(activity);
    }

    @Override
    public void onResponse(@NonNull Call<ResponseBody> call, Response<ResponseBody> response) {
        Utility.dismissProgress();
        if (response.isSuccessful() && response.code() == 200) {
            try {

                Log.e("Response==for==rating==", response.body().string());

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                CommonValidation.showToast(getActivity(), response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        Utility.dismissProgress();
        t.printStackTrace();
    }
}