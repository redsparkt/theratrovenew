package com.theratrove.activityList.appUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;
import com.theratrove.activityList.model.User;


public class AppSharedPreferences {
    private static AppSharedPreferences appSharedPreferences_ref;
    private SharedPreferences appSharedPreferences;
    private Editor prefsEditor;

    public static AppSharedPreferences getSharePreference(Context context) {
        if (appSharedPreferences_ref == null) {
            appSharedPreferences_ref = new AppSharedPreferences();
            appSharedPreferences_ref.appSharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
            appSharedPreferences_ref.prefsEditor = appSharedPreferences_ref.appSharedPreferences.edit();
        }
        return appSharedPreferences_ref;
    }

    public void setUser(User user) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString("user", json);
        prefsEditor.commit();
    }

    public User getUser() {
        Gson gson = new Gson();
        String json = appSharedPreferences.getString("user", null);
        User obj = gson.fromJson(json, User.class);
        return obj;
    }

    public void setDeviceToken(String regid) {
        prefsEditor.putString("regid", regid);
        prefsEditor.commit();

    }

    public String getRegID() {
        return appSharedPreferences.getString("regid", "");
    }





    public String getUserImage() {
        return appSharedPreferences.getString("profile_picture", "");
    }

    public void setUserImage(String profile_picture) {
        prefsEditor.putString("profile_picture", profile_picture);
        prefsEditor.commit();
    }

    public String getImageName() {
        return appSharedPreferences.getString("image_name", "");
    }

    public void setImageName(String image_name) {
        prefsEditor.putString("image_name", image_name);

        prefsEditor.commit();
    }

    public void setTherapistLogin(boolean islogin) {
        prefsEditor.putBoolean("is_thraphist_login", islogin);
        prefsEditor.commit();
    }

    public void setProfilePic(String data) {
        prefsEditor.putString("profile_pic", data);
        prefsEditor.commit();
    }
    public void setUserName(String data) {
        prefsEditor.putString("username", data);
        prefsEditor.commit();
    }
    public String getUserName() {
        return appSharedPreferences.getString("username", "");
    }

    public void setUserID(String data) {
        prefsEditor.putString("userID", data);
        prefsEditor.commit();
    }

    public void setUserEmail(String data) {
        prefsEditor.putString("email", data);
        prefsEditor.commit();
    }

    public String getUserEmail() {
        return appSharedPreferences.getString("email", "");
    }

    public String getUserID() {
        return appSharedPreferences.getString("userID", "");
    }

    public String getProfileic() {
        return appSharedPreferences.getString("profile_pic", "");
    }

    public boolean getTheraphistLogin() {
        return appSharedPreferences.getBoolean("is_thraphist_login", false);
    }

    public boolean getOnlyTime() {
        return appSharedPreferences.getBoolean("onlyOnetime", false);
    }

    public void setOnlyOnetime(boolean onlyOnetime) {
        prefsEditor.putBoolean("onlyOnetime", onlyOnetime);
        prefsEditor.commit();
    }
    public void setOnlyOnetimeSkip(boolean onlyOnetime) {
        prefsEditor.putBoolean("onlyOnetimeskip", onlyOnetime);
        prefsEditor.commit();
    }
    public boolean getOnlyTimeskip() {
        return appSharedPreferences.getBoolean("onlyOnetimeskip", false);
    }


    public void clearAllData() {
        prefsEditor.clear();
        prefsEditor.commit();
    }


    public String getTocken() {
        return appSharedPreferences.getString("regid", "");
    }
}
