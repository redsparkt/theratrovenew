//
//  LeftMenuCell.swift
//  Theratrove
//
//  Created by Dhruv Patel on 17/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class LeftMenuCell: UITableViewCell {

    @IBOutlet var lblContent: UILabel!
    @IBOutlet var imgContentImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
