package com.theratrove.activityList.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.theratrove.R;
import com.theratrove.activityList.Adapter.BestAdapter;
import com.theratrove.activityList.Adapter.GalleryAdapter;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.model.Profile;
import com.theratrove.activityList.model.SearchData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class GalleryFragment extends Fragment {

    RecyclerView recyclerView;
    ArrayList<Profile.TherapyGalleryList> GalleryList;
    GalleryAdapter adapter;
    Activity activity;

    public static GalleryFragment newInstance(List<Profile.TherapyGalleryList> GalleryList) {
        GalleryFragment myFragment = new GalleryFragment();
        Bundle args = new Bundle();
        args.putSerializable("list", (Serializable) GalleryList);
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_best, container, false);


        Bundle args = getArguments();
        if (args != null) {
            GalleryList = (ArrayList<Profile.TherapyGalleryList>) args.getSerializable("list");
//            Log.e("Size of list==", GalleryList.size() + "");

        }


        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
//        recyclerView.setLayoutManager(new GridLayoutManager(activity, 2));

        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        adapter = new GalleryAdapter(activity, GalleryList);
        recyclerView.setAdapter(adapter);


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            this.activity = (Activity) context;
        }
    }


}