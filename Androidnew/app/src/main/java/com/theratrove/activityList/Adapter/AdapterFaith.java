package com.theratrove.activityList.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.theratrove.R;
import com.theratrove.activityList.model.Faith;

import java.util.ArrayList;

public class AdapterFaith extends RecyclerView.Adapter<AdapterFaith.ViewHolder> {

    private Activity activity;
    private ArrayList<Faith> faithlist;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_faith, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

    }

    public AdapterFaith(ArrayList<Faith> fithdata, Activity activity) {
        try {
            this.activity = activity;
            this.faithlist = fithdata;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return faithlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvContent;
        public AppCompatCheckBox cbSelect;

        public ViewHolder(View v) {
            super(v);
            cbSelect = v.findViewById(R.id.faith_checkbox);
        }


    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
