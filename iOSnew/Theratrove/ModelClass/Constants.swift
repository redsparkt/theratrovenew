//
//  Constants.swift
//  Orion
//
//  Created by Dhruv Patel on 26/09/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import Foundation
import UIKit

class Constants {

    public static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    public static let USERDEFAULTS = UserDefaults.standard
    
    public static let BASE_URL = "http://theratrove-staging.azurewebsites.net"
    
    public static let Login = "/api/TheRatrove/Login"
    public static let Signup = "/api/TheRatrove/SignUp"
    public static let GetPageContent = "/api/TheRatrove/GetPageContent"
    public static let ForgotPassword = "/api/TheRatrove/ForgotPassword"
    
    public static let SendContactEmail = "/api/TheRatrove/SendContactEmail"
    public static let Contact = "/api/TheRatrove/Contact"
    
    public static let GetSearchFilters = "/api/TheRatrove/GetSearchFilters"
    
    public static let Search = "/api/TheRatrove/Search"
    
    public static let UserDetail = "/api/TheRatrove/Get/"
    
    public static let SearchByRating = "/api/TheRatrove/SearchByRating"
    
    public static let EditProfile = "/api/TheRatrove/Profile"
    
    public static let GalleryPicUpload = "/api/TheRatrove/GalleryPicUpload"
    
    public static let ProfilePicUpload = "/api/TheRatrove/ProfilePicUpload"
    
    public static let DeleteGalleryPic = "/api/TheRatrove/DeleteGalleryPic"
    
    public static let AddRaging = "/api/TheRatrove/AddRaging"
    public static let ChangePassword = "/api/TheRatrove/ChangePassword"
   
    
    
    public static let NetworkUnavailable = "Network unavailable. Please check your internet connectivity"
    public static let device_type = "ios" // iOS = 2
}
