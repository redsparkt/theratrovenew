package com.theratrove.activityList.appUtils;


/**
 * Created by Redspark on 1/22/2018.
 * Constant class for storing data that would be accessed throughout application
 */

public class Constant {
    public static final String DEVICE_TYPE = "android";
    public static final int MY_PERMISSIONS_REQUEST_CONTACT = 1001;
    public static final int MY_PERMISSIONS_REQUEST = 1002;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 1003;
    public static CustomDialog custDailog = null;

    //Login Request Parameter
    public static final String LOGIN__USER_KEY = "userName";
    public static final String LOGIN__PASSWORD_KEY = "password";
    public static final String LOGIN__DEVICE_TOKEN = "device_token";
    public static final String LOGIN__DEVICE_TYPE = "device_type";


    //SignUp Request Parameter
    public static final String SIGN_UP_EMAIL = "email";
    public static final String SIGN_UP__CONFIRM_PASSWORD = "confirmPassword";

    //CMS PAge
    public static final String CMS_PAGE_NAME = "pageName";

    public static final String DATA = "DATA";




}
