package com.theratrove.activityList.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.theratrove.R;
import com.theratrove.activityList.model.SearchFilterData;

import java.util.ArrayList;

public class GenderAdapter extends ArrayAdapter<SearchFilterData.GenderOption> {

    LayoutInflater flater;
    private final Activity activity;

    public GenderAdapter(Activity context, ArrayList<SearchFilterData.GenderOption> list){

        super(context,R.layout.item_for,0 ,list);
        flater = context.getLayoutInflater();
        this.activity=context;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        assert convertView != null;
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        assert convertView != null;
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = flater.inflate(R.layout.item_for, parent, false);

        TextView for_text = (TextView) view.findViewById(R.id.for_text);

        SearchFilterData.GenderOption rowItem = getItem(position);
        assert rowItem != null;
        for_text.setText(rowItem.getKey());

        return view;
    }
    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

}