//
//  Nearest.swift
//  Theratrove
//
//  Created by Dhruv Patel on 17/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class Nearest: UIViewController {

    @IBOutlet var tblNearest: UITableView!
     var parentpage = MatchPage()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblNearest.delegate = self
        self.tblNearest.dataSource = self
        self.tblNearest.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension Nearest : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MyCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? BestMatchCell
        if cell == nil {
            var nib = Bundle.main.loadNibNamed("BestMatchCell", owner: self, options: nil)
            cell = nib?[0] as? BestMatchCell
        }
        cell?.selectionStyle = .none
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let Profile = storyboard.instantiateViewController(withIdentifier: "Profile") as? Profile
        Profile?.forDetail = "1"
        parentpage.navigationController?.pushViewController(Profile!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 126
    }
}
