namespace theratrove.data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("User")]
    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            TherapistLikes = new HashSet<TherapistLike>();
            TherapistLikes1 = new HashSet<TherapistLike>();
            TherapyGalleries = new HashSet<TherapyGallery>();
        }

        public int UserId { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(200)]
        public string Email { get; set; }

        [StringLength(200)]
        public string Password { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? LastLogin { get; set; }

        public int? FailedLoginAttempts { get; set; }

        public bool? Locked { get; set; }

        public bool? Active { get; set; }

        public byte? UserType { get; set; }

        [StringLength(50)]
        public string ActivationCode { get; set; }

        [StringLength(1000)]
        public string ProfilePicURL { get; set; }

        public virtual Therapist Therapist { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistLike> TherapistLikes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapistLike> TherapistLikes1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TherapyGallery> TherapyGalleries { get; set; }
    }
}
