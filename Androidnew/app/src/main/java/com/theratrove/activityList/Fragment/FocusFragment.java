package com.theratrove.activityList.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.theratrove.R;


public class FocusFragment extends Fragment {


    public static FocusFragment newInstance(String Faith, String For, String Gender, String Language, String Insurance, String issue) {
        FocusFragment myFragment = new FocusFragment();
        Bundle args = new Bundle();
        args.putString("Faith", Faith);
        args.putString("For", For);
        args.putString("Gender", Gender);
        args.putString("Language", Language);
        args.putString("Insurance", Insurance);
        args.putString("issue", issue);


        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_focus, container, false);

        TextView txtDes = view.findViewById(R.id.txtFaith);
        TextView txtFor = view.findViewById(R.id.txtFor);
        TextView txtGender = view.findViewById(R.id.txtGender);
        TextView txtLanguage = view.findViewById(R.id.txtLanguage);
        TextView txtInsurance = view.findViewById(R.id.txtInsurance);
        TextView txtIssues = view.findViewById(R.id.txtIssues);

        TextView faithlabel = view.findViewById(R.id.faithlabel);
        TextView forlabel = view.findViewById(R.id.forlabel);
        TextView Genderabel = view.findViewById(R.id.Genderabel);
        TextView lnglable = view.findViewById(R.id.lnglable);
        TextView lnslable = view.findViewById(R.id.lnslable);
        TextView lngIssue = view.findViewById(R.id.lngIssue);


        if (getArguments() != null) {

            if (getArguments().getString("Faith") != null && getArguments().getString("Faith").length() > 0) {
                txtDes.setText(getArguments().getString("Faith"));
            } else {
                faithlabel.setVisibility(View.GONE);
            }

            if (getArguments().getString("For") != null && getArguments().getString("For").length() > 0) {
                txtFor.setText(getArguments().getString("For"));
            } else {
                forlabel.setVisibility(View.GONE);
            }

            if (getArguments().getString("Gender") != null && getArguments().getString("Gender").length() > 0) {
                txtGender.setText(getArguments().getString("Gender"));
            } else {
                Genderabel.setVisibility(View.GONE);
            }

            if (getArguments().getString("Language") != null && getArguments().getString("Language").length() > 0) {
                txtLanguage.setText(getArguments().getString("Language"));
            } else {
                lnglable.setVisibility(View.GONE);
            }

            if (getArguments().getString("Insurance") != null && getArguments().getString("Insurance").length() > 0) {
                txtInsurance.setText(getArguments().getString("Insurance"));
            } else {
                lnslable.setVisibility(View.GONE);
            }

            if (getArguments().getString("issue") != null && getArguments().getString("issue").length() > 0) {
                txtIssues.setText(getArguments().getString("issue"));
            } else {
                lngIssue.setVisibility(View.GONE);
            }




        }
//

        return view;
    }
}