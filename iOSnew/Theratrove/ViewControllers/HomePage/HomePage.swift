//
//  HomePage.swift
//  Theratrove
//
//  Created by Dhruv Patel on 27/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class HomePage: UIViewController {
    @IBOutlet var tblHome: UITableView!
    var response = NSMutableArray()
    var ApiCall = Int()
    var pageno = Int()
    var NoRecordFound = Int()
    @IBOutlet var topview_height: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       if Device.DeviceType.IS_IPHON_X{
            self.topview_height.constant = 80
        }
        self.ApiCall = 0
        self.pageno = 1
        self.NoRecordFound = 0
        
        self.DirectHome()

        // Do any additional setup after loading the view.
    }
    @IBAction func btnSearchClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let SearchPage = storyboard.instantiateViewController(withIdentifier: "SearchPage") as? SearchPage
        self.navigationController?.pushViewController(SearchPage ?? UIViewController(), animated: true)
    }
    
    
    @IBAction func btnMenuclick(_ sender: Any) {
         sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height {
            if self.ApiCall == 0 && self.NoRecordFound == 0{
                self.DirectHome()
            }
        }
    }
    
    @objc func onCallTapped(sender: UIButton){
        let ImageData = self.response.object(at:sender.tag) as! NSDictionary
        let Phone = ImageData.value(forKey: "PhoneNumber") as! String
        if !Phone.isEmpty{
            self.call(phoneNumber: Phone)
        }

    }
    
    func call(phoneNumber:String) {
        let cleanPhoneNumber = phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        let urlString:String = "tel://\(cleanPhoneNumber)"
        if let phoneCallURL = URL(string: urlString) {
            if (UIApplication.shared.canOpenURL(phoneCallURL)) {
                UIApplication.shared.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func onEmailTapped(sender: UIButton){
        let ImageData = self.response.object(at:sender.tag) as! NSDictionary
        let Email = ImageData.value(forKey: "Email") as! String
        if !Email.isEmpty{
            
            let url = URL(string: "mailto:\(Email)")
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        }
    }
    func DirectHome(){
        self.ApiCall = 1
        var parameter : [String : Any] = [:]
        //"SelectedFaith": "7"
        
        parameter.updateValue(self.pageno , forKey: "PageNo")
        parameter.updateValue(25, forKey: "PageSize")
        
        Utils().ShowLoader()
        WebService.postMethod(params: parameter as [String : AnyObject], apikey: Constants.Search, completion: { (Json) in
            let Data : NSDictionary = Json as! NSDictionary
            DispatchQueue.main.async {
                let temparray = NSMutableArray()
                temparray.addObjects(from: Data.object(forKey: "Result") as! [Any])
                
                if temparray.count > 0
                {
                    self.pageno = self.pageno + 1
                    self.response.addObjects(from: Data.object(forKey: "Result") as! [Any])
                    self.tblHome.delegate = self
                    self.tblHome.dataSource = self
                    self.tblHome.reloadData()
                }
                else
                {
                    self.NoRecordFound = 1
                }
                self.ApiCall = 0
            }
            
            
        }, failure: {(error) in
            self.ApiCall = 0
        })
        
    }

}

extension HomePage : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.response.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MyCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? BestMatchCell
        if cell == nil {
            var nib = Bundle.main.loadNibNamed("BestMatchCell", owner: self, options: nil)
            cell = nib?[0] as? BestMatchCell
        }
        
        let ItemData = self.response.object(at: indexPath.row) as! NSDictionary
        
        cell?.selectionStyle = .none
        cell?.uvwRating.backgroundColor = UIColor.clear
        cell?.uvwRating.delegate = self as FloatRatingViewDelegate
        cell?.uvwRating.contentMode = UIViewContentMode.scaleAspectFit
        cell?.uvwRating.type = .halfRatings
        cell?.uvwRating.rating = 3
        
        let therapistRating = ItemData.value(forKey: "TherapistRating") as! Double
        cell?.uvwRating.rating = therapistRating
        
        var firstname = ItemData.value(forKey: "FirstName") as? String
        var lastname = ItemData.value(forKey: "LastName") as? String
        if firstname == nil || firstname == "-"{
            firstname = ""
        }
        if lastname == nil || lastname == "-"{
            lastname = ""
        }
        if (firstname?.isEmpty)! && (lastname?.isEmpty)!{
           firstname = ItemData.value(forKey: "Email") as? String
        }
        
        cell?.lblUserName.text = "\(firstname!) \(lastname!)"
        
         var Title = ItemData.value(forKey: "Title") as? String
        if Title == nil || Title == "-"{
            Title = ""
        }
        else {
            Title = Title?.uppercased()
        }
        
        cell?.lblType.text = "\(Title!)"
        
        let aboutme = ItemData.value(forKey: "AboutMe") as? String
        cell?.lblAbout.text = aboutme?.htmlToString
        
        var city = ItemData.value(forKey: "City") as? String
        var State = ItemData.value(forKey: "State") as? String
        if State == nil || State == "-"{
            State = ""
        }
        if city == nil || city == "-"{
            city = ""
        }
        
        cell?.lblAddress.text = "\(city!) \(State!)"
        
        
        let avgcost = ItemData.value(forKey: "AvgCost") as? Int
        if avgcost != nil{
            
            cell?.lblCost.text = "$\(avgcost!)"
        }
        else{
            cell?.lblCost.text = ""
        }
        
        cell?.btnCall.addTarget(self, action: #selector(self.onCallTapped(sender:)), for: UIControlEvents.touchUpInside)
        cell?.btnCall.tag = indexPath.row
        
        cell?.btnMsg.addTarget(self, action: #selector(self.onEmailTapped(sender:)), for: UIControlEvents.touchUpInside)
        cell?.btnMsg.tag = indexPath.row
        
        
        
       var Image = ItemData.object(forKey: "ProfilePicURL") as? String
        if Image == nil{
            Image = ""
        }
        
       if !(Image?.isEmpty)!
       {
           cell?.imgUserImage.moa.url = Image
       }
       else
       {
            cell?.imgUserImage.image = UIImage(named: "user-icon.png")
       }
        
        
        var Phone = ItemData.value(forKey: "PhoneNumber") as? String
        if Phone == nil{
            Phone = ""
        }
        if (Phone?.isEmpty)!{
            cell?.btncall_width.constant = 0
            cell?.btnCall.isHidden = true
        }
        else
        {
            cell?.btncall_width.constant = 27
            cell?.btnCall.isHidden = false
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let ItemData = self.response.object(at: indexPath.row) as! NSDictionary
        let userid = ItemData.value(forKey: "Id") as! Int
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let Profile = storyboard.instantiateViewController(withIdentifier: "Profile") as? Profile
        Profile?.forDetail = "1"
        Profile?.UserId = "\(userid)"
        self.navigationController?.pushViewController(Profile!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let ItemData = self.response.object(at: indexPath.row) as! NSDictionary
        
        var Title = ItemData.value(forKey: "Title") as? String
        if Title == nil{
            Title = ""
        }
        
        if Title != nil && !(Title?.isEmpty)!{
            var labelHeight = CGFloat()
            labelHeight = Utils().height(forText: Title, font: UIFont.systemFont(ofSize: 13), withinWidth: self.view.frame.size.width - 90)

            return labelHeight + 140
        }
        else {
            return 138
       
    }
}
}
extension HomePage: FloatRatingViewDelegate {
    
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        //        liveLabel.text = String(format: "%.2f", self.floatRatingView.rating)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        //        updatedLabel.text = String(format: "%.2f", self.floatRatingView.rating)
    }
    
}
