//
//  ForgotPassword.swift
//  Theratrove
//
//  Created by Dhruv Patel on 18/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class ForgotPassword: UIViewController {

    @IBOutlet var uvwtopview_height: NSLayoutConstraint!
    @IBOutlet var imgBackground: UIImageView!
    @IBOutlet var btnSendPassword: UIButton!
    @IBOutlet var txtEmail: UITextField!
    
    @IBOutlet var uvwMainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Device.DeviceType.IS_IPHONE_5_OR_LESS{
            self.imgBackground.image = UIImage(named: "background_5")
        }
        else if Device.DeviceType.IS_IPHONE_6{
            self.imgBackground.image = UIImage(named: "background_6")
        }
        else if Device.DeviceType.IS_IPHONE_6P{
            self.imgBackground.image = UIImage(named: "background_plus")
        }
        else{
            self.imgBackground.image = UIImage(named: "background_x")
            self.uvwtopview_height.constant = 80
        }
        
        self.txtEmail.setLeftPaddingPoints(5)
        self.txtEmail.CornerRadious(5)
        
        self.uvwMainView.layer.cornerRadius = 5
        self.uvwMainView.layer.masksToBounds = true
        
        self.btnSendPassword.layer.cornerRadius = 5
        self.btnSendPassword.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }
   
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func callapi(){
        self.view.endEditing(true)
       let username = self.txtEmail.text
        if (username?.isEmpty)!{
            Utils().showMessage("User Name is required.")
            return
        }
        
        let parameter : [String : Any] = [
            "userName" : username!
        ]
        
        WebService.postMethod(params: parameter as [String : AnyObject] , apikey: Constants.ForgotPassword, completion: { (Json) in
            let Data : NSDictionary = Json as! NSDictionary
            let Message = Data.value(forKey: "Message") as! String
            Utils().showMessage(Message)
            self.navigationController?.popViewController(animated: true)
        }, failure: {(error) in
            
        })
    }
    
    @IBAction func btnSendPasswordClick(_ sender: Any) {
        self.callapi()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
