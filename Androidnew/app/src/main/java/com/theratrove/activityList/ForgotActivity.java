package com.theratrove.activityList;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.theratrove.R;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theratrove.activityList.appUtils.Constant.DEVICE_TYPE;

public class ForgotActivity extends AppCompatActivity implements Callback<String> {


    ApiInterface apiService;
    HttpUrl httpForgotPWDUrl;
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txttitle)
    TextView txttitle;
    @BindView(R.id.toolbar_top)
    Toolbar toolbarTop;
    @BindView(R.id.edtUserName)
    EditText edtUserName;
    @BindView(R.id.txtSubmit)
    TextView txtSubmit;
    @BindView(R.id.txtSignIn)
    TextView txtSignIn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pwd);
        ButterKnife.bind(this);
        initVIew();

        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    public void initVIew() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_top);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            imgBack = toolbar.findViewById(R.id.imgBack);
            txttitle.setText(getString(R.string.forgot_password_new));
        }


        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    if (CommonValidation.checkInternetConnection(ForgotActivity.this)) {
                        callForgotAPI(edtUserName.getText().toString().trim());
                    } else {
                        CommonValidation.showToast(ForgotActivity.this, getString(R.string.no_internet));
                    }
                }

            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    public void callForgotAPI(String userName) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(Constant.LOGIN__USER_KEY, userName);
        Call<String> callLogin = apiService.forgotPassword(jsonObject);
        httpForgotPWDUrl = callLogin.request().url();
        callLogin.clone().enqueue(this);
        Utility.showProgress(ForgotActivity.this);
    }

    @Override
    public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
        try {
            Utility.dismissProgress();
            if (response.isSuccessful() && response.code() == 200) {
                Log.e("Sucess", "response");
                CommonValidation.showToast(ForgotActivity.this, response.body());
                finish();
            } else {
                try {
                    CommonValidation.showToast(ForgotActivity.this, response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

    }

    @Override
    public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
        Utility.dismissProgress();
        t.printStackTrace();
    }

    public boolean isValidate() {
        if (edtUserName.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(this, getString(R.string.username_blank_msg));
            return false;
        }
        return true;
    }
}
