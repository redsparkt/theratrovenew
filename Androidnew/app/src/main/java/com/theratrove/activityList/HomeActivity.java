package com.theratrove.activityList;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theratrove.activityList.Fragment.AboutUsFragment;
import com.theratrove.activityList.Fragment.ContactUsFragment;
import com.theratrove.activityList.Fragment.HowtoWorkFragment;
import com.theratrove.activityList.Fragment.HomeFragment;
import com.theratrove.activityList.Fragment.ProfileFragment;
import com.theratrove.activityList.Fragment.SearchFragment;
import com.theratrove.activityList.Fragment.SettingFragment;
import com.theratrove.R;
import com.theratrove.activityList.appUtils.AppSharedPreferences;

public class HomeActivity extends AppCompatActivity {

    ImageView imeMenu, imgSearch;
    TextView txttitle;
    public static DrawerLayout drawerLayout;
    LinearLayout llContainer;
    NavigationView nav_view;
    FrameLayout content_frame;
    Toolbar toolbar;
    Runnable runnable;
    Handler mHandler;
    Fragment fragment;
    RequestOptions options;
    public ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        initView();


    }

    public void initView() {

        options = new RequestOptions()
//                .centerCrop()
                .placeholder(R.drawable.dummyuserimage)
                .error(R.drawable.dummyuserimage);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            imeMenu = toolbar.findViewById(R.id.imgBack);
            imeMenu.setImageResource(R.drawable.menu);
            imgSearch = toolbar.findViewById(R.id.imgSearch);
            txttitle = toolbar.findViewById(R.id.txttitle);
            txttitle.setText(getResources().getString(R.string.home));

        }
        drawerLayout = findViewById(R.id.drawer_layout);
        llContainer = findViewById(R.id.llContainer);
        nav_view = findViewById(R.id.nav_view);
//        View headerLayout =
//                nav_view.inflateHeaderView(R.layout.nav_header_main);
//        RelativeLayout header_layout = headerLayout.findViewById(R.id.view_container);
//        content_frame = findViewById(R.id.content_frame);

        mHandler = new Handler();

        if (AppSharedPreferences.getSharePreference(this).getTheraphistLogin()) {
//            nav_view.getMenu().getItem(0).setVisible(false);

            nav_view.getMenu().getItem(5).setVisible(false);
            nav_view.getMenu().getItem(6).setVisible(false);

            nav_view.getMenu().getItem(1).setVisible(true);
            nav_view.getMenu().getItem(2).setVisible(true);
            nav_view.getMenu().getItem(3).setVisible(true);
            nav_view.getMenu().getItem(4).setVisible(true);

//            nav_view.getHeaderView(0).setVisibility(View.VISIBLE);

        } else {
//            nav_view.getMenu().getItem(0).setVisible(false);
            nav_view.getMenu().getItem(1).setVisible(false);
            nav_view.getMenu().getItem(5).setVisible(true);
            nav_view.getMenu().getItem(6).setVisible(true);
            nav_view.getMenu().getItem(7).setVisible(false);
            nav_view.getMenu().getItem(8).setVisible(false);

//            nav_view.getHeaderView(0).setVisibility(View.GONE);

        }

        setUserData();


        imeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    //drawer is open
                    drawerLayout.closeDrawer(Gravity.LEFT);

                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);

                }
            }
        });

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });


//        if (AppSharedPreferences.getSharePreference(HomeActivity.this).getOnlyTime()){
//            displayFrgment(1, true);
//        }else
//        {
//            displayFrgment(1, false);
//        }

        if (getIntent().hasExtra("isfrom")) {
            if (getIntent().getStringExtra("isfrom").equalsIgnoreCase("signin")) {
                displayFrgment(1, true);
            }
        } else {
            displayFrgment(1, false);
        }


        mHandler.post(runnable);

        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                float moveFactor = 0;
                moveFactor = (drawerView.getWidth() * slideOffset);
                llContainer.setTranslationX(moveFactor);
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                invalidateOptionsMenu();
                if (runnable != null) {
                    mHandler.post(runnable);
                    runnable = null;
                }

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                drawerLayout.closeDrawer(GravityCompat.START);
                switch (menuItem.getItemId()) {
//                    case R.id.nav_search:
//
//                        displayFrgment(0);
//
//                        break;
                    case R.id.nav_home:


//                        if (AppSharedPreferences.getSharePreference(HomeActivity.this).getOnlyTime()) {
                            displayFrgment(1, true);
//                        } else {
//                            displayFrgment(1, false);
//                        }


                        break;


                    case R.id.nav_profile:


                        displayFrgment(2, true);
                        break;

//                    case R.id.nav_settings:
//
//
//                        displayFrgment(3);
//                        break;

                    case R.id.nav_sign_out:

                        callLogout();
                        break;

                    case R.id.nav_login:
                        Intent intent = new Intent(HomeActivity.this, SignInActivity.class);
                        intent.putExtra("from", "home");
                        startActivity(intent);
                        break;

                    case R.id.nav_sign_up:
                        Intent intent1 = new Intent(HomeActivity.this, SignUpActivity.class);
                        startActivity(intent1);
                        break;


                    case R.id.nav_contact:
                        displayFrgment(4, true);
                        break;
                    case R.id.nav_about:
                        displayFrgment(5, true);
                        break;
                    case R.id.nav_how_work:
                        displayFrgment(6, true);
                        break;
                    case R.id.nav_change:
                        Intent cha = new Intent(HomeActivity.this, ChangePasswordActivity.class);
                        startActivity(cha);
                        break;


                }

                return false;
            }
        });
    }

    private void callLogout() {
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.logout))
                .setMessage(getResources().getString(R.string.would_you_like_to_logout))
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //callApiLogout();
                        dialog.dismiss();

                        Intent it = new Intent(HomeActivity.this, SignInActivity.class);
                        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(it);
                        finishAffinity();
                        AppSharedPreferences.getSharePreference(HomeActivity.this).setTherapistLogin(false);
                        AppSharedPreferences.getSharePreference(HomeActivity.this).clearAllData();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // user doesn't want to logout
                        dialog.dismiss();
                    }
                })
                .show();
    }

//    private void addFragment(Fragment fragment) {
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.content_frame, fragment);
//        fragmentTransaction.commitAllowingStateLoss();
//    }

    public void displayFrgment(int position, boolean fromHome) {


        fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);

        switch (position) {
            case 0:
                if (!(fragment instanceof SearchFragment)) {
                    loadFragment(SearchFragment.newInstance(), position);
                    txttitle.setText(getResources().getString(R.string.search));
                }
                break;
            case 1:
                if (!(fragment instanceof HomeFragment)) {
                    loadFragment(HomeFragment.newInstance(fromHome), position);
                    txttitle.setText(getResources().getString(R.string.home));
                }
                break;
            case 2:
                if (!(fragment instanceof ProfileFragment)) {
                    loadFragment(ProfileFragment.newInstance(), position);
//                    txttitle.setText(getResources().getString(R.string.home));
                }

                break;
            case 3:
                if (!(fragment instanceof SettingFragment)) {
                    loadFragment(SettingFragment.newInstance(), position);
                    txttitle.setText(getResources().getString(R.string.setting));
                }


                break;
            case 4:

                if (!(fragment instanceof ContactUsFragment)) {
                    loadFragment(ContactUsFragment.newInstance(), position);
                    txttitle.setText(getResources().getString(R.string.contact));
                }


                break;
            case 5:


                if (!(fragment instanceof AboutUsFragment)) {
                    loadFragment(AboutUsFragment.newInstance(), position);
                    txttitle.setText(getResources().getString(R.string.about_us));
                }
                break;
            case 6:

                if (!(fragment instanceof HowtoWorkFragment)) {
                    loadFragment(HowtoWorkFragment.newInstance(), position);
                    txttitle.setText(getResources().getString(R.string.how_it_works));
                }


                break;


        }

    }

    private void loadFragment(final Fragment fragment, final int position) {


//        Handler handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {


                if (fragment != null) {
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                            .beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, fragment)
                            .commit();

                }


                if (position == 1) {
                    imgSearch.setVisibility(View.VISIBLE);
                    toolbar.setVisibility(View.VISIBLE);
                } else if (position == 2) {
                    toolbar.setVisibility(View.GONE);
                    imgSearch.setVisibility(View.VISIBLE);
                } else {
                    imgSearch.setVisibility(View.GONE);
                    toolbar.setVisibility(View.VISIBLE);
                }


            }
        };


//        handler.postDelayed(runnable, 200);


    }

    public void setUserData() {
        View headerview = nav_view.getHeaderView(0);
        imageView = headerview.findViewById(R.id.iv_header_logo);
        TextView tvUserName = headerview.findViewById(R.id.tvUserName);

        if (AppSharedPreferences.getSharePreference(this).getTheraphistLogin()) {
            Glide.with(this).load(AppSharedPreferences.getSharePreference(this).getProfileic()).apply(options).into(imageView);
        } else {
            Glide.with(this).load(R.drawable.app_logo).apply(options).into(imageView);
        }

        tvUserName.setText(AppSharedPreferences.getSharePreference(this).getUserName());
    }


}
