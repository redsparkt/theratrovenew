package com.theratrove.activityList.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;
import com.theratrove.R;
import com.theratrove.activityList.Adapter.BestAdapter;
import com.theratrove.activityList.Adapter.ViewPagerAdapter;
import com.theratrove.activityList.MatchActivity;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.appUtils.EndlessRecyclerOnScrollListener;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.model.FilterDataModel;
import com.theratrove.activityList.model.SearchData;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MostPopularFragment extends Fragment {

    RecyclerView recyclerView;
    FilterDataModel filterDataModel;
    BestAdapter adapter;
    Activity activity;
    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    int page = 1;
    ApiInterface apiService;

    public static MostPopularFragment newInstance(FilterDataModel filterDataModel) {
        MostPopularFragment myFragment = new MostPopularFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constant.DATA, filterDataModel);
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_best, container, false);


        Bundle args = getArguments();
        if (args != null) {
            filterDataModel = (FilterDataModel) args.getSerializable(Constant.DATA);

        }


        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setAdapter(adapter);

        apiService = ApiClient.getClient().create(ApiInterface.class);

        if (CommonValidation.checkInternetConnection(activity)) {
            callGetMOstPopular(filterDataModel.keyword, filterDataModel.SelectedFor, filterDataModel.SelectedFaith,
                    filterDataModel.SelectedGender, filterDataModel.SelectedIssues, filterDataModel.SelectedInsurance, filterDataModel.SelectedLanguage,
                    filterDataModel.MaxDistance, filterDataModel.ZipCode, page, 25);
        } else {
            CommonValidation.showToast(getActivity(), getString(R.string.no_internet));
        }

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(activity, layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page = current_page;
                callGetMOstPopular(filterDataModel.keyword, filterDataModel.SelectedFor, filterDataModel.SelectedFaith,
                        filterDataModel.SelectedGender, filterDataModel.SelectedIssues, filterDataModel.SelectedInsurance, filterDataModel.SelectedLanguage,
                        filterDataModel.MaxDistance, filterDataModel.ZipCode, page, 25);

            }
        };

        recyclerView.addOnScrollListener(endlessRecyclerOnScrollListener);


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            this.activity = (Activity) context;
        }
    }

    public void callGetMOstPopular(String keyword, String SelectedFor, String SelectedFaith, String SelectedGender, String SelectedIssues,
                                   String SelectedInsurance,
                                   String language, String MaxDistance, String zipcode
            , int page, int pageSize) {


        Map<String, Object> jsonParams = new ArrayMap<>();
        try {
            jsonParams.put("Keyword", keyword);
            jsonParams.put("SelectedFor", SelectedFor);
            jsonParams.put("SelectedFaith", new JSONArray(SelectedFaith));
            jsonParams.put("SelectedGender", SelectedGender);
            jsonParams.put("SelectedIssues", new JSONArray(SelectedIssues));
            jsonParams.put("SelectedLanguage", new JSONArray(language));
            jsonParams.put("SelectedInsurance", new JSONArray(SelectedInsurance));
            jsonParams.put("MaxDistance", MaxDistance);
            jsonParams.put("ZipCode", zipcode);
            jsonParams.put("PageNo", page);
            jsonParams.put("PageSize", pageSize);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(jsonParams)).toString());

            Utility.showProgress(activity);
            Call<SearchData> callLogin = apiService.MostRating(body);
            callLogin.clone().enqueue(new Callback<SearchData>() {
                @Override
                public void onResponse(@NonNull Call<SearchData> call, @NonNull Response<SearchData> response) {
                    Utility.dismissProgress();
                    if (response.code() == 200) {
                        try {
                            SearchData most = response.body();
                            if (adapter == null) {
                                assert most != null;
                                adapter = new BestAdapter(activity, most.getResult());
                                recyclerView.setAdapter(adapter);
                            } else {
                                assert most != null;
                                adapter.updateList(most.getResult());
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SearchData> call, Throwable t) {
                    Utility.dismissProgress();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}