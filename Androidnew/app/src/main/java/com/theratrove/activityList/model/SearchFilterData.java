package com.theratrove.activityList.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchFilterData implements Serializable {

    @SerializedName("ForOptions")
    @Expose
    private ArrayList<ForOption> forOptions = null;
    @SerializedName("FaithOpitions")
    @Expose
    private ArrayList<FaithOpition> faithOpitions = null;
    @SerializedName("GenderOptions")
    @Expose
    private ArrayList<GenderOption> genderOptions = null;
    @SerializedName("IssuesOptions")
    @Expose
    private ArrayList<IssuesOption> issuesOptions = null;
    @SerializedName("InsuranceOptions")
    @Expose
    private ArrayList<InsuranceOption> insuranceOptions = null;
    @SerializedName("LanguageOptions")
    @Expose
    private ArrayList<LanguageOption> languageOptions = null;
    @SerializedName("LocationOptions")
    @Expose
    private ArrayList<LocationOption> locationOptions = null;

    public ArrayList<ForOption> getForOptions() {
        return forOptions;
    }

    public void setForOptions(ArrayList<ForOption> forOptions) {
        this.forOptions = forOptions;
    }

    public ArrayList<FaithOpition> getFaithOpitions() {
        return faithOpitions;
    }

    public void setFaithOpitions(ArrayList<FaithOpition> faithOpitions) {
        this.faithOpitions = faithOpitions;
    }

    public ArrayList<GenderOption> getGenderOptions() {
        return genderOptions;
    }

    public void setGenderOptions(ArrayList<GenderOption> genderOptions) {
        this.genderOptions = genderOptions;
    }

    public ArrayList<IssuesOption> getIssuesOptions() {
        return issuesOptions;
    }

    public void setIssuesOptions(ArrayList<IssuesOption> issuesOptions) {
        this.issuesOptions = issuesOptions;
    }

    public ArrayList<InsuranceOption> getInsuranceOptions() {
        return insuranceOptions;
    }

    public void setInsuranceOptions(ArrayList<InsuranceOption> insuranceOptions) {
        this.insuranceOptions = insuranceOptions;
    }

    public ArrayList<LanguageOption> getLanguageOptions() {
        return languageOptions;
    }

    public void setLanguageOptions(ArrayList<LanguageOption> languageOptions) {
        this.languageOptions = languageOptions;
    }

    public ArrayList<LocationOption> getLocationOptions() {
        return locationOptions;
    }

    public void setLocationOptions(ArrayList<LocationOption> locationOptions) {
        this.locationOptions = locationOptions;
    }

    public static class FaithOpition implements Serializable{

        @SerializedName("Key")
        @Expose
        private String key;
        @SerializedName("Value")
        @Expose
        private Integer value;


        private boolean isSelectedFaith=false;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }
        public boolean getSelected() {
            return isSelectedFaith;
        }

        public void setSelected(boolean selected) {
            isSelectedFaith = selected;
        }

    }


    public static  class ForOption implements Serializable {

        @SerializedName("Key")
        @Expose
        private String key;
        @SerializedName("Value")
        @Expose
        private Integer value;
        private boolean isSelectedFaith=false;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public boolean getSelected() {
            return isSelectedFaith;
        }

        public void setSelected(boolean selected) {
            isSelectedFaith = selected;
        }

    }


    public static class GenderOption implements Serializable{

        @SerializedName("Key")
        @Expose
        private String key;
        @SerializedName("Value")
        @Expose
        private Integer value;
        private boolean isSelectedFaith=false;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public boolean getSelected() {
            return isSelectedFaith;
        }

        public void setSelected(boolean selected) {
            isSelectedFaith = selected;
        }

    }


    public static class InsuranceOption implements Serializable {

        @SerializedName("Key")
        @Expose
        private String key;
        @SerializedName("Value")
        @Expose
        private Integer value;
        private boolean isSelectedInsurance=false;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public boolean getSelected() {
            return isSelectedInsurance;
        }

        public void setSelected(boolean selected) {
            isSelectedInsurance = selected;
        }
    }


    public static class IssuesOption implements Serializable {

        @SerializedName("Key")
        @Expose
        private String key;
        @SerializedName("Value")
        @Expose
        private Integer value;

        private boolean isSelectedIssue=false;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }


        public boolean getSelected() {
            return isSelectedIssue;
        }

        public void setSelected(boolean selected) {
            isSelectedIssue = selected;
        }
    }


    public static class LanguageOption implements Serializable{

        @SerializedName("Key")
        @Expose
        private String key;
        @SerializedName("Value")
        @Expose
        private Integer value;

        private boolean isSelectedLanguage=false;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public boolean getSelected() {
            return isSelectedLanguage;
        }

        public void setSelected(boolean selected) {
            isSelectedLanguage = selected;
        }

    }


   public static class LocationOption implements Serializable{

        @SerializedName("Key")
        @Expose
        private String key;
        @SerializedName("Value")
        @Expose
        private String value;
       private boolean isSelectedLanguage=false;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

       public boolean getSelected() {
           return isSelectedLanguage;
       }

       public void setSelected(boolean selected) {
           isSelectedLanguage = selected;
       }

    }
}


