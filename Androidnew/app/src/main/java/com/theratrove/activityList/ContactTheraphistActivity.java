package com.theratrove.activityList;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.theratrove.R;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactTheraphistActivity extends AppCompatActivity implements Callback<ResponseBody> {

    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txttitle)
    TextView txttitle;
    @BindView(R.id.toolbar)
    Toolbar toolbarTop;
    @BindView(R.id.etFullName)
    EditText etFullName;
    @BindView(R.id.cvFullName)
    CardView cvFullName;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etsubject)
    EditText etsubject;
    @BindView(R.id.etDescription)
    EditText etDescription;
    @BindView(R.id.tvSubmit)
    TextView tvSubmit;
    @BindView(R.id.linearLayout2)
    LinearLayout linearLayout2;
    @BindView(R.id.card_subject)
    CardView card_subject;

    @BindView(R.id.txtMessage)
    TextView txtMessage;


    boolean is_hide_subject = false;
    ApiInterface apiService;
    HttpUrl httpUrl;

    int id = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us);
        ButterKnife.bind(this);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        apiService = ApiClient.getClient().create(ApiInterface.class);
        if (getIntent().hasExtra("is_hide")) {
            is_hide_subject = getIntent().getBooleanExtra("is_hide", false);
        }

        if (is_hide_subject) {
            id = getIntent().getIntExtra("id", -1);
            setSupportActionBar(toolbarTop);
            toolbarTop.setVisibility(View.VISIBLE);
            card_subject.setVisibility(View.GONE);
            txtMessage.setVisibility(View.GONE);
            txttitle.setText("Contact");
        }


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    if (CommonValidation.checkInternetConnection(ContactTheraphistActivity.this)) {
                        callContactUs();
                    } else {
                        CommonValidation.showToast(ContactTheraphistActivity.this, getString(R.string.no_internet));
                    }
                }
            }
        });


    }

    public boolean isValidate() {
        if (etFullName.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(ContactTheraphistActivity.this, getString(R.string.full_enter));
            return false;
        } else if (etEmail.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(ContactTheraphistActivity.this, getString(R.string.email_blank));
            return false;
        } else if (!CommonValidation.isValidEmail(etEmail.getText().toString().trim())) {
            CommonValidation.showToast(ContactTheraphistActivity.this, getString(R.string.email__valid_msg));
            return false;
        } else if (etDescription.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(ContactTheraphistActivity.this, getString(R.string.msg_enter));
            return false;
        }

        return true;
    }


    public void callContactUs() {
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("username", etFullName.getText().toString().trim());
            jsonObject.addProperty("emailAddress", etEmail.getText().toString().trim());
            jsonObject.addProperty("therapistId", id);
            jsonObject.addProperty("message", etDescription.getText().toString().trim());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Call<ResponseBody> call = apiService.ContactTOTheraphist(jsonObject);
        httpUrl = call.request().url();
        call.enqueue(this);
        Utility.showProgress(ContactTheraphistActivity.this);
    }

    @Override
    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
        Utility.dismissProgress();
        if (response.isSuccessful() && response.code() == 200) {
            try {

                assert response.body() != null;
                CommonValidation.showToast(this, getString(R.string.succerss));
                finish();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                assert response.errorBody() != null;
                CommonValidation.showToast(ContactTheraphistActivity.this, response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<ResponseBody> call, Throwable t) {
        Utility.dismissProgress();
        t.printStackTrace();
    }

    public void clearAll() {
        etFullName.setText("");
        etEmail.setText("");
        etDescription.setText("");
        etFullName.requestFocus();
    }


}