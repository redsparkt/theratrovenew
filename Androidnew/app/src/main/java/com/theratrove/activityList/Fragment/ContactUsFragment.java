package com.theratrove.activityList.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.theratrove.R;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactUsFragment extends Fragment implements Callback<ResponseBody> {

    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txttitle)
    TextView txttitle;
    @BindView(R.id.toolbar)
    Toolbar toolbarTop;
    @BindView(R.id.etFullName)
    EditText etFullName;
    @BindView(R.id.cvFullName)
    CardView cvFullName;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etsubject)
    EditText etsubject;
    @BindView(R.id.etDescription)
    EditText etDescription;
    @BindView(R.id.tvSubmit)
    TextView tvSubmit;
    @BindView(R.id.linearLayout2)
    LinearLayout linearLayout2;

    @BindView(R.id.constraint)
    ConstraintLayout constraint;
    Unbinder unbinder;

    ApiInterface apiService;
    HttpUrl httpUrlCallLogin;


    public static ContactUsFragment newInstance() {
        ContactUsFragment myFragment = new ContactUsFragment();

        Bundle args = new Bundle();

        return myFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contact_us, container, false);
        unbinder = ButterKnife.bind(this, view);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    if (CommonValidation.checkInternetConnection(Objects.requireNonNull(getActivity()))) {
                        callContactUs();
                    } else {
                        CommonValidation.showToast(getActivity(), getString(R.string.no_internet));
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void callContactUs() {
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("name", etFullName.getText().toString().trim());
            jsonObject.addProperty("email", etEmail.getText().toString().trim());
            jsonObject.addProperty("subject", etsubject.getText().toString().trim());
            jsonObject.addProperty("Message", etDescription.getText().toString().trim());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Call<ResponseBody> callLogin = apiService.Contact(jsonObject);
        httpUrlCallLogin = callLogin.request().url();
        callLogin.clone().enqueue(this);
        Utility.showProgress(getActivity());
    }

    public boolean isValidate() {
        if (etFullName.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(getActivity(), getString(R.string.full_enter));
            return false;
        } else if (etEmail.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(getActivity(), getString(R.string.email_blank));
            return false;
        } else if (!CommonValidation.isValidEmail(etEmail.getText().toString().trim())) {
            CommonValidation.showToast(getActivity(), getString(R.string.email__valid_msg));
            return false;
        } else if (etsubject.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(getActivity(), getString(R.string.sub_enter));
            return false;
        } else if (etDescription.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(getActivity(), getString(R.string.msg_enter));
            return false;
        }

        return true;
    }

    @Override
    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
        Utility.dismissProgress();
        if (response.isSuccessful() && response.code() == 200) {
            try {

                assert response.body() != null;
                CommonValidation.showToast(getActivity(), getString(R.string.succerss));
                clearAll();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                assert response.errorBody() != null;
                CommonValidation.showToast(getActivity(), response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<ResponseBody> call, Throwable t) {
        Utility.dismissProgress();
        t.printStackTrace();
    }

    public void clearEdiText() {

    }

    public void clearAll() {
        etFullName.setText("");
        etEmail.setText("");
        etsubject.setText("");
        etDescription.setText("");
        etFullName.requestFocus();
    }

}