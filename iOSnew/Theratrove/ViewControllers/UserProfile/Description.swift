//
//  Description.swift
//  Theratrove
//
//  Created by Dhruv Patel on 18/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class Description: UIViewController {

    @IBOutlet var btnPreview: UIButton!
    @IBOutlet var uvwRatingMain_Height: NSLayoutConstraint!
    var fromdetail = String()

    @IBOutlet var lblAboutMe_Width: NSLayoutConstraint!
    @IBOutlet var lblabout_height: NSLayoutConstraint!
    @IBOutlet var aboutMeLabel_height: NSLayoutConstraint!
    @IBOutlet var lblAboutMeLabel: UILabel!
    
    @IBOutlet var lblBAsicInfoLabel: UILabel!
    @IBOutlet var lblBasicInfolbl_height: NSLayoutConstraint!
    
    @IBOutlet var lbltytpe_height: NSLayoutConstraint!
    @IBOutlet var lbltypelbl_height: NSLayoutConstraint!
    
    @IBOutlet var lblLicense_height: NSLayoutConstraint!
    @IBOutlet var lblInsurance_height: NSLayoutConstraint!
    @IBOutlet var lblCost_height: NSLayoutConstraint!
    @IBOutlet var lblEdu_Height: NSLayoutConstraint!
    @IBOutlet var lblExp_Height: NSLayoutConstraint!
    
    @IBOutlet var lblIssuetitle: UILabel!
    @IBOutlet var lblIssues: UILabel!
    @IBOutlet var lblCost: UILabel!
    @IBOutlet var lblPhone: UILabel!
    @IBOutlet var lblLicense: UILabel!
    @IBOutlet var lblEducation: UILabel!
    
    @IBOutlet var lblIssue_height: NSLayoutConstraint!
    @IBOutlet var scrollview_height: NSLayoutConstraint!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblTypeTitle: UILabel!
    @IBOutlet var lblAcceptInsurance: UILabel!
    @IBOutlet var lblYearExperience: UILabel!
    @IBOutlet var lblAboutMe: UILabel!
    var dictData = NSDictionary()
    
    @IBOutlet var uvwRating: FloatRatingView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.fromdetail == "1"{
            self.uvwRating.isHidden = false
            self.uvwRatingMain_Height.constant = 60
            self.uvwRating.isUserInteractionEnabled = true
            self.btnPreview.isHidden = false
        }else{
            self.uvwRating.isHidden = true
            self.uvwRatingMain_Height.constant = 0
            self.uvwRating.isUserInteractionEnabled = false
            self.btnPreview.isHidden = true
        }
        
         let therapistRating = self.dictData.value(forKey: "TherapistRating") as! Double
        uvwRating.backgroundColor = UIColor.clear
        uvwRating.delegate = self
        uvwRating.contentMode = UIViewContentMode.scaleAspectFit
        uvwRating.type = .halfRatings
        self.uvwRating.rating = therapistRating
        self.lblAboutMe_Width.constant = self.view.frame.size.width - 20

        
        var Description = self.dictData.value(forKey: "AboutMe") as? String
        if Description == nil{
            Description = ""
        }
        if (Description?.isEmpty)! || Description == "-"{
             self.lblAboutMe.isHidden = true
             self.lblAboutMeLabel.isHidden = true
             self.aboutMeLabel_height.constant = 0
             self.lblabout_height.constant = 0
        }
        else{
            self.lblAboutMe.isHidden = false
            self.lblAboutMeLabel.isHidden = false
            self.lblAboutMe.text = Description?.htmlToString
            var DescHeight = CGFloat()
            DescHeight = Utils().height(forText: Description?.htmlToString, font: UIFont.systemFont(ofSize: 17), withinWidth: self.lblAboutMe.frame.size.width)
            self.lblabout_height.constant = DescHeight
        }
        
        
        var Yearinservice = self.dictData.value(forKey: "YearsInService") as? String
        var Education = self.dictData.value(forKey: "Education") as? String
        let AvgCost = self.dictData.value(forKey: "AvgCost") as? Int
        let AcceptInsurance = self.dictData.value(forKey: "AcceptInsurance") as? Int
        var License = self.dictData.value(forKey: "License") as? String
        var ISsueFilter = self.dictData.value(forKey: "IssuesFilter") as? String
        var typeFilter = self.dictData.value(forKey: "TherapistTypesOfTherapyFilter") as? String
        
        if Yearinservice == nil{
            Yearinservice = ""
        }
        if !((Yearinservice?.isEmpty)!) && Yearinservice != "-"{
            let formattedString = NSMutableAttributedString()
            formattedString
                .bold("Years In Practice : ")
                .normal("\(Yearinservice!)")
            self.lblYearExperience.attributedText = formattedString
            self.lblExp_Height.constant = 21
            var Height = CGFloat()
            Height = Utils().height(forText: self.lblYearExperience.text, font: UIFont.systemFont(ofSize: 17), withinWidth: self.lblYearExperience.frame.size.width)
            self.lblExp_Height.constant = Height
        }
        else{
            self.lblYearExperience.isHidden = true
            self.lblExp_Height.constant = 0
        }
        if Education == nil{
            Education = ""
        }
        
        if !((Education?.isEmpty)!) && Education != "-"{
        let formattedString = NSMutableAttributedString()
        formattedString
            .bold("Education : ")
            .normal("\(Education!)")
        self.lblEducation.attributedText = formattedString
            self.lblEdu_Height.constant = 21
            var Height = CGFloat()
            Height = Utils().height(forText: self.lblEducation.text, font: UIFont.systemFont(ofSize: 17), withinWidth: self.lblEducation.frame.size.width)
            self.lblEdu_Height.constant = Height
        }
        else{
            self.lblEducation.isHidden = true
            self.lblEdu_Height.constant = 0
        }
        
      
        
        if AvgCost != nil
        {
            self.lblCost.isHidden = false
            let formattedString3 = NSMutableAttributedString()
            formattedString3
                .bold("Average Cost : ")
                .normal("$\(AvgCost!)")
            self.lblCost.attributedText = formattedString3
            self.lblCost_height.constant = 21
            var Height = CGFloat()
            Height = Utils().height(forText:  self.lblCost.text, font: UIFont.systemFont(ofSize: 17), withinWidth: self.lblCost.frame.size.width)
            self.lblCost_height.constant = Height
        }
        else{
            self.lblCost.isHidden = true
            self.lblCost_height.constant = 0
        }
        
        if AcceptInsurance == 0{
            let formattedString1 = NSMutableAttributedString()
        formattedString1
            .bold("Accept Insurance : ")
            .normal("No")
        self.lblAcceptInsurance.attributedText = formattedString1
            self.lblInsurance_height.constant = 21
            var Height = CGFloat()
            Height = Utils().height(forText:  self.lblAcceptInsurance.text, font: UIFont.systemFont(ofSize: 17), withinWidth: self.lblAcceptInsurance.frame.size.width)
            self.lblInsurance_height.constant = Height
        }
            
         else if AcceptInsurance == 1 {
            let formattedString1 = NSMutableAttributedString()
            formattedString1
                .bold("Accept Insurance :  ")
                .normal("Yes")
            self.lblAcceptInsurance.attributedText = formattedString1
            
            self.lblInsurance_height.constant = 21
            var Height = CGFloat()
            Height = Utils().height(forText:  self.lblAcceptInsurance.text, font: UIFont.systemFont(ofSize: 17), withinWidth: self.lblAcceptInsurance.frame.size.width)
            self.lblInsurance_height.constant = Height
        }
        else{
            self.lblAcceptInsurance.isHidden = true
             self.lblInsurance_height.constant = 0
        }
        
        if License == nil{
            License = ""
        }
        
        if !((License?.isEmpty)!) && License != nil && License != "-"{
            let formattedString2 = NSMutableAttributedString()
        formattedString2
            .bold("License : ")
            .normal("\(License!)")
        self.lblLicense.attributedText = formattedString2
            self.lblLicense_height.constant = 21
            var Height = CGFloat()
            Height = Utils().height(forText:  self.lblLicense.text, font: UIFont.systemFont(ofSize: 17), withinWidth: self.lblLicense.frame.size.width)
            self.lblLicense_height.constant = Height
        }
        else{
            self.lblLicense.isHidden = true
            self.lblLicense_height.constant = 0
        }
        
        if ISsueFilter == nil{
            ISsueFilter = ""
        }
        
        if !((ISsueFilter?.isEmpty)!){
            self.lblIssues.isHidden = false
            self.lblIssuetitle.isHidden = false
            self.lblIssues.text = ISsueFilter
            self.lblIssue_height.constant = 21
            var Height = CGFloat()
            Height = Utils().height(forText:  self.lblIssues.text, font: UIFont.systemFont(ofSize: 17), withinWidth: self.lblIssues.frame.size.width)
            self.lblIssue_height.constant = Height
        }
        else{
            self.lblIssues.isHidden = true
            self.lblIssuetitle.isHidden = true
            self.lblIssue_height.constant = 0
        }
        
        if typeFilter == nil{
            typeFilter = ""
        }
        
        if  !((typeFilter?.isEmpty)!){
             self.lblType.isHidden = false
            self.lblTypeTitle.isHidden = false
            self.lblType.text = typeFilter
            self.lbltypelbl_height.constant = 21
            self.lbltytpe_height.constant = 21
            var Height = CGFloat()
            Height = Utils().height(forText:  self.lblType.text, font: UIFont.systemFont(ofSize: 17), withinWidth: self.lblType.frame.size.width)
            self.lbltytpe_height.constant = Height
        }
        else{
            self.lblType.isHidden = true
            self.lblTypeTitle.isHidden = true
            self.lbltytpe_height.constant = 0
            self.lbltypelbl_height.constant = 0
        }
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func RatingApi(rating_value : Double){
        
        let userid = self.dictData.value(forKey: "UserId")
        
        let parameter : [String : Any] = [
            "TherapisttId" : userid!,
            "rating" : rating_value
        ]
        
        WebService.postMethod(params: parameter as [String : AnyObject], apikey: Constants.AddRaging, completion: { (Json) in
            let data : NSDictionary = Json as! NSDictionary
            let ratingAverage = data.value(forKey: "ratingAverate") as! Double
            self.uvwRating.rating = ratingAverage
            Utils().showMessage("Rating Sucessfully Given.")
        }, failure: {(Error) in
            
        })
    }
}

extension Description: FloatRatingViewDelegate {
    
    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        let rating_value = rating
        self.RatingApi(rating_value: rating_value)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        
    }
    
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont.systemFont(ofSize: 15)]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}
