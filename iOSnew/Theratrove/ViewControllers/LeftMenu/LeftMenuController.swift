//
//  LeftMenuController.swift
//  Theratrove
//
//  Created by Dhruv Patel on 17/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import LGSideMenuController
import UIKit

class LeftMenuController: UIViewController,LGSideMenuDelegate {
    
    @IBOutlet var imgprofile_width: NSLayoutConstraint!
    var MyDetailDictionary = NSDictionary()
    @IBOutlet var lblUserName_height: NSLayoutConstraint!
    @IBOutlet var imgProfile_height: NSLayoutConstraint!
    @IBOutlet var imgBackground: UIImageView!
    @IBOutlet var imgProfile: UIImageView!
  
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet var tblItemList: UITableView!
    @IBOutlet var lblUserName: UILabel!
    var arrContent = NSArray()
    var arrContentImg = NSArray()
    override func viewDidLoad() {
        super.viewDidLoad()
       sideMenuController?.delegate = self

        
        if Constants.appDelegate.beforLogin == 0
        {
            
            self.imgProfile.isHidden = false
            self.lblUserName.isHidden = true
            self.lblUserName_height.constant = 0
            self.imgProfile_height.constant = 100
            self.imgProfile.image = UIImage(named: "app_logo")
            
            
            self.arrContent = ["Home","How it Works","About Us","Contact Us","Login","Sign Up"]
            self.arrContentImg = ["home","Howtowork","AboutUs","ContactUs","login","signup_icon"]
        }
        else
        {
            self.imgProfile.layer.borderWidth = 4
            self.imgProfile.layer.borderColor = UIColor.white.cgColor
            self.imgProfile.layer.masksToBounds = true
            let outData = UserDefaults.standard.data(forKey: "LoggedInUserDict")
            self.MyDetailDictionary = NSKeyedUnarchiver.unarchiveObject(with: outData!) as! NSDictionary
            
            var firstname = self.MyDetailDictionary.value(forKey: "FirstName") as? String
            var lastname = self.MyDetailDictionary.value(forKey: "LastName") as? String
            if firstname == nil{
                firstname = ""
            }
            if lastname == nil{
                lastname = ""
            }
            if (firstname?.isEmpty)! && (lastname?.isEmpty)!{
                firstname = self.MyDetailDictionary.value(forKey: "UserName") as? String
            }
            self.lblUserName.text = "\(firstname!) \(lastname!)"
            
            let imagedata = UserDefaults.standard.value(forKey: "profilepic") as? String
            if imagedata != nil && !(imagedata?.isEmpty)!{
                self.imgProfile.moa.url = imagedata
            }
            else
            {
                let imagedata = self.MyDetailDictionary.value(forKey: "ProfilePic") as? String
                if imagedata != nil && !(imagedata?.isEmpty)!{
                    self.imgProfile.moa.url = imagedata
                }
            }
            
            self.lblUserName_height.constant = 21
            self.imgProfile_height.constant = 100
           // self.imgProfile.image = self.convierteImagen(cadenaImagen: Imagevalue as! String)
            self.arrContent = ["Home","Profile","How it Works","About Us","Contact Us","Change Password","Sign Out"]
            self.arrContentImg = ["home","profile","Howtowork","AboutUs","ContactUs","change_password_icon","logout"]
        }
        
         //perform(#selector(resizeProfileImage), with: nil, afterDelay:2)
        NotificationCenter.default.addObserver(self, selector: #selector(methodOfReceivedNotification(notification:)), name:NSNotification.Name(rawValue: "profilepicUpdate"), object: nil)
        
    
        
        // Do any additional setup after loading the view.
    }
    
    @objc func resizeProfileImage()
    {
        let image = self.imgProfile.image       
        let newImage = Utils().resizeImage(image: image!, height: 90, Width: 90)
        self.imgProfile_height.constant = newImage.size.height
        self.imgprofile_width.constant = newImage.size.width
        self.imgProfile.image = newImage
    }
    
    @objc func methodOfReceivedNotification(notification: NSNotification){
        // Take Action on Notification
        if Constants.appDelegate.beforLogin == 0
        {
            self.imgProfile_height.constant = 100
            self.imgProfile.image = UIImage(named: "app_logo")
        }
        else
        {
            let imagedata = UserDefaults.standard.value(forKey: "profilepic") as? String
            if imagedata != nil && !(imagedata?.isEmpty)!{
                self.imgProfile.moa.url = imagedata
            }
        }
        
        // perform(#selector(resizeProfileImage), with: nil, afterDelay:0.3)
    }
    
    func convierteImagen(cadenaImagen: String) -> UIImage? {
        var strings = cadenaImagen.components(separatedBy: ",")
        var bytes = [UInt8]()
        for i in 0..<strings.count {
            if let signedByte = Int8(strings[i]) {
                bytes.append(UInt8(bitPattern: signedByte))
            } else {
                // Do something with this error condition
            }
        }
        let datos: NSData = NSData(bytes: bytes, length: bytes.count)
        return UIImage(data: datos as Data) // Note it's optional. Don't force unwrap!!!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didShowLeftView(_ leftView: UIView, sideMenuController: LGSideMenuController) {
        if Constants.appDelegate.beforLogin == 0
        {
            
        }
        else
        {
            
            let imagedata = Constants.appDelegate.value(forKey: "ProfilePic") as? String
            if imagedata != nil && !(imagedata?.isEmpty)!{
                self.imgProfile.moa.url = imagedata
            }
        }
    }
    
    func willShowLeftView(_ leftView: UIView, sideMenuController: LGSideMenuController) {
        if Constants.appDelegate.beforLogin == 0
        {
            
        }
        else
        {
            
            let imagedata = Constants.appDelegate.value(forKey: "ProfilePic") as? String
            if imagedata != nil && !(imagedata?.isEmpty)!{
                self.imgProfile.moa.url = imagedata
            }
        }
    }
    
    
}


extension LeftMenuController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrContent.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "MyCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? LeftMenuCell
        if cell == nil {
            var nib = Bundle.main.loadNibNamed("LeftMenuCell", owner: self, options: nil)
            cell = nib?[0] as? LeftMenuCell
        }
        cell?.selectionStyle = .none
        cell?.lblContent.text = self.arrContent.object(at: indexPath.row) as? String
        cell?.imgContentImg.image = UIImage(named: (self.arrContentImg.object(at: indexPath.row) as? String)!)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if Constants.appDelegate.beforLogin == 0
        {
            let mainViewController = sideMenuController!
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            if indexPath.row == 0{
                let viewController = storyboard.instantiateViewController(withIdentifier: "HomePage") as? HomePage
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(viewController!, animated: false)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 1{
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let webview = storyboard.instantiateViewController(withIdentifier: "webview") as? webview
                webview?.Title = "How It Works"
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(webview!, animated: false)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 2{
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let webview = storyboard.instantiateViewController(withIdentifier: "webview") as? webview
                webview?.Title = "About Us"
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(webview!, animated: false)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
             //About Us
            }
            if indexPath.row == 3{
                let viewController = storyboard.instantiateViewController(withIdentifier: "ContactUs") as? ContactUs
                viewController?.strTitle = "Contact Us"
                
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(viewController!, animated: false)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 4{
                Constants.appDelegate.setRootview(GuestUser: "0")
            }
            if indexPath.row == 5{
                Constants.appDelegate.setRootview(GuestUser: "1")
            }
           

        }
        else
        {
            let mainViewController = sideMenuController!
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            
          
            if indexPath.row == 0{
                let viewController = storyboard.instantiateViewController(withIdentifier: "HomePage") as? HomePage
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(viewController!, animated: false)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 1{
                let viewController = storyboard.instantiateViewController(withIdentifier: "Profile") as? Profile
                viewController?.forDetail = "0"
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(viewController!, animated: false)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 2{
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let webview = storyboard.instantiateViewController(withIdentifier: "webview") as? webview
                webview?.Title = "How It Works"
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(webview!, animated: false)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 3{
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let webview = storyboard.instantiateViewController(withIdentifier: "webview") as? webview
                webview?.Title = "About Us"
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(webview!, animated: false)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 4{
                let viewController = storyboard.instantiateViewController(withIdentifier: "ContactUs") as? ContactUs
                viewController?.strTitle = "Contact Us"
                
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(viewController!, animated: false)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
            if indexPath.row == 5{
                let viewController = storyboard.instantiateViewController(withIdentifier: "ChangePassword") as? ChangePassword
                let navigationController = mainViewController.rootViewController as! UINavigationController
                navigationController.pushViewController(viewController!, animated: false)
                mainViewController.hideLeftView(animated: true, completionHandler: nil)
            }
        
            if indexPath.row == 6{
                let alert = UIAlertController(title: "Theratrove", message: "Are you sure you want to SignOut?", preferredStyle: .alert)
                let yesButton = UIAlertAction(title: "Sign Out", style: .destructive, handler: { action in
                    
                    Constants.USERDEFAULTS.removeObject(forKey: "username")
                    Constants.USERDEFAULTS.removeObject(forKey: "LoggedInUserDict")
                    Constants.USERDEFAULTS.removeObject(forKey: "profilepic")
                    
                    Constants.appDelegate.beforLogin = 0
                    
                    self.appDelegate.setRootview()
                })
                
                let noButton = UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    //Handle no, thanks button
                })
                alert.addAction(noButton)
                alert.addAction(yesButton)
                present(alert, animated: true)
            }
        }
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
