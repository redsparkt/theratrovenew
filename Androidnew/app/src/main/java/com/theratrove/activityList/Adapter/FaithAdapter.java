package com.theratrove.activityList.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.theratrove.R;
import com.theratrove.activityList.model.Faith;
import com.theratrove.activityList.model.SearchFilterData;

import java.util.ArrayList;

public class FaithAdapter extends RecyclerView.Adapter<FaithAdapter.ViewHolder> {

    private ArrayList<SearchFilterData.FaithOpition> myItems = new ArrayList<>();
    Activity activity;
    private LayoutInflater inflater;
    private String faith;

    public FaithAdapter(Activity activity, ArrayList<SearchFilterData.FaithOpition> getItems) {
        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        myItems = getItems;
        this.faith=faith;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public AppCompatCheckBox cbSelect;

        public ViewHolder(View v) {
            super(v);
            cbSelect = (AppCompatCheckBox) v.findViewById(R.id.faith_checkbox);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.item_faith, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final SearchFilterData.FaithOpition objIncome = myItems.get(position);
        holder.cbSelect.setText(objIncome.getKey());

        //if true, your checkbox will be selected, else unselected
        holder.cbSelect.setChecked(objIncome.getSelected());
        holder.cbSelect.setTag(holder.getAdapterPosition());
        holder.cbSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
                Integer pos = (Integer) holder.cbSelect.getTag();
                if (myItems.get(holder.getAdapterPosition()).getSelected()) {
                    myItems.get(holder.getAdapterPosition()).setSelected(false);
                } else {
                    myItems.get(holder.getAdapterPosition()).setSelected(true);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return myItems.size();
    }

    public ArrayList<SearchFilterData.FaithOpition> getSelectFaith() {
            ArrayList<SearchFilterData.FaithOpition> selectedfaith = new ArrayList<>();
        for (SearchFilterData.FaithOpition c : myItems) {
            if (c.getSelected())
                selectedfaith.add(c);
        }

        return selectedfaith;
    }
    @Override
    public int getItemViewType(int position)
    {
        return position;
    }
}