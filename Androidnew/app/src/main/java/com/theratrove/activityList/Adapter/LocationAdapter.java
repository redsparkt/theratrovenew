package com.theratrove.activityList.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.theratrove.R;
import com.theratrove.activityList.model.SearchFilterData;

import java.util.ArrayList;

public class LocationAdapter extends ArrayAdapter<SearchFilterData.LocationOption> {

    LayoutInflater flater;
    private final Activity activity;

    public LocationAdapter(Activity context, ArrayList<SearchFilterData.LocationOption> list) {

        super(context, R.layout.item_for, 0, list);
        flater = LayoutInflater.from(context);;
        this.activity = context;
    }



    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        assert convertView != null;
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        assert convertView != null;
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = flater.inflate(R.layout.item_for, parent, false);

        TextView for_text = (TextView) view.findViewById(R.id.for_text);

        SearchFilterData.LocationOption rowItem = getItem(position);
        assert rowItem != null;
        for_text.setText(rowItem.getKey());




        return view;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

}