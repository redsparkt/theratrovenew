package com.theratrove.activityList.network;

import com.google.gson.JsonObject;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.model.Profile;
import com.theratrove.activityList.model.SearchData;
import com.theratrove.activityList.model.SearchFilterData;


import org.json.JSONObject;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Redspark on 2/20/2017.
 */
public interface ApiInterface {

    @POST("Login")
    Call<JsonObject> login(@Body JsonObject jsonObject);


    @POST("SignUp")
    Call<JsonObject> signUp(@Body JsonObject jsonObject);

    @POST("ForgotPassword")
    Call<String> forgotPassword(@Body JsonObject jsonObject);

    @POST("ChangePassword ")
    Call<String> ChangePassword(@Body JsonObject jsonObject);

    @POST("Profile")
    Call<ResponseBody> EditProfile(@Body RequestBody jsonObject);

    @GET("GetPageContent")
    Call<ResponseBody> CmsPage(@Query(Constant.CMS_PAGE_NAME) String pageName);

    @POST("Contact")
    Call<ResponseBody> Contact(@Body JsonObject jsonObject);

    @POST("SendContactEmail")
    Call<ResponseBody> ContactTOTheraphist(@Body JsonObject jsonObject);

    @GET("GetSearchFilters")
    Call<SearchFilterData> getSearchFilterData();


    @POST("Search")
    Call<SearchData> search(@Body RequestBody jsonObject);


    @GET
    Call<Profile> getProfileData(@Url String url);

    @POST("SearchByRating")
    Call<SearchData> MostRating(@Body RequestBody jsonObject);


    @POST("ProfilePicUpload")
    Call<ResponseBody> ProfilePicUpload(@Body RequestBody jsonObject);


    @POST("AddRaging")
    Call<ResponseBody> rating(@Body JsonObject jsonObject);

    @POST("DeleteGalleryPic")
    Call<ResponseBody> deleteHalleryPic(@Body JsonObject jsonObject);

    @POST("GalleryPicUpload")
    Call<ResponseBody> galleryPicUpload(@Body RequestBody jsonObject);










}

