package com.theratrove.activityList.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.theratrove.R;
import com.theratrove.activityList.appUtils.ProportionalImageView;

import java.util.ArrayList;

public class IntroPagerAdapte extends PagerAdapter {


    private int mSize;
    private ArrayList<Integer> integerArrayList = new ArrayList<>();
    Activity activity;
    LayoutInflater inflater;


    public IntroPagerAdapte(Activity activity, ArrayList<Integer> integerArrayList) {
        this.activity = activity;
        this.integerArrayList = integerArrayList;
    }


    @Override
    public int getCount() {
        return integerArrayList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup view, int position, @NonNull Object object) {
        view.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, int position) {
        ImageView image;
        inflater = (LayoutInflater) activity
                .getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.item_intro, view,
                false);

        image = itemView.findViewById(R.id.image);
        image.setImageResource(integerArrayList.get(position));
//        textView.setScaleType(ImageView.ScaleType.FIT_XY);
//        textView.setAdjustViewBounds(true);

//        view.addView(textView, ViewGroup.LayoutParams.MATCH_PARENT,
        ((ViewPager) view).addView(itemView);

//        view.addView(image);
        return itemView;
    }

}