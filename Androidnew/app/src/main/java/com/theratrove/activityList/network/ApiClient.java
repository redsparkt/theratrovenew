package com.theratrove.activityList.network;




import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Redspark on 2/20/2017.
 */
public class ApiClient {



    public static final String BASE_URL = "http://theratrove-staging.azurewebsites.net/api/TheRatrove/";
//    public static final String CMS_BASE_URL="https://redspark.biz/hateCrime/frontend/web/index.php?r=site/get-page&page=";
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.MINUTES)
                .readTimeout(60, TimeUnit.MINUTES)

                .writeTimeout(60, TimeUnit.MINUTES)
//                .addNetworkInterceptor(new StethoInterceptor())
                .build();


        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())


                    .build();
        }
        return retrofit;
    }
}
