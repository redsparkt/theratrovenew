package com.theratrove.activityList.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.theratrove.R;
import com.theratrove.activityList.Adapter.BestAdapter;
import com.theratrove.activityList.Adapter.IntroPagerAdapte;
import com.theratrove.activityList.HomeActivity;
import com.theratrove.activityList.IntroductionActivity;
import com.theratrove.activityList.appUtils.AppSharedPreferences;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.EndlessRecyclerOnScrollListener;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.model.SearchData;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {
    //
//    RecyclerView recycler_view;
    Activity activity;
    BestAdapter adapter;
    ApiInterface apiService;
    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    int page = 1;

    ArrayList<Integer> imagelist = new ArrayList<>();
    @BindView(R.id.txtSkip)
    TextView txtSkip;
    @BindView(R.id.txttitle)
    TextView txttitle;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    Unbinder unbinder;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.llMain)
    LinearLayout llMain;

    @BindView(R.id.framlayout)
    FrameLayout framlayout;

    public static HomeFragment newInstance(boolean fromHome) {
        HomeFragment myFragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putBoolean("fromHome", fromHome);
        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);


        unbinder = ButterKnife.bind(this, view);
        initView();


//        recycler_view = view.findViewById(R.id.recycler_view);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);


        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(activity, layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page = current_page;
                callSearchServiceCall();

            }
        };


        if ((getArguments() != null && getArguments().getBoolean("fromHome"))) {
            boolean isfromHome = getArguments().getBoolean("fromHome", false);
            if (isfromHome) {
                hidelayout();
            }
        }


        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidelayout();
                AppSharedPreferences.getSharePreference(activity).setOnlyOnetime(true);
            }
        });

        recyclerView.addOnScrollListener(endlessRecyclerOnScrollListener);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;

    }

    public void initView() {
        imagelist.add(R.drawable.intro1);
        imagelist.add(R.drawable.intro2);
        imagelist.add(R.drawable.intro3);
        IntroPagerAdapte adapter = new IntroPagerAdapte(activity, imagelist);
        viewpager.setAdapter(adapter);
        indicator.setViewPager(viewpager);

        recyclerView.setVisibility(View.GONE);

//        txtSkip.setVisibility(View.GONE);
        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hidelayout();
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void hidelayout() {
        recyclerView.setVisibility(View.VISIBLE);
        framlayout.setVisibility(View.GONE);
        viewpager.setVisibility(View.GONE);
        llMain.setBackgroundColor(Color.WHITE);

        if (CommonValidation.checkInternetConnection(activity)) {
            callSearchServiceCall();
        } else {
            CommonValidation.showToast(getActivity(), getString(R.string.no_internet));
        }
    }

    public void callSearchServiceCall() {

        Map<String, Object> jsonParams = new ArrayMap<>();
        try {


            jsonParams.put("Keyword", "");
            jsonParams.put("SelectedFor", "");
            jsonParams.put("SelectedFaith", "");
            jsonParams.put("SelectedGender", "");
            jsonParams.put("SelectedIssues", "");
            jsonParams.put("SelectedLanguage", "");
            jsonParams.put("SelectedInsurance", "");
            jsonParams.put("MaxDistance", "");
            jsonParams.put("ZipCode", "");
            jsonParams.put("PageNo", page);
            jsonParams.put("PageSize", 25);

        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(jsonParams)).toString());
        Utility.showProgress(activity);
        Call<SearchData> callLogin = apiService.search(body);
        callLogin.clone().enqueue(new Callback<SearchData>() {
            @Override
            public void onResponse(@NonNull Call<SearchData> call, @NonNull Response<SearchData> response) {
                Utility.dismissProgress();
                if (response.code() == 200) {
                    try {
                        Log.e("response", response.body() + "");
                        SearchData searchData = response.body();

                        assert searchData != null;
                        if (searchData.getResult() != null && searchData.getResult().size() > 0) {
                            if (adapter == null) {
                                adapter = new BestAdapter(activity, searchData.getResult());
                                recyclerView.setAdapter(adapter);
                            } else {
                                adapter.updateList(searchData.getResult());
                            }
                        } else {
                            endlessRecyclerOnScrollListener.reachedMax(true);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SearchData> call, Throwable t) {
                Utility.dismissProgress();
                t.printStackTrace();
            }
        });


    }

}