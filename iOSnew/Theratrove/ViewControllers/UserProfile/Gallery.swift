//
//  Gallery.swift
//  Theratrove
//
//  Created by Dhruv Patel on 18/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class Gallery: UIViewController {
    var dictData = NSDictionary()
     @IBOutlet var clwGallery: UICollectionView!
    var arrGallery = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.arrGallery.addObjects(from: self.dictData.object(forKey: "TherapyGalleryList") as! [Any])
        self.clwGallery.delegate = self
        self.clwGallery.dataSource = self
        self.clwGallery.reloadData()
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension Gallery : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrGallery.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
        let ImageData = self.arrGallery.object(at: indexPath.row) as! NSDictionary
        let value = ImageData.value(forKey: "GalleryImageURL") as! String
        cell.imgContent.moa.url = value
       
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: (self.clwGallery.frame.size.width / 2) - 10, height: 169)
        return size
        
    }
}
