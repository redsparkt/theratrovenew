package com.theratrove.activityList;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theratrove.R;
import com.theratrove.activityList.Adapter.ProfilePagerAdapter;
import com.theratrove.activityList.Adapter.SectionsPagerAdapter;
import com.theratrove.activityList.Fragment.DescrptionFragment;
import com.theratrove.activityList.Fragment.FocusFragment;
import com.theratrove.activityList.Fragment.GalleryFragment;
import com.theratrove.activityList.Fragment.VideoFragment;
import com.theratrove.activityList.appUtils.AppSharedPreferences;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.model.Profile;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import java.util.ArrayList;

import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theratrove.activityList.appUtils.CommonValidation.checkInternetConnection;
import static com.theratrove.activityList.appUtils.Utility.dismissProgress;
import static com.theratrove.activityList.appUtils.Utility.showProgress;

public class ProfileActivity extends AppCompatActivity implements Callback<Profile> {
    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.imgUserHome)
    ImageView imgUserHome;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtThera)
    TextView txtThera;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.txtContact)
    TextView txtContact;
    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.container)
    ViewPager container;
    private ApiInterface apiService;
    private HttpUrl getProfileCallUrl;
    private AppSharedPreferences appSharedPreferences;
    private Profile profile;
    private ArrayList<Fragment> fragmentList = new ArrayList<>();
    private ArrayList<String> fragmentTitleList = new ArrayList<>();
    private SectionsPagerAdapter viewPagerAdapter;
    private final String TAG = ProfileActivity.class.getSimpleName();
    int id = -1;
    RequestOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        appSharedPreferences = AppSharedPreferences.getSharePreference(this);

        if (getIntent().hasExtra("id")) {
            id = getIntent().getIntExtra("id", 79);
        }

        initNetwork(id);
    }

    private void initNetwork(int id) {

        options = new RequestOptions()
//                .centerCrop()
                .placeholder(R.drawable.dummyuserimage)
                .error(R.drawable.dummyuserimage);

        apiService = ApiClient.getClient().create(ApiInterface.class);
        callApiGetProfile(id);
    }

    private void callApiGetProfile(int id) {
        if (checkInternetConnection(this)) {
            Call<Profile> callLogin = apiService.getProfileData(ApiClient.BASE_URL + "Get/" + id);
            getProfileCallUrl = callLogin.request().url();
            callLogin.clone().enqueue(this);
            showProgress(this);
        }
    }

    @OnClick({R.id.txtContact, R.id.imgBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtContact:
                Intent intent = new Intent(ProfileActivity.this, ContactTheraphistActivity.class);
                intent.putExtra("is_hide", true);
                intent.putExtra("id", id);

                startActivity(intent);
                break;
            case R.id.imgBack:
                finish();
                break;
        }
    }

    @Override
    public void onResponse(@NonNull Call<Profile> call, @NonNull Response<Profile> response) {
        dismissProgress();
        if (response.code() == 200) {
            if (call.request().url().equals(getProfileCallUrl)) {
                try {
                    profile = response.body();
                    Log.d(TAG, "onResponse: " + response.toString());
                    if (profile != null) {
                        txtName.setText(profile.getUserName());
                        txtThera.setText(profile.getTitle());
                        String address = profile.getAddress1() + " " + profile.getAddress2() + " " + profile.getCity() + " " + profile.getState();
                        txtAddress.setText(address);
                        if (profile.getProfilePic() != null && profile.getProfilePic().length() > 0) {
                            Glide.with(this).load(profile.getProfilePic()).apply(options).into(imgUserHome);
                            AppSharedPreferences.getSharePreference(this).setProfilePic(profile.getProfilePic());
                        }

                        if ((profile.getAboutMe() != null && profile.getAboutMe().length() > 0)
                                || (profile.getAvgCost() != null)
                                || (profile.getTherapistRating() != null)
                                || (profile.getYearsInService() != null && profile.getYearsInService().length() > 0)
                                || (profile.getEducation() != null && profile.getEducation().length() > 0)
                                || (profile.getAcceptInsurance() != null)
                                || (profile.getLicense() != null && profile.getLicense().length() > 0)
                                || (profile.getTherapistTypesOfTherapyFilter()!= null && profile.getTherapistTypesOfTherapyFilter().toString().length() > 0)
                                || (profile.getIssuesFilter() != null && profile.getIssuesFilter().toString().length() > 0)) {

                            DescrptionFragment descrptionFragment = DescrptionFragment.newInstance(profile.getAboutMe(),
                                    profile.getTherapistRating() + "", profile.getYearsInService(), profile.getEducation(),
                                    profile.getAvgCost() + "", profile.getAcceptInsurance(), profile.getLicense(), profile.getTherapistTypesOfTherapyFilter().toString(),
                                    profile.getIssuesFilter(), profile.getUserId(),false);
                            fragmentList.add(descrptionFragment);
                            fragmentTitleList.add("DESCRIPTION");
                        }
                        if ((profile.getFaithFilter() != null && profile.getFaithFilter().length() > 0)
                                || (profile.getForFilter() != null && profile.getForFilter().length() > 0)
                                || (profile.getGenderPreferenceFilter() != null && profile.getGenderPreferenceFilter().length() > 0)
                                || (profile.getLanguageFilter() != null && profile.getLanguageFilter().length() > 0)
                                || (profile.getInsuranceZipCodeFilter() != null && profile.getInsuranceZipCodeFilter().length() > 0)
                                || (profile.getIssuesFilter() != null && profile.getIssuesFilter().length() > 0)) {
                            FocusFragment focusFragment =
                                    FocusFragment.newInstance(profile.getFaithFilter(), profile.getForFilter(),
                                            profile.getGenderPreferenceFilter(), profile.getLanguageFilter(), profile.getInsuranceZipCodeFilter(), profile.getIssuesFilter());
                            fragmentList.add(focusFragment);
                            fragmentTitleList.add("FOCUS");
                        }
                        if (profile.getTherapyGalleryList() != null && profile.getTherapyGalleryList().size() > 0) {
                            GalleryFragment galleryFragment = GalleryFragment.newInstance(profile.getTherapyGalleryList());
                            fragmentList.add(galleryFragment);
                            fragmentTitleList.add("GALLERY");
                        }
                        if (profile.getVideoUrl() != null && profile.getVideoUrl().length() > 0 && !profile.getVideoUrl().equalsIgnoreCase("-")) {
                            VideoFragment videoFragment = VideoFragment.newInstance(profile.getVideoUrl());
                            fragmentList.add(videoFragment);
                            fragmentTitleList.add("VIDEO");
                        }

                        viewPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), fragmentList,
                                fragmentTitleList);
                        container.setOffscreenPageLimit(3);
                        container.setAdapter(viewPagerAdapter);

                        tabs.setupWithViewPager(container);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<Profile> call, @NonNull Throwable t) {
        dismissProgress();
        t.printStackTrace();
    }
}
