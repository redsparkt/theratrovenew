package com.theratrove.activityList.model;

public class GenderModel {

    public String name = "";
   public String genderId = "";

    public String getGenderId() {
        return genderId;
    }

    public void setGenderId(String genderId) {
        this.genderId = genderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
