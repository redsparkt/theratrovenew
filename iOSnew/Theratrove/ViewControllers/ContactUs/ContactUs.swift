//
//  ContactUs.swift
//  Theratrove
//
//  Created by Dhruv Patel on 19/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class ContactUs: UIViewController {
    private let placeholder = "Enter your message"
    @IBOutlet var tvwMessage: UITextView!{
        didSet {
            tvwMessage.textColor = UIColor.black
            tvwMessage.text = placeholder
            tvwMessage.layer.cornerRadius = 5
            tvwMessage.layer.masksToBounds = true
        }
    }
    @IBOutlet var txtSubject: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var imgBackground: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    var strTitle = String()
    var userid = String()
    @IBOutlet var btnSubmit: UIButton!
    
    @IBOutlet var txtSubject_height: NSLayoutConstraint!
    @IBOutlet var lblGetInTouch: UILabel!
    @IBOutlet var btnBackClick: UIButton!
    @IBOutlet var uvwtopview_height: NSLayoutConstraint!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.lblTitle.text = self.strTitle
        
        if self.strTitle == "Contact Us"{
            self.lblGetInTouch.isHidden = false
            self.btnBackClick.setImage(UIImage(named: "list_icon"), for: .normal)
            self.txtSubject_height.constant = 45
            self.txtSubject.isHidden = false
        }
        else{
            self.lblGetInTouch.isHidden = true
            self.btnBackClick.setImage(UIImage(named: "profile_back"), for: .normal)
            self.txtSubject_height.constant = 0
            self.txtSubject.isHidden = true
        }
            
        if Device.DeviceType.IS_IPHONE_5_OR_LESS{
            self.imgBackground.image = UIImage(named: "background_5")
        }
        else if Device.DeviceType.IS_IPHONE_6{
            self.imgBackground.image = UIImage(named: "background_6")
        }
        else if Device.DeviceType.IS_IPHONE_6P{
            self.imgBackground.image = UIImage(named: "background_plus")
        }
        else{
            self.imgBackground.image = UIImage(named: "background_x")
            self.uvwtopview_height.constant = 80
        }
        self.txtName.CornerRadious(5)
        self.txtName.setLeftPaddingPoints(15)
        self.txtEmail.CornerRadious(5)
        self.txtEmail.setLeftPaddingPoints(15)
        self.txtSubject.CornerRadious(5)
        self.txtSubject.setLeftPaddingPoints(15)
        self.tvwMessage.delegate = self
        self.btnSubmit.layer.cornerRadius = 5
        self.btnSubmit.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }

    @IBAction func btnContactClick(_ sender: Any) {
        self.view.endEditing(true)
        self.ApiCall()
    }
    func ApiCall(){
         if self.strTitle == "Contact Us"
         {
            let name = self.txtName.text
            let email = self.txtEmail.text
            let subject = self.txtSubject.text
            let message = self.tvwMessage.text
            
            if (name?.isEmpty)!{
                Utils().showMessage("Enter valid name.")
                return()
            }
            if (subject?.isEmpty)!{
                Utils().showMessage("Enter valid subject.")
                return
            }
            let email1 = Validation().email(email!)
            if(email1[0] as? String == "false"){
                Utils().showMessage(email1[1] as! String)
                return
            }
            if (message?.isEmpty)!{
                Utils().showMessage("Enter valid name.")
                return
            }
            
            let parameter : [String : Any] = [
                "name":name!,
                "email":email!,
                "subject":subject!,
                "Message":message!
            ]
            Utils().ShowLoader()
            WebService.postMethod(params: parameter as [String : AnyObject], apikey: Constants.Contact, completion: { (Json) in
                let Data : NSDictionary = Json as! NSDictionary
                let Message = Data.value(forKey: "Message") as! String
                Utils().showMessage(Message)
            }, failure: {(error) in
                
            })
         }
         else
         {
            let name = self.txtName.text
            let email = self.txtEmail.text
            let message = self.tvwMessage.text
            
            if (name?.isEmpty)!{
                Utils().showMessage("Enter valid name.")
                return
            }
            let email1 = Validation().email(email!)
            if(email1[0] as? String == "false"){
                Utils().showMessage(email1[1] as! String)
                return
            }
            if (message?.isEmpty)!{
                Utils().showMessage("Enter valid name.")
                return
            }
            
            let parameter : [String : Any] = [
                "username":name!,
                "emailAddress":email!,
                "therapistId":self.userid,
                "message":message!
            ]
            Utils().ShowLoader()
            WebService.postMethod(params: parameter as [String : AnyObject], apikey: Constants.SendContactEmail, completion: { (Json) in
                Utils().showMessage("Message sent successfully.")
            }, failure: {(error) in
                
            })
          }
        
    }
    
    @IBAction func btnListClick(_ sender: Any) {
        if self.strTitle == "Contact Us"{
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    }

extension ContactUs: UITextViewDelegate {
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        // Move cursor to beginning on first tap
        if textView.text == placeholder {
            textView.selectedRange = NSRange(location: 0, length: 0)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text == placeholder && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.black
            textView.selectedRange = NSRange(location: 0, length: 0)
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.textColor = UIColor.black
            textView.text = placeholder
        }
    }
    
    func textViewShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
