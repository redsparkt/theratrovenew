//
//  introduction.swift
//  Theratrove
//
//  Created by Dhruv Patel on 19/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class introduction: UIViewController {
   var arrImg = NSArray ()
    
    @IBOutlet var topview_height: NSLayoutConstraint!
    @IBOutlet var clwIntro: UICollectionView!
    @IBOutlet var pageController: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Device.DeviceType.IS_IPHONE_5_OR_LESS
        {
            self.arrImg = ["Mobile_Walkthrough_1_5.png",
                           "Walkthrough_2_5.png",
                           "Mobile_Walkthrough_3_5.png"]
        }
        else if Device.DeviceType.IS_IPHONE_6
        {
            self.arrImg = ["Mobile_Walkthrough_1_6.png",
                           "Walkthrough_2_6.png",
                           "Walkthrough_3_6.png"]
        }
        else if Device.DeviceType.IS_IPHONE_6P
        {
            self.arrImg = ["Mobile_Walkthrough_1_plus.png",
                           "Walkthrough_2_plus.png",
                           "Walkthrough_3_plus.png"]
        }
        else
        {
            self.arrImg = ["Mobile_Walkthrough_1_x.png",
                           "Walkthrough_2_x.png",
                           "Walkthrough_3_x.png"]
            
            self.topview_height.constant = 80
        }
        
        //self.clwIntro.register(UINib(nibName: "IntroCell", bundle: nil), forCellWithReuseIdentifier: "MyCell")
        self.clwIntro.delegate = self
        self.clwIntro.dataSource = self
        self.clwIntro.reloadData()
        self.pageController.numberOfPages = self.arrImg.count
        self.pageController.currentPage = 0

        // Do any additional setup after loading the view.
    }

    @IBAction func btnMenuClick(_ sender: Any) {
         sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSearchClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let SearchPage = storyboard.instantiateViewController(withIdentifier: "SearchPage") as? SearchPage
        self.navigationController?.pushViewController(SearchPage ?? UIViewController(), animated: true)
    }
    
    @IBAction func btnSkipclick(_ sender: Any) {
        Constants.USERDEFAULTS.set(1, forKey: "SkipIntro")
        Constants.appDelegate.SetupMenu()
    }
    
}
extension introduction: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! IntroCell
        cell.imgIntro.image = UIImage(named: self.arrImg.object(at: indexPath.row) as! String)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: self.clwIntro.frame.size.width, height: self.clwIntro.frame.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.pageController?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //
    //        let screenRect: CGRect = UIScreen.main.bounds
    //        let screenWidth: CGFloat = screenRect.size.width
    //        let cellWidth : CGFloat
    //        cellWidth = screenWidth / 3
    //
    //
    //        let ItemData = arrItemList.object(at: indexPath.row) as! NSDictionary
    //
    //        var LabelHeight : CGFloat
    //        LabelHeight = Utils().height(forText: ItemData.value(forKey: "name") as? String, font: UIFont.systemFont(ofSize: 13), withinWidth: cellWidth)
    //
    //        var size = CGSize()
    //        size = CGSize(width: CGFloat(cellWidth), height: self.collectionview.frame.size.height / 2)
    //
    //        return size
    //    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0.0
    //    }
    
}
