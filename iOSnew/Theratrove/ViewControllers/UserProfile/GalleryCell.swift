//
//  GalleryCell.swift
//  Theratrove
//
//  Created by Dhruv Patel on 27/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell {
    @IBOutlet var bthDelete: UIButton!
    @IBOutlet var imgContent: UIImageView!
}
