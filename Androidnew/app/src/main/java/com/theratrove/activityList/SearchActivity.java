package com.theratrove.activityList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.theratrove.R;
import com.theratrove.activityList.Adapter.FaithAdapter;
import com.theratrove.activityList.Adapter.ForAdapter;
import com.theratrove.activityList.Adapter.GenderAdapter;
import com.theratrove.activityList.Adapter.InsuranceAdapter;
import com.theratrove.activityList.Adapter.IssueAdapter;
import com.theratrove.activityList.Adapter.LanguageAdapter;
import com.theratrove.activityList.Adapter.LocationAdapter;
import com.theratrove.activityList.Adapter.ViewPagerAdapter;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.model.SearchData;
import com.theratrove.activityList.model.SearchFilterData;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import butterknife.internal.Utils;
import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity implements Callback<SearchFilterData> {

    ImageView imgeback;
    TextView txttitle, txtMatch;
    ApiInterface apiService;
    HttpUrl httpGetFilterData;
    RecyclerView recyclerview_faith, recyclerview_issue, recyclerview_issurance, recyclerview_language;
    Spinner spinner_for, spinner_location, spinner_gender;

    FaithAdapter faithAdapter;
    IssueAdapter issueAdapter;
    InsuranceAdapter insuranceAdapter;
    LanguageAdapter languageAdapter;
    ForAdapter forAdapter;
    LocationAdapter locationAdapter;
    GenderAdapter genderAdapter;
    String forId = "", genderId = "", locationId = "";

    EditText edtKeyword, edtZipcode;
    int page = 1, pageSize = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setVisibility(View.VISIBLE);
            imgeback = toolbar.findViewById(R.id.imgBack);
            txttitle = toolbar.findViewById(R.id.txttitle);
            txttitle.setText(getResources().getString(R.string.search));
        }
        imgeback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtMatch = findViewById(R.id.txtMatch);
        recyclerview_faith = findViewById(R.id.recyclerview_faith);
        recyclerview_issue = findViewById(R.id.recyclerview_issue);
        recyclerview_issurance = findViewById(R.id.recyclerview_issurance);
        recyclerview_language = findViewById(R.id.recyclerview_language);
        spinner_for = findViewById(R.id.spinner_for);
        spinner_location = findViewById(R.id.spinner_location);
        spinner_gender = findViewById(R.id.spinner_gender);
        edtKeyword = findViewById(R.id.edtKeyword);
        edtZipcode = findViewById(R.id.edtZipcode);

        txtMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(SearchActivity.this, MatchActivity.class);
                intent.putExtra("Keyword", edtKeyword.getText().toString().trim());
                intent.putExtra("SelectedFor", forId);
                intent.putExtra("SelectedFaith", getFaithId(faithAdapter.getSelectFaith()).toString());
                intent.putExtra("SelectedGender", genderId);
                intent.putExtra("SelectedIssues", getIssueId(issueAdapter.getSelectIssue()).toString());
                intent.putExtra("SelectedInsurance", getIssuranceId(insuranceAdapter.getSelectInsuranceOption()).toString());
                intent.putExtra("SelectedLanguage", getLanguageId(languageAdapter.getSelectLanguageOption()).toString());
                intent.putExtra("MaxDistance", locationId);
                intent.putExtra("ZipCode", edtZipcode.getText().toString().trim());
                startActivity(intent);


            }
        });

        apiService = ApiClient.getClient().create(ApiInterface.class);
        if (CommonValidation.checkInternetConnection(this)) {
            callGetSearchFilterList();
        } else {
            CommonValidation.showToast(this, getString(R.string.no_internet));
        }

        spinner_for.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {


                try {
                    if (forAdapter != null) {
                        SearchFilterData.ForOption forOption = forAdapter.getItem(position);
                        if (forOption.getKey().equalsIgnoreCase("Select For")) {
                            forId = "";
                        } else {
                            if (forOption.getValue() != null) {
                                forId = forOption.getValue() + "";
                            }
                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {


                try {
                    if (genderAdapter != null) {
                        SearchFilterData.GenderOption genderOption = genderAdapter.getItem(position);

                        if (genderOption.getKey().equalsIgnoreCase("Select Gender")) {
                            genderId = "";
                        } else {
                            if (genderOption.getValue() != null) {
                                genderId = genderOption.getValue() + "";
                            }
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {


                try {
                    if (locationAdapter != null) {
                        SearchFilterData.LocationOption locationOption = locationAdapter.getItem(position);

                        if (locationOption.getKey().equalsIgnoreCase("Select Location")) {
                            locationId = "";
                        } else {
                            if (locationOption.getValue() != null) {
//                                String theDigits = Character.digit().retainFrom("abc12 3def");

                                locationId = extractDigits(locationOption.getKey());
//                                locationId = locationOption.getValue();
                            }
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void callGetSearchFilterList() {
        Call<SearchFilterData> callLogin = apiService.getSearchFilterData();
        httpGetFilterData = callLogin.request().url();
        callLogin.clone().enqueue(this);
        Utility.showProgress(this);
    }


    @Override
    public void onResponse(@NonNull Call<SearchFilterData> call, @NonNull Response<SearchFilterData> response) {
        Utility.dismissProgress();
        if (response.isSuccessful() && response.code() == 200) {
            try {

                SearchFilterData searchFilterData = response.body();

                if (searchFilterData.getLocationOptions() != null && searchFilterData.getLocationOptions().size() > 0) {
                    ArrayList<SearchFilterData.LocationOption> locationOptions = searchFilterData.getLocationOptions();
                    SearchFilterData.LocationOption option = new SearchFilterData.LocationOption();
                    option.setKey("Select Location");
                    locationOptions.add(0, option);
                    locationAdapter = new LocationAdapter(this, locationOptions);
                    spinner_location.setAdapter(locationAdapter);
                }

                if (searchFilterData.getFaithOpitions() != null && searchFilterData.getFaithOpitions().size() > 0) {
                    faithAdapter = new FaithAdapter(this, searchFilterData.getFaithOpitions());
                    recyclerview_faith.setAdapter(faithAdapter);
                }

                if (searchFilterData.getForOptions() != null && searchFilterData.getForOptions().size() > 0) {

                    ArrayList<SearchFilterData.ForOption> forOptions = searchFilterData.getForOptions();
                    SearchFilterData.ForOption option = new SearchFilterData.ForOption();
                    option.setKey("Select For");
                    forOptions.add(0, option);

                    forAdapter = new ForAdapter(this, forOptions);
                    spinner_for.setAdapter(forAdapter);
                }


                if (searchFilterData.getIssuesOptions() != null && searchFilterData.getIssuesOptions().size() > 0) {
                    issueAdapter = new IssueAdapter(this, searchFilterData.getIssuesOptions());
                    recyclerview_issue.setAdapter(issueAdapter);
                }

                if (searchFilterData.getInsuranceOptions() != null && searchFilterData.getInsuranceOptions().size() > 0) {


                    insuranceAdapter = new InsuranceAdapter(this, searchFilterData.getInsuranceOptions());
                    recyclerview_issurance.setAdapter(insuranceAdapter);
                }

                if (searchFilterData.getGenderOptions() != null && searchFilterData.getGenderOptions().size() > 0) {

                    ArrayList<SearchFilterData.GenderOption> genderOptions = searchFilterData.getGenderOptions();
                    SearchFilterData.GenderOption option = new SearchFilterData.GenderOption();
                    option.setKey("Select Gender");
                    genderOptions.add(0, option);

                    genderAdapter = new GenderAdapter(this, genderOptions);
                    spinner_gender.setAdapter(genderAdapter);
                }

                if (searchFilterData.getLanguageOptions() != null && searchFilterData.getLanguageOptions().size() > 0) {
                    languageAdapter = new LanguageAdapter(this, searchFilterData.getLanguageOptions());
                    recyclerview_language.setAdapter(languageAdapter);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                CommonValidation.showToast(this, response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<SearchFilterData> call, Throwable t) {
        Utility.dismissProgress();
        t.printStackTrace();
    }


    public ArrayList<String> getFaithId(ArrayList<SearchFilterData.FaithOpition> faithOpitions) {
        StringBuilder sb = new StringBuilder();

        ArrayList<String> faithlist = new ArrayList<String>();

//        for (SearchFilterData.FaithOpition a : faithOpitions) {
//            sb.append(a.getValue());
//            sb.append(",");
//        }

        for (int i = 0; i < faithOpitions.size(); i++) {
            sb.append(faithOpitions.get(i).getValue());
            if (i < faithOpitions.size() - 1) {
                sb.append(",");
            }
            faithlist.add(sb.toString());
        }

        return faithlist;

    }

    public ArrayList<String> getIssueId(ArrayList<SearchFilterData.IssuesOption> issuesOptions) {
        StringBuilder sb = new StringBuilder();

//        for (SearchFilterData.IssuesOption a : issuesOptions) {
//            sb.append(a.getValue());
//            sb.append(",");
//        }

        ArrayList<String> issuelist = new ArrayList<String>();

        for (int i = 0; i < issuesOptions.size(); i++) {
            sb.append(issuesOptions.get(i).getValue());
            if (i < issuesOptions.size() - 1) {
                sb.append(",");
            }
            issuelist.add(sb.toString());
        }
        return issuelist;


    }

    public ArrayList<String> getIssuranceId(ArrayList<SearchFilterData.InsuranceOption> insuranceOption) {
        StringBuilder sb = new StringBuilder();

//        for (SearchFilterData.InsuranceOption a : issuesOptions) {
//            sb.append(a.getValue());
//            sb.append(",");
//        }

        ArrayList<String> insurancelist = new ArrayList<String>();

        for (int i = 0; i < insuranceOption.size(); i++) {
            sb.append(insuranceOption.get(i).getValue());
            if (i < insuranceOption.size() - 1) {
                sb.append(",");
            }
            insurancelist.add(sb.toString());
        }

        return insurancelist;
    }

    public ArrayList<String> getLanguageId(ArrayList<SearchFilterData.LanguageOption> languageOption) {
        StringBuilder sb = new StringBuilder();

//        for (SearchFilterData.LanguageOption a : issuesOptions) {
//            sb.append(a.getValue());
//                sb.append(",");
//        }

        ArrayList<String> Langlist = new ArrayList<String>();

        for (int i = 0; i < languageOption.size(); i++) {
            sb.append(languageOption.get(i).getValue());
            if (i < languageOption.size() - 1) {
                sb.append(",");
            }
            Langlist.add(sb.toString());
        }

        return Langlist;
    }

    public String extractDigits(String src) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < src.length(); i++) {
            char c = src.charAt(i);
            if (Character.isDigit(c)) {
                builder.append(c);
            }
        }
        return builder.toString();
    }

//    ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), searchData, most);
//                        viewPager.setAdapter(viewPagerAdapter);
//                        tabs.setupWithViewPager(viewPager);
}
