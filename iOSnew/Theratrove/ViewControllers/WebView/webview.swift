//
//  webview.swift
//  Theratrove
//
//  Created by Dhruv Patel on 18/12/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit
import WebKit

class webview: UIViewController {
    @IBOutlet var lblPageTitle: UILabel!
    @IBOutlet var uvwtopview_height: NSLayoutConstraint!
    @IBOutlet var wbview: WKWebView!
    var Title = NSString()
    var Pagemenu = NSString()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblPageTitle.text = self.Title as String
        
        if self.Title == "Terms and Conditions"{
            self.Pagemenu = "TermsAndConditions"
        }
        if self.Title == "About Us"{
            self.Pagemenu = "About"
        }
        if self.Title == "Privacy Policy"{
            self.Pagemenu = "PrivacyPolicy"
        }
        if self.Title == "How It Works"{
            self.Pagemenu = "HowItWork"
        }
        
        if Device.DeviceType.IS_IPHON_X{
            self.uvwtopview_height.constant = 80
        }
        
//        let preferences = WKPreferences()
//        preferences.javaScriptEnabled = true
//        let configuration = WKWebViewConfiguration()
//        configuration.preferences = preferences
//        self.wbview = WKWebView(frame: view.bounds, configuration: configuration)
        
        self.callapi()
    }
    
    func callapi(){
        let parameter : [String : Any] = [
            "pageName" : self.Pagemenu
        ]
        
        WebService.GetMethod(params: parameter as [String : AnyObject], apikey: Constants.GetPageContent, completion: { (Json) in
              let descUser  : NSDictionary = Json as! NSDictionary
            
            let pagecontent = descUser["pageContent"] as? String
            let content = "<html><body><p><font size=15>" + pagecontent! + "</font></p></body></html>"
            
           
            self.wbview.loadHTMLString(content, baseURL: nil)
         
            
        }, failure: {(error) in
            
        })
    }

    @IBAction func btnBackClick(_ sender: Any) {
        //self.navigationController?.popViewController(animated: true)
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
