package com.theratrove.activityList.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestFutureTarget;
import com.bumptech.glide.request.RequestOptions;
import com.theratrove.R;
import com.theratrove.activityList.ProfileActivity;
import com.theratrove.activityList.model.Profile;
import com.theratrove.activityList.model.SearchData;

import java.util.ArrayList;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {

    private ArrayList<Profile.TherapyGalleryList> bestList;
    Activity activity;
    RequestOptions options;

    class MyViewHolder extends RecyclerView.ViewHolder {


        ImageView image;


        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);

        }
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }


    public GalleryAdapter(Activity activity, ArrayList<Profile.TherapyGalleryList> bestList) {
        this.bestList = bestList;
        this.activity = activity;
        options = new RequestOptions()
//                .centerCrop()
                .placeholder(R.drawable.dummyuserimage)
                .error(R.drawable.dummyuserimage);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_gallry, parent, false);
//        int width = parent.getMeasuredWidth() / 2;
//        view.setMinimumWidth(width);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (bestList.get(position).getGalleryImage() != null && bestList.get(position).getGalleryImage().length() > 0) {
            Glide.with(activity).load(bestList.get(position).getGalleryImage()).apply(options).into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        return bestList.size();
    }
}