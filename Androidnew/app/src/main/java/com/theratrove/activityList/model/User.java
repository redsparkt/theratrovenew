package com.theratrove.activityList.model;

import org.json.JSONObject;

/**
 * Created by Redspark on 1/22/2018.
 */

public class User {
    public String userId;
    public String name;
    public String email;
    public String username;
    public String contact_no;
    public String address;
    public String gender;
    public String profile_pic;
    public String birth_date;
    public String latitude;
    public String longitude;


    public User(JSONObject data) {
        try {
            userId = data.optString("ID");
            name = data.optString("name");
            email = data.optString("email");
            username = data.optString("username");

            contact_no = data.optString("contact_no");
            address = data.optString("address");
            gender = data.optString("gender");

            profile_pic = data.optString("profile_pic");
            birth_date = data.optString("birth_date");

            latitude = data.optString("latitude");
            longitude = data.optString("longitude");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return userId;
    }

    public void setId(String userId) {
        this.userId = userId;
    }


}
