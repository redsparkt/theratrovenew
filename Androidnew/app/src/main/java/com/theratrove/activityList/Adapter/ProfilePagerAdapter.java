package com.theratrove.activityList.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.theratrove.activityList.Fragment.DescrptionFragment;

public class ProfilePagerAdapter extends FragmentPagerAdapter {

    public ProfilePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
//        if (position == 0)
//        {
        fragment = new DescrptionFragment();
//        }
//        else if (position == 1)
//        {
//            fragment = new FragmentB();
//        }
//        else if (position == 2)
//        {
//            fragment = new FragmentC();
//        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "DESCRIPTION ";
        } else if (position == 1) {
            title = "FOCUS";
        } else if (position == 2) {
            title = "GALLERY";
        } else if (position == 3) {
            title = "VIDEO";
        }
        return title;
    }
}
