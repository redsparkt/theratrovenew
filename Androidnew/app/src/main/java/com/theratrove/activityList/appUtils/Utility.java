package com.theratrove.activityList.appUtils;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

/**
 * Created by Redspark on 9/17/2018.
 */

public class Utility {

    public static void collapseWithDuration(final View v, int duration)
    {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override protected void applyTransformation(float interpolatedTime, Transformation t)
            {
                if (interpolatedTime == 1)
                {
                    v.setVisibility(View.GONE);
                } else
                {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override public boolean willChangeBounds()
            {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration(duration);
        v.startAnimation(a);
    }

    public static void expandWithDuration(final View v, int duration)
    {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);

        Animation a = new Animation()
        {
            @Override protected void applyTransformation(float interpolatedTime, Transformation t)
            {
                v.getLayoutParams().height = interpolatedTime == 1 ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                Log.d("DEMO", "applyTransformation: view height" + v.getLayoutParams().height);
                Log.d("DEMO", "applyTransformation: interpolated time" + interpolatedTime);
                v.requestLayout();
            }

            @Override public boolean willChangeBounds()
            {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration(duration);
        v.startAnimation(a);
    }
    public static void showProgress(Context context) {
        try {
            if (Constant.custDailog != null && Constant.custDailog.isShowing())
                Constant.custDailog.dismiss();

            if (Constant.custDailog == null)
                Constant.custDailog = new CustomDialog(context);
            Constant.custDailog.setCancelable(false);
            Constant.custDailog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dismissProgress() {
        if (Constant.custDailog != null && Constant.custDailog.isShowing())
            Constant.custDailog.dismiss();
        Constant.custDailog = null;
    }

}
