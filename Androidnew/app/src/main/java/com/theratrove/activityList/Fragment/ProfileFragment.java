package com.theratrove.activityList.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theratrove.R;
import com.theratrove.activityList.Adapter.SectionsPagerAdapter;
import com.theratrove.activityList.EditProfileActivity;
import com.theratrove.activityList.HomeActivity;
import com.theratrove.activityList.SignUpActivity;
import com.theratrove.activityList.appUtils.AppSharedPreferences;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.model.Profile;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import java.util.ArrayList;

import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theratrove.activityList.HomeActivity.drawerLayout;


public class ProfileFragment extends Fragment implements Callback<Profile> {

    ImageView imgBack, imgUserHome, imgEdit;
    TextView txtContact, txtName, txtThera, txtAddress;
    TabLayout tabLayout;
    ViewPager viewPager;
    Activity activity;
    ApiInterface apiService;
    HttpUrl httpUrlCallLogin;
    SectionsPagerAdapter viewPagerAdapter;
    ArrayList<Fragment> fragmentList = new ArrayList<>();
    ArrayList<String> fragmentTitleList = new ArrayList<>();
    CoordinatorLayout coornitorlayout;
    Profile profile;
    RequestOptions options;


    public static ProfileFragment newInstance() {
        ProfileFragment myFragment = new ProfileFragment();
        Bundle args = new Bundle();
        return myFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        apiService = ApiClient.getClient().create(ApiInterface.class);

        txtContact = view.findViewById(R.id.txtContact);
        txtName = view.findViewById(R.id.txtName);
        txtThera = view.findViewById(R.id.txtThera);
        txtAddress = view.findViewById(R.id.txtAddress);
        imgBack = view.findViewById(R.id.imgBack);
        imgUserHome = view.findViewById(R.id.imgUserHome);
        imgEdit = view.findViewById(R.id.imgEdit);
        imgBack.setImageResource(R.drawable.menu);
        txtContact.setVisibility(View.GONE);

        tabLayout = view.findViewById(R.id.tabs);
        viewPager = (ViewPager) view.findViewById(R.id.container);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    //drawer is open
                    drawerLayout.closeDrawer(Gravity.LEFT);

                } else {
                    drawerLayout.openDrawer(Gravity.LEFT);

                }
            }
        });


        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                intent.putExtra("FirstName", profile.getFirstName());
                intent.putExtra("lastName", profile.getLastName());
                intent.putExtra("title", profile.getTitle());
                intent.putExtra("YearsInService", profile.getYearsInService());
                intent.putExtra("Education", profile.getEducation());
                intent.putExtra("AcceptInsurance", profile.getAcceptInsurance());
                intent.putExtra("License", profile.getLicense());
                intent.putExtra("AboutMe", profile.getAboutMe());
                intent.putExtra("videoUrl", profile.getVideoUrl());
                intent.putExtra("phoneNumber", profile.getPhoneNumber());
                intent.putExtra("Location", profile.getLocation());
                intent.putExtra("AvgCost", profile.getAvgCost());
                intent.putExtra("Address1", profile.getAddress1());
                intent.putExtra("Address2", profile.getAddress2());
                intent.putExtra("city", profile.getCity());
                intent.putExtra("zipcode", profile.getZipCode());
                intent.putExtra("faithFilter", profile.getFaithFilter());
                intent.putExtra("forFilter", profile.getForFilter());
                intent.putExtra("GenderPreference", profile.getGenderPreferenceFilter());
                intent.putExtra("LocationFilter", profile.getLocationFilter());
                intent.putExtra("LanguageFilter", profile.getLanguageFilter());
                intent.putExtra("InsuranceFilter", profile.getInsuranceZipCodeFilter());
                intent.putExtra("IssuesFilter", profile.getIssuesFilter());
                intent.putExtra("gender", profile.getGender());
                intent.putExtra("galleryImage", profile.getTherapyGalleryList());
                intent.putExtra("typeofFilter", profile.getSelectedTherapistTypesOfTherapy());

                startActivity(intent);
            }
        });


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;

        options = new RequestOptions()
//                .centerCrop()
                .placeholder(R.drawable.dummyuserimage)
                .error(R.drawable.dummyuserimage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void onResume() {
        super.onResume();

//        if (AppSharedPreferences.getSharePreference(activity).getProfileic().length() > 0) {
//            Glide.with(activity).load(AppSharedPreferences.getSharePreference(activity).getProfileic()).apply(options).into(imgUserHome);
//
//        }

        if (CommonValidation.checkInternetConnection(activity)) {

            callGetProfiel(AppSharedPreferences.getSharePreference(activity).getUserID());
        } else {
            CommonValidation.showToast(activity, getString(R.string.no_internet));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResponse(@NonNull Call<Profile> call, @NonNull Response<Profile> response) {
        Utility.dismissProgress();
        if (response.code() == 200) {
            try {


                if (response.body() != null) {
                    fragmentList.clear();
                    fragmentTitleList.clear();
                    profile = response.body();
                    txtName.setText(profile.getUserName());
                    txtThera.setText(profile.getTitle());
                    if (profile.getAddress1() != null || profile.getAddress2() != null || profile.getCity() != null
                            || profile.getState() != null) {

                        txtAddress.setText(profile.getAddress1() + " " + profile.getAddress2() + " " + profile.getCity() + " " + profile.getState());
                    } else {
                        txtAddress.setVisibility(View.GONE);
                    }

                    if (profile.getProfilePic() != null && profile.getProfilePic().length() > 0) {
                        Glide.with(activity).load(profile.getProfilePic()).apply(options).into(imgUserHome);
                        AppSharedPreferences.getSharePreference(activity).setProfilePic(profile.getProfilePic());
                        ((HomeActivity) activity).setUserData();
                    }


                    if ((profile.getAboutMe() != null && profile.getAboutMe().length() > 0)
                            || (profile.getAvgCost() != null)
                            || (profile.getTherapistRating() != null)
                            || (profile.getYearsInService() != null && profile.getYearsInService().length() > 0)
                            || (profile.getEducation() != null && profile.getEducation().length() > 0)
                            || (profile.getAcceptInsurance() != null)
                            || (profile.getLicense() != null && profile.getLicense().length() > 0)
                            || (profile.getTherapistTypesOfTherapyFilter()!= null && profile.getTherapistTypesOfTherapyFilter().toString().length() > 0)
                            || (profile.getIssuesFilter() != null && profile.getIssuesFilter().toString().length() > 0)) {


                        DescrptionFragment descrptionFragment = DescrptionFragment.newInstance(profile.getAboutMe(),
                                profile.getTherapistRating() + "", profile.getYearsInService(), profile.getEducation(),
                                profile.getAvgCost() + "", profile.getAcceptInsurance(), profile.getLicense(),
                                profile.getTherapistTypesOfTherapyFilter().toString(), profile.getIssuesFilter(), profile.getUserId(), true);
                        fragmentList.add(descrptionFragment);
                        fragmentTitleList.add("DESCRIPTION");

                    }
                    if ((profile.getFaithFilter() != null && profile.getFaithFilter().length() > 0)
                            || (profile.getForFilter() != null && profile.getForFilter().length() > 0)
                            || (profile.getGenderPreferenceFilter() != null && profile.getGenderPreferenceFilter().length() > 0)
                            || (profile.getLanguageFilter() != null && profile.getLanguageFilter().length() > 0)
                            || (profile.getInsuranceZipCodeFilter() != null && profile.getInsuranceZipCodeFilter().length() > 0)
                            || (profile.getIssuesFilter() != null && profile.getIssuesFilter().length() > 0)) {
                        FocusFragment focusFragment = FocusFragment.newInstance(profile.getFaithFilter(), profile.getForFilter(),
                                profile.getGenderPreferenceFilter(), profile.getLanguageFilter(),
                                profile.getInsuranceZipCodeFilter(),
                                profile.getIssuesFilter());
                        fragmentList.add(focusFragment);
                        fragmentTitleList.add("FOCUS");

                    }
                    if (profile.getTherapyGalleryList() != null && profile.getTherapyGalleryList().size() > 0) {
                        GalleryFragment galleryFragment = GalleryFragment.newInstance(profile.getTherapyGalleryList());
                        fragmentList.add(galleryFragment);
                        fragmentTitleList.add("GALLERY");
                    }

                    if (profile.getVideoUrl() != null && profile.getVideoUrl().length() > 0 && !profile.getVideoUrl().equalsIgnoreCase("-")) {
                        VideoFragment videoFragment = VideoFragment.newInstance(profile.getVideoUrl());
                        fragmentList.add(videoFragment);
                        fragmentTitleList.add("VIDEO");
                    }


                    viewPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager(), fragmentList, fragmentTitleList);
//                    viewPager.setOffscreenPageLimit(3);
                    viewPager.setAdapter(null);
                    viewPager.setAdapter(viewPagerAdapter);

                    tabLayout.setupWithViewPager(viewPager);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public void onFailure(@NonNull Call<Profile> call, Throwable t) {
        Utility.dismissProgress();
    }

    public void callGetProfiel(String userID) {
        Call<Profile> callLogin = apiService.getProfileData(ApiClient.BASE_URL + "Get/" + userID);
        httpUrlCallLogin = callLogin.request().url();
        callLogin.clone().enqueue(this);
        Utility.showProgress(activity);
    }

}