package com.theratrove.activityList.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.theratrove.R;
import com.theratrove.activityList.model.SearchFilterData;

import java.util.ArrayList;

public class InsuranceAdapter extends RecyclerView.Adapter<InsuranceAdapter.ViewHolder> {

    private ArrayList<SearchFilterData.InsuranceOption> myItems = new ArrayList<>();
    Activity activity;
    private LayoutInflater inflater;

    public InsuranceAdapter(Activity activity, ArrayList<SearchFilterData.InsuranceOption> getItems) {
        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        myItems = getItems;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public AppCompatCheckBox cbSelect;

        public ViewHolder(View v) {
            super(v);
            cbSelect = (AppCompatCheckBox) v.findViewById(R.id.faith_checkbox);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.item_faith, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final SearchFilterData.InsuranceOption objIncome = myItems.get(position);
        holder.cbSelect.setText(objIncome.getKey());


        //if true, your checkbox will be selected, else unselected
        holder.cbSelect.setChecked(objIncome.getSelected());
        holder.cbSelect.setTag(holder.getAdapterPosition());
        holder.cbSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
                Integer pos = (Integer) holder.cbSelect.getTag();
                myItems.get(pos).setSelected(isChecked);

            }
        });

    }

    @Override
    public int getItemCount() {
        return myItems.size();
    }

    public ArrayList<SearchFilterData.InsuranceOption> getSelectInsuranceOption() {
        ArrayList<SearchFilterData.InsuranceOption> selectedIssue = new ArrayList<>();
        for (SearchFilterData.InsuranceOption c : myItems) {
            if (c.getSelected())
                selectedIssue.add(c);
        }

        return selectedIssue;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }
}