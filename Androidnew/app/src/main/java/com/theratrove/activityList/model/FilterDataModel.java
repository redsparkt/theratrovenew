package com.theratrove.activityList.model;

import java.io.Serializable;

public class FilterDataModel implements Serializable {

    public String keyword = "";
    public String SelectedFor = "";
    public String SelectedFaith;
    public String SelectedGender = "";
    public String SelectedIssues;
    public String SelectedInsurance;
    public String SelectedLanguage;
    public String MaxDistance = "";
    public String ZipCode = "";

}
