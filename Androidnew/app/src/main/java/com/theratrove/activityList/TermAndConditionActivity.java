package com.theratrove.activityList;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.theratrove.R;
import com.theratrove.activityList.appUtils.AppSharedPreferences;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.theratrove.activityList.appUtils.Constant.DEVICE_TYPE;

public class TermAndConditionActivity extends AppCompatActivity implements Callback<ResponseBody> {

    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txttitle)
    TextView txttitle;
    @BindView(R.id.toolbar)
    Toolbar toolbarTop;
    @BindView(R.id.myWebView)
    WebView myWebView;

    ApiInterface apiService;
    HttpUrl httpUrlCallLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term);
        ButterKnife.bind(this);

        setSupportActionBar(toolbarTop);
        txttitle.setText(getResources().getString(R.string.tem_condition));
        apiService = ApiClient.getClient().create(ApiInterface.class);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (CommonValidation.checkInternetConnection(TermAndConditionActivity.this)) {
            callTermAndCondition();
        } else {
            CommonValidation.showToast(TermAndConditionActivity.this, getString(R.string.no_internet));
        }


//        myWebView.loadData(html, "text/html", null);

    }

    public void callTermAndCondition() {
        Call<ResponseBody> callLogin = apiService.CmsPage("TermsAndConditions");
        httpUrlCallLogin = callLogin.request().url();
        callLogin.clone().enqueue(this);
        Utility.showProgress(TermAndConditionActivity.this);
    }

    @Override
    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
        Utility.dismissProgress();
        if (response.isSuccessful() && response.code() == 200) {
            try {

                JSONObject jsonObject = new JSONObject(response.body().string());
                String data=jsonObject.getString("pageContent");
                myWebView.loadData(data, "text/html", null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                CommonValidation.showToast(TermAndConditionActivity.this, response.errorBody().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
        Utility.dismissProgress();
        t.printStackTrace();
    }
}
