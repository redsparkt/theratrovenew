package com.theratrove.activityList.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;
import com.theratrove.activityList.Adapter.BestAdapter;
import com.theratrove.R;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.appUtils.EndlessRecyclerOnScrollListener;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.model.FilterDataModel;
import com.theratrove.activityList.model.SearchData;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BestFragment extends Fragment {

    RecyclerView recyclerView;
    FilterDataModel filterDataModel;
    BestAdapter adapter;
    Activity activity;

    ApiInterface apiService;
    HttpUrl httpUrlCallLogin;

    EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    int page = 1;

    public static BestFragment newInstance(FilterDataModel filterDataModel) {
        BestFragment myFragment = new BestFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constant.DATA, filterDataModel);
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_best, container, false);


        Bundle args = getArguments();
        if (args != null) {
            filterDataModel = (FilterDataModel) args.getSerializable(Constant.DATA);
        }


        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);


        apiService = ApiClient.getClient().create(ApiInterface.class);

        if (CommonValidation.checkInternetConnection(activity)) {
            callSearchServiceCall(filterDataModel.keyword, filterDataModel.SelectedFor, filterDataModel.SelectedFaith,
                    filterDataModel.SelectedGender, filterDataModel.SelectedIssues, filterDataModel.SelectedInsurance, filterDataModel.MaxDistance, filterDataModel.SelectedLanguage
                    , page, 25, filterDataModel.ZipCode);
        } else {
            CommonValidation.showToast(getActivity(), getString(R.string.no_internet));
        }

        endlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(activity, layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                Log.e("current_page==", current_page + "");
                page = current_page;
                callSearchServiceCall(filterDataModel.keyword, filterDataModel.SelectedFor, filterDataModel.SelectedFaith,
                        filterDataModel.SelectedGender, filterDataModel.SelectedIssues, filterDataModel.SelectedInsurance, filterDataModel.MaxDistance,
                        filterDataModel.SelectedLanguage
                        , current_page, 25, filterDataModel.ZipCode);

            }
        };

        recyclerView.addOnScrollListener(endlessRecyclerOnScrollListener);


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            this.activity = (Activity) context;
        }
    }

    public void callSearchServiceCall(final String keyword, final String SelectedFor, String SelectedFaith, String SelectedGender
            , String SelectedIssues, String SelectedInsurance, String MaxDistance, String language, int PageNo, int PageSize
            , final String zipcode) {


        Map<String, Object> jsonParams = new ArrayMap<>();

        try {
            jsonParams.put("Keyword", keyword);
            if (SelectedFor != null && SelectedFor.length() > 0) {
                jsonParams.put("SelectedFor", new JSONArray(Arrays.asList(SelectedFor)));
            }
            jsonParams.put("SelectedFaith", new JSONArray(SelectedFaith));

            if (SelectedGender != null && SelectedGender.length() > 0) {
                jsonParams.put("SelectedGender", new JSONArray(Arrays.asList(SelectedGender)));
            }
            jsonParams.put("SelectedIssues", new JSONArray(SelectedIssues));
            jsonParams.put("SelectedLanguage", new JSONArray(language));
            jsonParams.put("SelectedInsurance", new JSONArray(SelectedInsurance));
            jsonParams.put("MaxDistance", MaxDistance);
            jsonParams.put("ZipCode", zipcode);
            jsonParams.put("PageNo", PageNo);
            jsonParams.put("PageSize", PageSize);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (new JSONObject(jsonParams)).toString());
            Call<SearchData> call1 = apiService.search(body);
            Utility.showProgress(activity);
            HttpUrl httpUrl = call1.request().url();
            call1.enqueue(new Callback<SearchData>() {
                @Override
                public void onResponse(@NonNull Call<SearchData> call, @NonNull Response<SearchData> response) {
                    Utility.dismissProgress();
                    if (response.code() == 200) {
                        try {
                            assert response.body() != null;
//                        Log.e("response", response.body().string() + "");
                            SearchData searchData = response.body();
                            if (searchData.getResult() != null && searchData.getResult().size() > 0) {
                                if (adapter == null) {
                                    adapter = new BestAdapter(activity, searchData.getResult());
                                    recyclerView.setAdapter(adapter);
                                } else {
                                    adapter.updateList(searchData.getResult());
                                }
                            } else {
                                endlessRecyclerOnScrollListener.reachedMax(true);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SearchData> call, Throwable t) {
                    Utility.dismissProgress();
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


//        try {
//            if (keyword.trim().length() > 0) {
//                jsonObject.put("Keyword", keyword);
//            }
//            if (SelectedFor.trim().length() > 0) {
//                jsonObject.put("SelectedFor", SelectedFor);
//            }
//            if (SelectedFaith.trim().length() > 0) {
//                jsonObject.put("SelectedFaith", new JSONArray(SelectedFaith));
//            }
//            if (SelectedGender.trim().length() > 0) {
//                jsonObject.put("SelectedGender", SelectedGender);
//            }
//            if (SelectedIssues.trim().length() > 0) {
//                jsonObject.put("SelectedIssues", new JSONArray(SelectedIssues));
//            }
//            if (SelectedInsurance.trim().length() > 0) {
//                jsonObject.put("SelectedInsurance", new JSONArray(SelectedInsurance));
//            }
//            if (language.trim().trim().length() > 0) {
//                jsonObject.put("SelectedLanguage", new JSONArray(language));
//            }
//            if (MaxDistance.trim().length() > 0) {
//                jsonObject.put("MaxDistance", MaxDistance);
//            }
//            if (zipcode.trim().length() > 0) {
//                jsonObject.put("ZipCode", zipcode);
//            }
//            jsonObject.put("PageNo", PageNo);
//            jsonObject.put("PageSize", PageSize);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }

}