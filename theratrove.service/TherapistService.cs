﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace theratrove.service
{
    public class TherapistService
    {
        /// <summary>
        /// Get Therapist By Id
        /// </summary>
        /// <param name="Id"> Therapist Id</param>
        /// <returns> Therapist Object </returns>
        public List<theratrove.data.Therapist> GetbyId(int Id)
        {
            using (var context = new theratrove.data.TheratroveDB())
            {
               return context.Therapists.Where(th => th.Id == Id).ToList();
            }
        }
    }
}
