package com.theratrove.activityList.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SearchData implements Serializable {


    @SerializedName("Result")
    @Expose
    private ArrayList<Result> result = null;

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }

    public class Result implements Serializable{

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("Email")
        @Expose
        private String email;
        @SerializedName("FirstName")
        @Expose
        private String firstName;
        @SerializedName("LastName")
        @Expose
        private String lastName;
        @SerializedName("Title")
        @Expose
        private String title;
        @SerializedName("YearsInService")
        @Expose
        private String yearsInService;
        @SerializedName("Education")
        @Expose
        private String education;
        @SerializedName("AcceptInsurance")
        @Expose
        private Boolean acceptInsurance;
        @SerializedName("License")
        @Expose
        private String license;
        @SerializedName("AboutMe")
        @Expose
        private String aboutMe;
        @SerializedName("VideoUrl")
        @Expose
        private String videoUrl;
        @SerializedName("PhoneNumber")
        @Expose
        private String phoneNumber;
        @SerializedName("Location")
        @Expose
        private Object location;
        @SerializedName("AvgCost")
        @Expose
        private Integer avgCost;
        @SerializedName("Address1")
        @Expose
        private Object address1;
        @SerializedName("Address2")
        @Expose
        private Object address2;
        @SerializedName("City")
        @Expose
        private String city;
        @SerializedName("State")
        @Expose
        private String state;
        @SerializedName("ZipCode")
        @Expose
        private String zipCode;
        @SerializedName("Gender")
        @Expose
        private String gender;
        @SerializedName("TherapistTypesOfTherapyFilter")
        @Expose
        private Object therapistTypesOfTherapyFilter;
        @SerializedName("TherapistRating")
        @Expose
        private float therapistRating;
        @SerializedName("TherapistRatingCount")
        @Expose
        private Integer therapistRatingCount;
        @SerializedName("TotalRecords")
        @Expose
        private Integer totalRecords;
        @SerializedName("ProfilePicURL")
        @Expose
        private String ProfilePicURL;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getYearsInService() {
            return yearsInService;
        }

        public void setYearsInService(String yearsInService) {
            this.yearsInService = yearsInService;
        }

        public String getEducation() {
            return education;
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public Boolean getAcceptInsurance() {
            return acceptInsurance;
        }

        public void setAcceptInsurance(Boolean acceptInsurance) {
            this.acceptInsurance = acceptInsurance;
        }

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getAboutMe() {
            return aboutMe;
        }

        public void setAboutMe(String aboutMe) {
            this.aboutMe = aboutMe;
        }

        public String getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public Object getLocation() {
            return location;
        }

        public void setLocation(Object location) {
            this.location = location;
        }

        public Integer getAvgCost() {
            return avgCost;
        }

        public void setAvgCost(Integer avgCost) {
            this.avgCost = avgCost;
        }

        public Object getAddress1() {
            return address1;
        }

        public void setAddress1(Object address1) {
            this.address1 = address1;
        }

        public Object getAddress2() {
            return address2;
        }

        public void setAddress2(Object address2) {
            this.address2 = address2;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getZipCode() {
            return zipCode;
        }

        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public Object getTherapistTypesOfTherapyFilter() {
            return therapistTypesOfTherapyFilter;
        }

        public void setTherapistTypesOfTherapyFilter(Object therapistTypesOfTherapyFilter) {
            this.therapistTypesOfTherapyFilter = therapistTypesOfTherapyFilter;
        }

        public float getTherapistRating() {
            return therapistRating;
        }

        public void setTherapistRating(float therapistRating) {
            this.therapistRating = therapistRating;
        }

        public Integer getTherapistRatingCount() {
            return therapistRatingCount;
        }

        public void setTherapistRatingCount(Integer therapistRatingCount) {
            this.therapistRatingCount = therapistRatingCount;
        }

        public Integer getTotalRecords() {
            return totalRecords;
        }

        public void setTotalRecords(Integer totalRecords) {
            this.totalRecords = totalRecords;
        }


        public String getProfilePicURL() {
            return ProfilePicURL;
        }

        public void setProfilePicURL(String profilePicURL) {
            ProfilePicURL = profilePicURL;
        }
    }


}