﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using theratrove.data;

namespace theratrove.Models
{
    public class UserModel
    {
        public UserModel()
        {
            this.SelectedFaith = new List<CheckboxModel>();
            this.SelectedFor = new List<CheckboxModel>();
            this.SelectedIssues = new List<CheckboxModel>();
            this.SelectedInsurance = new List<CheckboxModel>();
            this.SelectedGenderPreference = new List<CheckboxModel>();
            this.SelectedLanguage = new List<CheckboxModel>();
            this.SelectedLocation = new List<CheckboxModel>();
            this.TherapyGalleryList = new List<TherapyGallery>();
            this.SelectedTherapistTypesOfTherapy = new List<CheckboxModel>();
        }

        public Therapist Therapist { get; set; }
        public List<User> UserList { get; set; }
        public List<Therapist> TherapistList { get; set; }
        public List<SearchFilterValue> SearchFilterValueList { get; set; }
        public List<CheckboxModel> SelectedFaith { get; set; }
        public List<CheckboxModel> SelectedFor { get; set; }
        public List<CheckboxModel> SelectedIssues { get; set; }
        public List<CheckboxModel> SelectedInsurance { get; set; }
        public List<CheckboxModel> SelectedGenderPreference { get; set; }
        public List<CheckboxModel> SelectedLanguage { get; set; }
        public List<CheckboxModel> SelectedLocation { get; set; }
        public List<TherapyGallery> TherapyGalleryList { get; set; }
        public List<CheckboxModel> SelectedTherapistTypesOfTherapy { get; set; }

        public int LikeCount { get; set; }
        public int? UserId { get; set; }
        public string ProfilePic { get; set; }
        [Required, StringLength(50)]
        public string UserName { get; set; }

        [Required, StringLength(200)]
        public string Email { get; set; }

        [Required, StringLength(200)]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d](?=.*?[#?!@$%^&*-]).{6,12}$", ErrorMessage = "Password is in invalid format. Password format requirements: Min: 6 characters, Max: 12 characters and at least 1 upper case, 1 lower case, 1 number, 1 special character; Confirm password is invalid format.")]
        public string Password { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? LastLogin { get; set; }

        public int? FailedLoginAttempts { get; set; }

        public bool? Locked { get; set; }



        [StringLength(50)]
        public string UserType { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string Title { get; set; }

        [StringLength(20)]
        public string YearsInService { get; set; }

        [StringLength(100)]
        public string Education { get; set; }

        public bool AcceptInsurance { get; set; }

        [StringLength(50)]
        public string License { get; set; }

        public string AboutMe { get; set; }

        [StringLength(200)]
        [Url(ErrorMessage = "Please enter a valid url")]
        public string VideoUrl { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        public int? AvgCost { get; set; }

        [StringLength(200)]
        public string Address1 { get; set; }

        [StringLength(200)]
        public string Address2 { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(8)]
        public string ZipCode { get; set; }

        public byte? Gender { get; set; }
        public bool? Active { get; set; }

        public string FaithFilter { get; set; }
        public string ForFilter { get; set; }
        public string IssuesFilter { get; set; }
        public string InsuranceZipCodeFilter { get; set; }
        public string GenderPreferenceFilter { get; set; }
        public string LanguageFilter { get; set; }
        public string LocationFilter { get; set; }
        public string TherapistTypesOfTherapyFilter { get; set; }
        public decimal TherapistRating { get; set; }
        public int TherapistRatingCount { get; set; }

    }
    public class CheckboxModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Checked { get; set; }
        public int itemFor { get; set; }
    }

    public class SendContactEmailVM
    {
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string Message { get; set; }
        public int TherapistId { get; set; }
    }

    public class PictureVM
    {
        public byte[] PictureFile { get; set; }
        public string PictureFileName { get; set; }
        public int PictureId { get; set; }
        public int TherapistId { get; set; }
    }
    public class ContactVM
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }

    public class LoginSignUpVM
    {
        public SignUpVM SignUpVM { get; set; }
        public LoginVM LoginVM { get; set; }
    }

    public class SignUpVM
    {
        public int? UserId{ get; set; }
        [Required(ErrorMessage="User Name is required.")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Email is required.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d](?=.*?[#?!@$%^&*-]).{6,12}$", ErrorMessage = "Password is in invalid format. Password format requirements: Min: 6 characters, Max: 12 characters and at least 1 upper case, 1 lower case, 1 number, 1 special character; Confirm password is invalid format.")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Confirm Password is required.") ]
        public string ConfirmPassword { get; set; }
        public bool? Active { get; set; }

    }

    public class ChangePasswordVM
    {
        public int? UserId { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d](?=.*?[#?!@$%^&*-]).{6,12}$", ErrorMessage = "Password is in invalid format. Password format requirements: Min: 6 characters, Max: 12 characters and at least 1 upper case, 1 lower case, 1 number, 1 special character; Confirm password is invalid format.")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Confirm Password is required.")]
        public string ConfirmPassword { get; set; }
        

    }
    public class LoginVM
    {
        [Required(ErrorMessage = "User Name is required.")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        public string Password { get; set; }
    }
    
    public class ForgotPasswordVM
    {
        public string UserName { get; set; }
    }
    public class UserWebModel
    {
        public UserWebModel()
        {
            this.SelectedFaith = new List<CheckboxModel>();
            this.SelectedFor = new List<CheckboxModel>();
            this.SelectedIssues = new List<CheckboxModel>();
            this.SelectedInsurance = new List<CheckboxModel>();
            this.SelectedGenderPreference = new List<CheckboxModel>();
            this.SelectedLanguage = new List<CheckboxModel>();
            this.SelectedLocation = new List<CheckboxModel>();
            this.SelectedTherapistTypesOfTherapy = new List<CheckboxModel>();
            this.TherapyGalleryList = new List<TherapyGallery>();
        }

        public List<CheckboxModel> SelectedFaith { get; set; }
        public List<CheckboxModel> SelectedFor { get; set; }
        public List<CheckboxModel> SelectedIssues { get; set; }
        public List<CheckboxModel> SelectedInsurance { get; set; }
        public List<CheckboxModel> SelectedGenderPreference { get; set; }
        public List<CheckboxModel> SelectedLanguage { get; set; }
        public List<CheckboxModel> SelectedLocation { get; set; }
        public List<CheckboxModel> SelectedTherapistTypesOfTherapy { get; set; }
        public List<TherapyGallery> TherapyGalleryList { get; set; }
        public int? UserId { get; set; }
        
        [StringLength(50)]
        public string UserType { get; set; }
        public string ProfilePic { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string Title { get; set; }

        [StringLength(20)]
        public string YearsInService { get; set; }

        [StringLength(100)]
        public string Education { get; set; }

        public bool AcceptInsurance { get; set; }

        [StringLength(50)]
        public string License { get; set; }

        public string AboutMe { get; set; }
        
        [StringLength(200)]

        [Url(ErrorMessage = "Please enter a valid url")]
        public string VideoUrl { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        public string Location { get; set; }

        public int? AvgCost { get; set; }

        [StringLength(200)]
        public string Address1 { get; set; }

        [StringLength(200)]
        public string Address2 { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(8)]
        public string ZipCode { get; set; }

        public byte? Gender { get; set; } 

    }

    public class LogingResponseVM
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
    }
    
}