package com.theratrove.activityList.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Profile implements Serializable {

    @SerializedName("Therapist")
    @Expose
    private Object therapist;
    @SerializedName("UserList")
    @Expose
    private Object userList;
    @SerializedName("TherapistList")
    @Expose
    private Object therapistList;
    @SerializedName("SearchFilterValueList")
    @Expose
    private Object searchFilterValueList;
    @SerializedName("SelectedFaith")
    @Expose
    private List<Object> selectedFaith = null;
    @SerializedName("SelectedFor")
    @Expose
    private List<Object> selectedFor = null;
    @SerializedName("SelectedIssues")
    @Expose
    private List<Object> selectedIssues = null;
    @SerializedName("SelectedInsurance")
    @Expose
    private List<Object> selectedInsurance = null;
    @SerializedName("SelectedGenderPreference")
    @Expose
    private List<Object> selectedGenderPreference = null;
    @SerializedName("SelectedLanguage")
    @Expose
    private List<Object> selectedLanguage = null;
    @SerializedName("SelectedLocation")
    @Expose
    private List<Object> selectedLocation = null;
    @SerializedName("TherapyGalleryList")
    @Expose
    private ArrayList<TherapyGalleryList> therapyGalleryList = null;

    @SerializedName("SelectedTherapistTypesOfTherapy")
    @Expose
    private ArrayList<TherapistTypesOfTherapy> selectedTherapistTypesOfTherapy = null;
    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("ProfilePic")
    @Expose
    private String profilePic;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Password")
    @Expose
    private Object password;
    @SerializedName("CreateDate")
    @Expose
    private Object createDate;
    @SerializedName("LastLogin")
    @Expose
    private Object lastLogin;
    @SerializedName("FailedLoginAttempts")
    @Expose
    private Object failedLoginAttempts;
    @SerializedName("Locked")
    @Expose
    private Object locked;
    @SerializedName("UserType")
    @Expose
    private Object userType;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("YearsInService")
    @Expose
    private String yearsInService;
    @SerializedName("Education")
    @Expose
    private String education;
    @SerializedName("AcceptInsurance")
    @Expose
    private Boolean acceptInsurance;
    @SerializedName("License")
    @Expose
    private String license;
    @SerializedName("AboutMe")
    @Expose
    private String aboutMe;
    @SerializedName("VideoUrl")
    @Expose
    private String videoUrl;
    @SerializedName("PhoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("AvgCost")
    @Expose
    private Integer avgCost;
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("ZipCode")
    @Expose
    private String zipCode;
    @SerializedName("Gender")
    @Expose
    private Integer gender;
    @SerializedName("Active")
    @Expose
    private Boolean active;
    @SerializedName("FaithFilter")
    @Expose
    private String faithFilter;
    @SerializedName("ForFilter")
    @Expose
    private String forFilter;
    @SerializedName("IssuesFilter")
    @Expose
    private String issuesFilter;
    @SerializedName("InsuranceZipCodeFilter")
    @Expose
    private String insuranceZipCodeFilter;
    @SerializedName("GenderPreferenceFilter")
    @Expose
    private String genderPreferenceFilter;
    @SerializedName("LanguageFilter")
    @Expose
    private String languageFilter;
    @SerializedName("LocationFilter")
    @Expose
    private String locationFilter;
    @SerializedName("TherapistTypesOfTherapyFilter")
    @Expose
    private String therapistTypesOfTherapyFilter;
    @SerializedName("TherapistRating")
    @Expose
    private Float therapistRating;
    @SerializedName("TherapistRatingCount")
    @Expose
    private Integer therapistRatingCount;

    public Object getTherapist() {
        return therapist;
    }

    public void setTherapist(Object therapist) {
        this.therapist = therapist;
    }

    public Object getUserList() {
        return userList;
    }

    public void setUserList(Object userList) {
        this.userList = userList;
    }

    public Object getTherapistList() {
        return therapistList;
    }

    public void setTherapistList(Object therapistList) {
        this.therapistList = therapistList;
    }

    public Object getSearchFilterValueList() {
        return searchFilterValueList;
    }

    public void setSearchFilterValueList(Object searchFilterValueList) {
        this.searchFilterValueList = searchFilterValueList;
    }

    public List<Object> getSelectedFaith() {
        return selectedFaith;
    }

    public void setSelectedFaith(List<Object> selectedFaith) {
        this.selectedFaith = selectedFaith;
    }

    public List<Object> getSelectedFor() {
        return selectedFor;
    }

    public void setSelectedFor(List<Object> selectedFor) {
        this.selectedFor = selectedFor;
    }

    public List<Object> getSelectedIssues() {
        return selectedIssues;
    }

    public void setSelectedIssues(List<Object> selectedIssues) {
        this.selectedIssues = selectedIssues;
    }

    public List<Object> getSelectedInsurance() {
        return selectedInsurance;
    }

    public void setSelectedInsurance(List<Object> selectedInsurance) {
        this.selectedInsurance = selectedInsurance;
    }

    public List<Object> getSelectedGenderPreference() {
        return selectedGenderPreference;
    }

    public void setSelectedGenderPreference(List<Object> selectedGenderPreference) {
        this.selectedGenderPreference = selectedGenderPreference;
    }

    public List<Object> getSelectedLanguage() {
        return selectedLanguage;
    }

    public void setSelectedLanguage(List<Object> selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
    }

    public List<Object> getSelectedLocation() {
        return selectedLocation;
    }

    public void setSelectedLocation(List<Object> selectedLocation) {
        this.selectedLocation = selectedLocation;
    }

    public ArrayList<TherapyGalleryList> getTherapyGalleryList() {
        return therapyGalleryList;
    }

    public void setTherapyGalleryList(ArrayList<TherapyGalleryList> therapyGalleryList) {
        this.therapyGalleryList = therapyGalleryList;
    }

    public ArrayList<TherapistTypesOfTherapy> getSelectedTherapistTypesOfTherapy() {
        return selectedTherapistTypesOfTherapy;
    }

    public void setSelectedTherapistTypesOfTherapy(ArrayList<TherapistTypesOfTherapy> selectedTherapistTypesOfTherapy) {
        this.selectedTherapistTypesOfTherapy = selectedTherapistTypesOfTherapy;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getPassword() {
        return password;
    }

    public void setPassword(Object password) {
        this.password = password;
    }

    public Object getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Object createDate) {
        this.createDate = createDate;
    }

    public Object getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Object lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Object getFailedLoginAttempts() {
        return failedLoginAttempts;
    }

    public void setFailedLoginAttempts(Object failedLoginAttempts) {
        this.failedLoginAttempts = failedLoginAttempts;
    }

    public Object getLocked() {
        return locked;
    }

    public void setLocked(Object locked) {
        this.locked = locked;
    }

    public Object getUserType() {
        return userType;
    }

    public void setUserType(Object userType) {
        this.userType = userType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYearsInService() {
        return yearsInService;
    }

    public void setYearsInService(String yearsInService) {
        this.yearsInService = yearsInService;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public Boolean getAcceptInsurance() {
        return acceptInsurance;
    }

    public void setAcceptInsurance(Boolean acceptInsurance) {
        this.acceptInsurance = acceptInsurance;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(Integer avgCost) {
        this.avgCost = avgCost;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getFaithFilter() {
        return faithFilter;
    }

    public void setFaithFilter(String faithFilter) {
        this.faithFilter = faithFilter;
    }

    public String getForFilter() {
        return forFilter;
    }

    public void setForFilter(String forFilter) {
        this.forFilter = forFilter;
    }

    public String getIssuesFilter() {
        return issuesFilter;
    }

    public void setIssuesFilter(String issuesFilter) {
        this.issuesFilter = issuesFilter;
    }

    public String getInsuranceZipCodeFilter() {
        return insuranceZipCodeFilter;
    }

    public void setInsuranceZipCodeFilter(String insuranceZipCodeFilter) {
        this.insuranceZipCodeFilter = insuranceZipCodeFilter;
    }

    public String getGenderPreferenceFilter() {
        return genderPreferenceFilter;
    }

    public void setGenderPreferenceFilter(String genderPreferenceFilter) {
        this.genderPreferenceFilter = genderPreferenceFilter;
    }

    public String getLanguageFilter() {
        return languageFilter;
    }

    public void setLanguageFilter(String languageFilter) {
        this.languageFilter = languageFilter;
    }

    public String getLocationFilter() {
        return locationFilter;
    }

    public void setLocationFilter(String locationFilter) {
        this.locationFilter = locationFilter;
    }

    public Object getTherapistTypesOfTherapyFilter() {
        return therapistTypesOfTherapyFilter;
    }

    public void setTherapistTypesOfTherapyFilter(String therapistTypesOfTherapyFilter) {
        this.therapistTypesOfTherapyFilter = therapistTypesOfTherapyFilter;
    }

    public Float getTherapistRating() {
        return therapistRating;
    }

    public void setTherapistRating(Float therapistRating) {
        this.therapistRating = therapistRating;
    }

    public Integer getTherapistRatingCount() {
        return therapistRatingCount;
    }

    public void setTherapistRatingCount(Integer therapistRatingCount) {
        this.therapistRatingCount = therapistRatingCount;
    }

    public static class TherapyGalleryList implements Serializable {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("UserId")
        @Expose
        private Integer userId;
        @SerializedName("GalleryImageURL")
        @Expose
        private String GalleryImageURL;
        @SerializedName("CreateDateTime")
        @Expose
        private String createDateTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getGalleryImage() {
            return GalleryImageURL;
        }

        public void setGalleryImage(String galleryImage) {
            this.GalleryImageURL = galleryImage;
        }

        public String getCreateDateTime() {
            return createDateTime;
        }

        public void setCreateDateTime(String createDateTime) {
            this.createDateTime = createDateTime;
        }

    }

    public static class TherapistTypesOfTherapy implements Serializable {

        @SerializedName("Id")
        @Expose
        private Integer Id;
        @SerializedName("Name")
        @Expose
        private String Name;
        @SerializedName("Checked")
        @Expose
        private boolean Checked;
        @SerializedName("itemFor")
        @Expose
        private int itemFor;

        public Integer getId() {
            return Id;
        }

        public void setId(Integer id) {
            this.Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String userId) {
            this.Name = userId;
        }

        public boolean getChecked() {
            return Checked;
        }

        public void setChecked(boolean Checked) {
            this.Checked = Checked;
        }

        public int getItemFor() {
            return itemFor;
        }

        public void setItemFor(int itemFor) {
            this.itemFor = itemFor;
        }

    }

}




