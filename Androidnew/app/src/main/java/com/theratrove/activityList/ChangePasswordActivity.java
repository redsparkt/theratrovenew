package com.theratrove.activityList;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.theratrove.R;
import com.theratrove.activityList.appUtils.AppSharedPreferences;
import com.theratrove.activityList.appUtils.CommonValidation;
import com.theratrove.activityList.appUtils.Constant;
import com.theratrove.activityList.appUtils.Utility;
import com.theratrove.activityList.network.ApiClient;
import com.theratrove.activityList.network.ApiInterface;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity implements Callback<String> {


    ApiInterface apiService;
    HttpUrl httpUrl;

    @BindView(R.id.imgBack)
    ImageView imgBack;
    @BindView(R.id.txttitle)
    TextView txttitle;
    @BindView(R.id.toolbar)
    Toolbar toolbarTop;
    @BindView(R.id.edtUserName)
    EditText edtUserName;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.edtConfirmPwd)
    EditText edtConfirmPwd;
    @BindView(R.id.txtSubmit)
    TextView txtSubmit;
    @BindView(R.id.txtSignIn)
    TextView txtSignIn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change);
        ButterKnife.bind(this);
        initVIew();

        apiService = ApiClient.getClient().create(ApiInterface.class);
    }

    public void initVIew() {
        if (toolbarTop != null) {
            setSupportActionBar(toolbarTop);
            imgBack = toolbarTop.findViewById(R.id.imgBack);
            txttitle.setText("Change Password");
        }


        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()) {
                    if (CommonValidation.checkInternetConnection(ChangePasswordActivity.this)) {
                        callChangePassword(AppSharedPreferences.getSharePreference(ChangePasswordActivity.this).getUserID(), edtUserName.getText().toString().trim(),
                                edtPassword.getText().toString().trim(), edtConfirmPwd.getText().toString().trim());
                    } else {
                        CommonValidation.showToast(ChangePasswordActivity.this, getString(R.string.no_internet));
                    }
                }

            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    public void callChangePassword(String userid, String useName, String password, String confirmPassword) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("UserId", userid);
        jsonObject.addProperty("UserName", useName);
        jsonObject.addProperty("Password", password);
        jsonObject.addProperty("email", AppSharedPreferences.getSharePreference(this).getUserEmail());
        jsonObject.addProperty("ConfirmPassword", confirmPassword);

        Call<String> callLogin = apiService.ChangePassword(jsonObject);
        httpUrl = callLogin.request().url();
        callLogin.clone().enqueue(this);
        Utility.showProgress(ChangePasswordActivity.this);
    }

    @Override
    public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
        try {
            Utility.dismissProgress();
            if (response.isSuccessful() && response.code() == 200) {
                Log.e("Sucess", "response");
                CommonValidation.showToast(ChangePasswordActivity.this, response.body());
                finish();
            } else {
                try {
                    CommonValidation.showToast(ChangePasswordActivity.this, response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

    }

    @Override
    public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
        Utility.dismissProgress();
        t.printStackTrace();
    }

    public boolean isValidate() {
        if (edtUserName.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(this, getString(R.string.username_blank_msg));
            return false;
        } else if (edtPassword.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(this, getString(R.string.passworde_blank_msg));
            return false;
        } else if (edtConfirmPwd.getText().toString().trim().isEmpty()) {
            CommonValidation.showToast(this, getString(R.string.confirm_password_blank_msg));
            return false;
        } else if (!CommonValidation.isValidPassword(edtPassword.getText().toString().trim())) {
            CommonValidation.showToast(this, getString(R.string.pass_match));
            return false;
        } else if (!edtPassword.getText().toString().trim().equals(edtConfirmPwd.getText().toString().trim())) {
            CommonValidation.showToast(this, getString(R.string.not_match));
            return false;
        }
        return true;
    }
}
